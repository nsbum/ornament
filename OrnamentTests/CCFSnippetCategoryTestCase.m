#import "CCFTestCase.h"
#import "CCFSnippetCategory.h"
#import "CCFSnippet.h"

@interface CCFSnippetCategoryTestCase : CCFTestCase {
    CCFSnippetCategory *_category;
}

@end

@implementation CCFSnippetCategoryTestCase

#pragma mark - Test lifecycle

- (void)setUp {
    [super setUp];
    _category = [[CCFSnippetCategory alloc] init];
}

- (void)tearDown {
    _category = nil;
    [super tearDown];
}

#pragma mark - Properties

- (void)testThatCCFSnippetCategoryHasTitleProperty {
    objc_property_t property = class_getProperty([CCFSnippetCategory class], "title");
    STAssertTrue(property != NULL, @"CCFSnippetCategory has no title property");
}

- (void)testThatCCFSnippetCategoryHasSnippetsProperty {
    objc_property_t property = class_getProperty([CCFSnippetCategory class], "snippets");
    STAssertTrue(property != NULL, @"CCFSnippetCategory has no snippets property");
}

#pragma mark - Behaviors

- (void)testThatInitializationCreatesEmptySnippetsArray {
    STAssertTrue(_category.snippets != nil, @"No snippets array created at init");
}

- (void)testThatWeCanAddSnippet {
    CCFSnippet *snippet = [[CCFSnippet alloc] init];
    [_category addSnippet:snippet];
    STAssertTrue([[_category snippets] count] != 0, @"Can't add snippet");
}

- (void)testThatWeCanFindSnippetByTag {
    CCFSnippet *snippet = [[CCFSnippet alloc] init];
    snippet.tag = 1010;
    [_category addSnippet:snippet];
    STAssertNotNil([_category snippetWithTag:1010], @"Unable to find tagged snippet");
}


@end
