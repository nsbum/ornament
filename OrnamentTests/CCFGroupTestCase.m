#import "CCFTestCase.h"
#import "CCFGroup.h"

@interface CCFGroupTestCase : CCFTestCase {
    CCFGroup *_group;
}

@end

@implementation CCFGroupTestCase

- (void)testThatGRoupHasRangeProperty {
    objc_property_t property = class_getProperty([CCFGroup class], "range");
    STAssertTrue(property != NULL, @"CCFGroup has no range property");
}

- (void)testThatWeCanCreateAGroup {
    NSRange range = NSMakeRange(0, 2);
    _group = [[CCFGroup alloc] initWithSource:@"blah" index:2 range:range];
    STAssertNotNil(_group, @"Unable to create group with designated initializer");
}

@end
