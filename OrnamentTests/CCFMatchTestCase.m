#import "CCFTestCase.h"
#import "CCFMatch.h"

@interface CCFMatchTestCase : CCFTestCase {
    CCFMatch *_match;
}

@end

@implementation CCFMatchTestCase

#pragma mark - Test lifecycle

- (void)setUp {
    [super setUp];
    _match = [[CCFMatch alloc] init];
}

- (void)tearDown {
    _match = nil;
    [super tearDown];
}

#pragma mark - Properties

- (void)testThatCCFMatchHasResultProperty {
    objc_property_t property = class_getProperty([CCFMatch class], "result");
    STAssertTrue(property != NULL, @"CCFMatch has no result property");
}

- (void)testThatCCFMatchHasGroupsProperty {
    objc_property_t property = class_getProperty([CCFMatch class], "groups");
    STAssertTrue(property != NULL, @"CCFMatch has no groups property");
}

#pragma mark - Behaviors

- (void)testThatWeCanCreateMatchWithResultSourceAndIndex {
    NSTextCheckingResult *result = [NSTextCheckingResult new];
    NSString *source = @"blah";
    _match = [[CCFMatch alloc] initWithResult:result forSourceString:source index:1];
    STAssertNotNil(_match.result, @"Result not saved in init");
}

@end
