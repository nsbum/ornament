/**
 *   @file CCFExpressionFormatterHelperTestCase.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 10/26/12   5:22 AM
 *
 *   @note Copyright 2012 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFTestCase.h"
#import "CCFExpressionFormatterHelper.h"

 @interface CCFExpressionFormatterHelperTestCase : CCFTestCase {
 	//	ivar for unit under test
 	CCFExpressionFormatterHelper *_helper;
 }

 @end

@implementation CCFExpressionFormatterHelperTestCase

#pragma mark - Test lifecycle

 - (void)setUp {
 	[super setUp];
 	_helper = [[CCFExpressionFormatterHelper alloc] init];
 }

 - (void)tearDown {
 	_helper = nil;
 	[super tearDown];
 }

 #pragma mark - Properties

 #pragma mark - Behaviors

- (void)testThatWeCanAppendCaseInsensitiveOption {
    NSString *optionsString = [_helper optionsStringForOptions:NSRegularExpressionCaseInsensitive];
    STAssertTrue([optionsString isEqualToString:@"i"], @"Incorrect options string for case insensitive");
}

- (void)testThatVerboseReturnsCorrectString {
    NSString *optionsString = [_helper optionsStringForOptions:NSRegularExpressionAllowCommentsAndWhitespace];
    STAssertTrue([optionsString isEqualToString:@"x"], @"Incorrect options string for verbose");
}

- (void)testThatMultilineReturnsCorretString {
    NSString *optionsString = [_helper optionsStringForOptions:NSRegularExpressionAnchorsMatchLines];
    STAssertTrue([optionsString isEqualToString:@"m"], @"Incorrect options string for multiline");
}

- (void)testThatSingleLineReturnsCorrectString {
    NSString *optionsString = [_helper optionsStringForOptions:NSRegularExpressionDotMatchesLineSeparators];
    STAssertTrue([optionsString isEqualToString:@"s"], @"Incorrect options string for single line");
}

- (void)testThatCombinationOfOptionsReturnsCorrectString {
    NSRegularExpressionOptions opts = NSRegularExpressionAllowCommentsAndWhitespace | NSRegularExpressionCaseInsensitive;
    NSString *optionsString = [_helper optionsStringForOptions:opts];
    STAssertTrue([optionsString isEqualToString:@"ix"], @"Incorrect options string for combination of options");
}


@end
