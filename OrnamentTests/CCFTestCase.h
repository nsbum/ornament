//
//  CCFTestCase.h
//  Fidget
//
//  Created by alanduncan on 10/2/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#if TARGET_OS_IPHONE
#import "OCMock.h"
#else
#import <OCMock/OCMock.h>
#endif
#import <SenTestingKit/SenTestingKit.h>
#import <objc/runtime.h>

@interface CCFTestCase : SenTestCase

#if TARGET_OS_IPHONE
#elif TARGET_OS_MAC
- (BOOL)object:(id)object shouldHaveBinding:(NSString *)binding to:(id)boundObject throughKeyPath:(NSString *)keyPath;
#endif

@end
