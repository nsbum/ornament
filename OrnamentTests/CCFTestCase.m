//
//  CCFTestCase.m
//  Fidget
//
//  Created by alanduncan on 10/2/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFTestCase.h"

@implementation CCFTestCase

#if TARGET_OS_IPHONE
#elif TARGET_OS_MAC
- (BOOL)object:(id)object shouldHaveBinding:(NSString *)binding to:(id)boundObject throughKeyPath:(NSString *)keyPath {
    NSDictionary *info = [object infoForBinding:binding];
    BOOL isBoundObject = [[info objectForKey:NSObservedObjectKey] isEqual:boundObject];
    BOOL isKeyPath = [[info objectForKey:NSObservedKeyPathKey] isEqualToString:keyPath];
    
    return (isBoundObject && isKeyPath);
}
#endif

- (void)testThatAlwaysPasses {
    STAssertEquals(1, 1, @"This should always pass");
    printf("done");
}

@end
