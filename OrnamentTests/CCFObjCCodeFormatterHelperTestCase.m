/**
 *   @file CCFObjCCodeFormatterHelperTestCase.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 11/1/12   11:12 PM
 *
 *   @note Copyright 2012 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFTestCase.h"
#import "CCFObjCCodeFormatterHelper.h"

 @interface CCFObjCCodeFormatterHelperTestCase : CCFTestCase {
 	//	ivar for unit under test
 	CCFObjCCodeFormatterHelper *_helper;
 }

 @end

@implementation CCFObjCCodeFormatterHelperTestCase

#pragma mark - Test lifecycle

 - (void)setUp {
 	[super setUp];
 	_helper = [[CCFObjCCodeFormatterHelper alloc] init];
 }

 - (void)tearDown {
 	_helper = nil;
 	[super tearDown];
 }

 #pragma mark - Properties

 #pragma mark - Behaviors

- (void)testThatVerboseOptionsReturnsProperString {
    NSString *actual = [_helper optionNameForOption:NSRegularExpressionAllowCommentsAndWhitespace];
    NSString *expected = @"NSRegularExpressionAllowCommentsAndWhitespace";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect option string for verboxe");
}

- (void)testThatDotMatchesReturnsProperString {
    NSString *actual = [_helper optionNameForOption:NSRegularExpressionDotMatchesLineSeparators];
    NSString *expected = @"NSRegularExpressionDotMatchesLineSeparators";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect option string for dot matches...");
}

- (void)testThatAnchorsMatchesReturnsProperString {
    NSString *actual = [_helper optionNameForOption:NSRegularExpressionAnchorsMatchLines];
    NSString *expected = @"NSRegularExpressionAnchorsMatchLines";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect option string from anchor matches...");
}

- (void)testThatCaseInsensitiveOptionReturnsProperString {
    NSString *actual = [_helper optionNameForOption:NSRegularExpressionCaseInsensitive];
    NSString *expected = @"NSRegularExpressionCaseInsensitive";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect option string from case insensitive.");
}

- (void)testFullOptionsStringWithSingleOption {
    NSString *optionsString = [_helper optionStringForOptions:NSRegularExpressionCaseInsensitive];
    NSString *expected = @"NSRegularExpressionCaseInsensitive";
    STAssertTrue([optionsString isEqualToString:expected], @"Incorrect options string with single option");
}

- (void)testFullOptionsStringWithTwoOptions {
    NSString *optionsString = [_helper optionStringForOptions:NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace];
    NSString *expected = @"NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace";
    BOOL order1 = [optionsString isEqualToString:expected];
    BOOL order2 = [optionsString isEqualToString:@"NSRegularExpressionAllowCommentsAndWhitespace | NSRegularExpressionCaseInsensitive"];
    STAssertTrue((order1 | order2), @"Incorrect options string with multiple options");
}

@end
