#import "CCFTestCase.h"
#import "CCFSnippetImporter.h"

@interface CCFSnippetImporterTestCase : CCFTestCase

@end

@implementation CCFSnippetImporterTestCase

- (void)testThatWeCanImportSnippets {
    CCFSnippetImporter *_importer = [[CCFSnippetImporter alloc] initWithCompletionBlock:^(NSArray *snippetCategories) {
        STAssertTrue([snippetCategories count] != 0, @"Unable to import snippets");
    }];
    [_importer import];
}

@end
