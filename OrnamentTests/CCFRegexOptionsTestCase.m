#import "CCFTestCase.h"
#import "CCFRegexOptions.h"

@interface CCFRegexOptionsTestCase : CCFTestCase {
    CCFRegexOptions *_options;
}

@end


@implementation CCFRegexOptionsTestCase

#pragma mark - Test lifecycle

- (void)setUp {
    [super setUp];
    _options = [[CCFRegexOptions alloc] init];
}

- (void)tearDown {
    _options = nil;
    [super tearDown];
}

#pragma mark - Properties

- (void)testThatRegexOptionsHasCaseInsensitiveProperty {
    objc_property_t property = class_getProperty([CCFRegexOptions class], "caseInsensitive");
    STAssertTrue(property != NULL, @"CCFRegexOptions has no caseInsensitive property");
}

- (void)testThatRegexOptionsHasVerboseProperty {
    objc_property_t property = class_getProperty([CCFRegexOptions class], "verbose");
    STAssertTrue(property != NULL, @"CCFRegexOptions has no verbose property");
}

- (void)testThatRegexOptionsHasMultiLineProperty {
    objc_property_t property = class_getProperty([CCFRegexOptions class], "multiLine");
    STAssertTrue(property != NULL, @"CCFRegexOptions has no multiLine property");
}

- (void)testThatRegexOptionsHasSingleLineProperty {
    objc_property_t property = class_getProperty([CCFRegexOptions class], "singleLine");
    STAssertTrue(property != NULL, @"CCFRegexOptions has no singleLine property");
}

#pragma mark - Behaviors

- (void)testThatCaseInsensitiveOptionIsCorrect {
    _options.caseInsensitive = YES;
    STAssertTrue([_options regexOptions] == NSRegularExpressionCaseInsensitive, @"Case insensitive option is incorrect");
}

- (void)TestThatVerboseOptionIsCorrect {
    _options.verbose = YES;
    STAssertTrue([_options regexOptions] == NSRegularExpressionAllowCommentsAndWhitespace, @"Verbose option is incorrect");
}

- (void)testThatMultiLineOptionIsCorrect {
    _options.multiLine = YES;
    STAssertTrue([_options regexOptions] == NSRegularExpressionAnchorsMatchLines, @"Multiline option is incorrect");
}

- (void)testThatSingleLineOptionIsCorrect {
    _options.singleLine = YES;
    STAssertTrue([_options regexOptions] == NSRegularExpressionDotMatchesLineSeparators, @"Single line option is incorrect");
}

- (void)testCombinationOfOptionsIsCorrect {
    _options.caseInsensitive = YES;
    _options.verbose = YES;
    STAssertTrue([_options regexOptions] == (NSRegularExpressionAllowCommentsAndWhitespace | NSRegularExpressionCaseInsensitive), @"Combination of options is incorrect");
}

- (void)testThatWeCanCreateRegexOptionsObjectWithOptions {
    NSRegularExpressionOptions options = NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace;
    _options = [CCFRegexOptions optionsObjectWithOptions:options];
    STAssertTrue((_options.caseInsensitive && _options.verbose), @"Options object not created correctly from options");
}
@end
