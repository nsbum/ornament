/**
 *   @file CCFSnapshotTestCase.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 10/27/12   8:43 AM
 *
 *   @note Copyright 2012 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFTestCase.h"
#import "CCFSnapshot.h"

 @interface CCFSnapshotTestCase : CCFTestCase {
 	//	ivar for unit under test
 	CCFSnapshot *_snapshot;
 }

 @end

@implementation CCFSnapshotTestCase

#pragma mark - Test lifecycle

 - (void)setUp {
 	[super setUp];
 	_snapshot = [[CCFSnapshot alloc] init];
 }

 - (void)tearDown {
 	_snapshot = nil;
 	[super tearDown];
 }

 #pragma mark - Properties

- (void)testThatSnapshotHasRegexStringProperty {
    objc_property_t property = class_getProperty([CCFSnapshot class], "regexString");
    STAssertTrue(property != NULL, @"CCFSnapshot has no regexString property");
}

- (void)testThatSnapshotHasRegexOptionsProperty {
    objc_property_t property = class_getProperty([CCFSnapshot class], "regexOptions");
    STAssertTrue(property != NULL, @"CCFSnapshot has no regexOptions property");
}

- (void)testThatSnapshotHasSampleTextProperty {
    objc_property_t property = class_getProperty([CCFSnapshot class], "sampleText");
    STAssertTrue(property != NULL, @"CCFSnapshot has no sampleText property");
}

- (void)testThatSnapshotHasReplacementTextProperty {
    objc_property_t property = class_getProperty([CCFSnapshot class], "replacementText");
    STAssertTrue(property != NULL, @"CCFSnapshot has no replacementText property");
}

 #pragma mark - Behaviors

 

@end
