#import "CCFTestCase.h"
#import "CCFExpressionFormatter.h"
#import "_CCFExpressionFormatterPerl.h"
#import "CCFRegex.h"

@interface CCFExpressionFormatterTestCase : CCFTestCase

@end


@implementation CCFExpressionFormatterTestCase

- (void)testThatCCFExpressionFormatterHasRegexProperty {
    objc_property_t property = class_getProperty([CCFExpressionFormatter class], "regex");
    STAssertTrue(property != NULL, @"CCFExpressionFormatter has no regex property");
}

- (void)testThatCCFExpressionFormatterHasExpressionProperty {
    objc_property_t property = class_getProperty([CCFExpressionFormatter class], "expression");
    STAssertTrue(property != NULL, @"CCFExpressionFormatter has no expression property");
}

#pragma mark - Behaviors

- (void)testThatWeReturnPrivateObjCClassForType {
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypeObjC];
    NSString *className = NSStringFromClass([formatter class] );
    STAssertTrue([className isEqual:@"_CCFExpressionFormatterObjC"], @"Incorrect class returned for ObjC");
}

- (void)testThatWeReturnPrivatePerlClassForType {
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePerl];
    NSString *className = NSStringFromClass([formatter class] );
    STAssertTrue([className isEqualToString:@"_CCFExpressionFormatterPerl"], @"Incorrect class returned for Perl");
}

- (void)testThatWeReturnPrivatePythonClassForType {
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePython];
    NSString *className = NSStringFromClass([formatter class] );
    STAssertTrue([className isEqualToString:@"_CCFExpressionFormatterPython"], @"Incorrect class returned for Python");
}

#pragma mark Perl

- (void)testThatWeReturnFormattedPerlExpressionWithoutOptions {
    CCFRegex *regex = [[CCFRegex alloc] initWithString:@"[A-Za-z]{3}"];
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePerl];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Za-z]{3}";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect Perl match expression - failing %@",actual);
}

- (void)testThatWeReturnFormattedPerlExpressionWithVerboseOption {
    CCFRegex *regex = [[CCFRegex alloc] initWithString:@"[A-Za-z]{3}"];
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionAllowCommentsAndWhitespace];
    regex.options = option;
    
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePerl];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Za-z]{3}";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect Perl match expression - failing %@",actual);
}

- (void)testThatWeReturnFormattedPerlExpressionWithSingleLineOption {
    CCFRegex *regex = [[CCFRegex alloc] initWithString:@"[A-Za-z]{3}"];
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionDotMatchesLineSeparators];
    regex.options = option;
    
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePerl];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Za-z]{3}";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect Perl match expression - failing %@",actual);
}

- (void)testThatWeReturnFormattedPerlExpressionWithMultiLineOption {
    CCFRegex *regex = [[CCFRegex alloc] initWithString:@"[A-Za-z]{3}"];
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionAnchorsMatchLines];
    regex.options = option;
    
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePerl];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Za-z]{3}";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect Perl match expression - failing %@",actual);
}

- (void)testThatWeReturnFormattedPerlExpressionWithCaseInsensitiveOption {
    CCFRegex *regex = [[CCFRegex alloc] initWithString:@"[A-Za-z]{3}"];
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive];
    regex.options = option;
    
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePerl];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Za-z]{3}";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect Perl match expression - failing %@",actual);
}

#pragma mark Python

- (void)testThatWeReturnFormattedPythonExpression {
    //  create our regex object (default is matching...)
    NSString *regexString = @"[A-Z]\\d*\\s";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  create our formatter for python
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePython];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Z]\\d*\\s";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect python match expression");
}

#pragma mark PHP

- (void)testThatWeReturnFormattedPHPExpression {
    //  create our regex object (default is matching...)
    NSString *regexString = @"[A-Z]\\d*\\s";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  create the formatter for PHP
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Z]\\d*\\s";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect PHP match expression");
}

- (void)testThatWeReturnFormattedPHPExpressionWithCaseInsensitiveOption {
    //  create our regex object (default is matching...)
    NSString *regexString = @"[A-Z]\\d*\\s";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  add an option
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive];
    regex.options = option;
    //  create the formatter for PHP
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Z]\\d*\\s";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect PHP match expression");
}

- (void)testThatWeReturnFormattedPHPExpressionWithVerboseOption {
    //  create our regex object (default is matching...)
    NSString *regexString = @"[A-Z]\\d*\\s";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  add an option
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionAllowCommentsAndWhitespace];
    regex.options = option;
    //  create the formatter for PHP
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Z]\\d*\\s";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect PHP match expression");
}

- (void)testThatWeReturnFormattedPHPExpressionWithMultilineOption {
    //  create our regex object (default is matching...)
    NSString *regexString = @"[A-Z]\\d*\\s";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  add an option
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionAnchorsMatchLines];
    regex.options = option;
    //  create the formatter for PHP
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Z]\\d*\\s";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect PHP match expression");
}

- (void)testThatWeReturnFormattedPHPExpressionWithSingleLineOption {
    //  create our regex object (default is matching...)
    NSString *regexString = @"[A-Z]\\d*\\s";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  add an option
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionDotMatchesLineSeparators];
    regex.options = option;
    //  create the formatter for PHP
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Z]\\d*\\s";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect PHP match expression");
}

- (void)testThatWeReturnFormattedPHPExpressionWithMultipleOptions {
    //  create our regex object (default is matching...)
    NSString *regexString = @"[A-Z]\\d*\\s";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  add an option
    NSRegularExpressionOptions opts = NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace;
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:opts];
    regex.options = option;
    //  create the formatter for PHP
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [formatter setRegex:regex];
    
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Z]\\d*\\s";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect PHP match expression");
    
    printf("finished");
}

#pragma mark JavaScript

- (void)testThatWeReturnFormattedJavaScriptExpressionWithNoOptions {
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  create the formatter for JavaScript
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypeJavaScript];
    [formatter setRegex:regex];
    
    //  get expression and assert
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Za-z]{3}";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect expression for JavaScript without options");
}

- (void)testThatWeReturnFormattedJavaScriptExpressionWithOptions {
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  add an option
    NSRegularExpressionOptions opts = NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace;
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:opts];
    regex.options = option;
    //  create the formatter for JavaScript
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypeJavaScript];
    [formatter setRegex:regex];
    
    //  get expression and assert
    NSString *actual = formatter.expression;
    NSString *expected = @"[A-Za-z]{3}";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect expression for JavaScript without options");
}

#pragma mark Ruby

- (void)testThatWeReturnFormattedRubyExpressionWithNoOptions {
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  create the formatter for JavaScript
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypeRuby];
    [formatter setRegex:regex];
    
    //  get expression and assert
    NSString *actual = formatter.expression;
    NSString *expected = regexString;
    STAssertTrue([actual isEqualToString:expected], @"Incorrect expression for Ruby without options");
}

- (void)testThatWeReturnFormattedRubyExpressionWithOptions {
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  add an option
    NSRegularExpressionOptions opts = NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace;
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:opts];
    regex.options = option;
    //  create the formatter for JavaScript
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypeRuby];
    [formatter setRegex:regex];
    
    //  get expression and assert
    NSString *actual = formatter.expression;
    NSString *expected = regexString;
    STAssertTrue([actual isEqualToString:expected], @"Incorrect expression for Ruby without options");
}

@end
