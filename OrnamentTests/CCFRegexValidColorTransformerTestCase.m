#import "CCFTestCase.h"
#import "CCFRegexValidColorTransformer.h"

@interface CCFRegexValidColorTransformerTestCase : CCFTestCase

@end

@implementation CCFRegexValidColorTransformerTestCase

- (void)testThatTheTransformedValueClassIsNSColor {
    Class actualClass = [CCFRegexValidColorTransformer transformedValueClass];
    Class expectedClass = [NSColor class];
    STAssertEqualObjects(actualClass, expectedClass, @"Transformed value class is not NSColor");
}

- (void)testThatNoReturnsAReddishColor {
    CCFRegexValidColorTransformer *transformer = [[CCFRegexValidColorTransformer alloc] init];
    NSColor *actual = [transformer transformedValue:@NO];
    CGFloat red,green,blue,alpha;
    [actual getRed:&red green:&green blue:&blue alpha:&alpha];
    BOOL isReddish = ( (red > green) && (red > blue) );
    STAssertTrue(isReddish, @"Returned color for NO is not reddish");
}

- (void)testThatYesReturnsTestColor {
    CCFRegexValidColorTransformer *transformer = [[CCFRegexValidColorTransformer alloc] init];
    NSColor *actual = [transformer transformedValue:@YES];
    STAssertTrue([actual isEqual:[NSColor textColor]], @"Returned wrong color for YES");
}

@end
