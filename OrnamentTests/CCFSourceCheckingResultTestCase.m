#import "CCFTestCase.h"
#import "CCFSourceCheckingResult.h"

@interface CCFSourceCheckingResultTestCase : CCFTestCase {
    CCFSourceCheckingResult *_result;
}

@end


@implementation CCFSourceCheckingResultTestCase

#pragma mark - Test lifecyle

- (void)setUp {
    [super setUp];
    _result = [[CCFSourceCheckingResult alloc] init];
}

- (void)tearDown {
    _result = nil;
    [super tearDown];
}

#pragma mark - Properties

- (void)testThatCCFSourceCheckingResultHasSourceProperty {
    objc_property_t property = class_getProperty([CCFSourceCheckingResult class], "source");
    STAssertTrue(property != NULL, @"CCFSourceCheckingResult has no source property");
}

- (void)testThatCCFSourceCheckingResultHasIndexProperty {
    objc_property_t property = class_getProperty([CCFSourceCheckingResult class], "index");
    STAssertTrue(property != NULL, @"CCFSourceCheckingResult has no index property");
}

- (void)testThatCCFSourceCheckingResultHasTitleProperty {
    objc_property_t property = class_getProperty([CCFSourceCheckingResult class], "title");
    STAssertTrue(property != NULL, @"CCFSourceCheckingResult has no title property");
}

- (void)testThatCCFSourceCheckingResultHasRangeStringProperty {
    objc_property_t property = class_getProperty([CCFSourceCheckingResult class], "rangeString");
    STAssertTrue(property != NULL, @"CCFSourceCheckingResult has no rangeString property");
}

- (void)testThatCCFSourceCheckingResultHasMatchedTextProperty {
    objc_property_t property = class_getProperty([CCFSourceCheckingResult class], "matchedText");
    STAssertTrue(property != NULL, @"CCFSourceCheckingResult has no matchedText property");
}

#pragma mark - Behaviors

- (void)testThatWeCanCreateSourceCheckingResultWithSourceAndIndex {
    _result = [[CCFSourceCheckingResult alloc] initWithSource:@"blah" index:2];
    STAssertTrue([[_result source] isEqualToString:@"blah"], @"Incorrect source from init");
    STAssertTrue([_result index] == 2, @"Incorrect index from init");
}

@end
