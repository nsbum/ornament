/**
 *   @file CCFCodeFormatterTestCase.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 10/25/12   1:43 PM
 *
 *   @note Copyright 2012 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFTestCase.h"
#import "CCFCodeFormatter.h"
#import "CCFRegex.h"
#import "CCFRegexOptions.h"
#import <CCFBase/CCFBase.h>


BOOL compareCodeString(id self, NSString *expectedFileName, NSString *codeString) {
    NSString *expectedPath = resourcePath(self, expectedFileName, @"txt");
    NSString *expectedString = [[NSString alloc] initWithContentsOfFile:expectedPath encoding:NSUTF8StringEncoding error:nil];
    return [codeString isEqualToString:expectedString];
}

CCFCodeFormatter *formatterWithType(CCFCodeType type, NSString *expression, NSRegularExpressionOptions options) {
    CCFRegex *regex = [[CCFRegex alloc] initWithString:expression];
    
    //  options
    CCFRegexOptions *opts = [CCFRegexOptions optionsObjectWithOptions:options];
    regex.options = opts;
    
    CCFCodeFormatter *formatter = [[CCFCodeFormatter alloc] initWithCodeType:type];
    formatter.regex = regex;
    return formatter;
}

void WithSubstitutionString(NSString *subText, BasicBlock block) {
    if( subText && [subText length] != 0 ) {
        [[CCFOrnamentData sharedData] setIsMatching:NO];
        [[CCFOrnamentData sharedData] setSubstitutionString:subText];
    }
    
    block();
    
    [[CCFOrnamentData sharedData] setIsMatching:YES];
    [[CCFOrnamentData sharedData] setSubstitutionString:@""];
}

 @interface CCFCodeFormatterTestCase : CCFTestCase {
 	CCFCodeFormatter *_formatter;
 }

 @end

@implementation CCFCodeFormatterTestCase

#pragma mark - Test lifecycle

 - (void)setUp {
 	[super setUp];
 	_formatter = [[CCFCodeFormatter alloc] init];
 }

 - (void)tearDown {
 	_formatter = nil;
 	[super tearDown];
 }

 #pragma mark - Properties

- (void)testThatCodeFormatterHasCodeStringProperty {
    objc_property_t property = class_getProperty([CCFCodeFormatter class], "codeString");
    STAssertTrue(property != NULL, @"CCFCodeFormatter has no codeString property");
}

- (void)testThatCodeFormatterHasRegexProperty {
    objc_property_t property = class_getProperty([CCFCodeFormatter class], "regex");
    STAssertTrue(property != NULL, @"CCFCodeFormatter has no regex property");
}

 #pragma mark - Behaviors

- (void)testThatWeReturnPrivateObjCClassForType {
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypeObjC];
    NSString *actualClassName = NSStringFromClass([_formatter class] );
    STAssertTrue([actualClassName isEqualToString:@"_CCFCodeFormatterObjC"], @"Incorrect private class returned for ObjC type");
}

- (void)testThatWeReturnPrivatePerlClassForType {
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePerl];
    NSString *actualClassName = NSStringFromClass([_formatter class] );
    STAssertTrue([actualClassName isEqualToString:@"_CCFCodeFormatterPerl"], @"Incorrect private class returned for Perl type");
}

- (void)testethatWeReturnPrivatePythonClassForType {
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePython];
    NSString *actualClassName = NSStringFromClass([_formatter class] );
    STAssertTrue([actualClassName isEqualToString:@"_CCFCodeFormatterPython"], @"Incorrect private class return for Python type");
}

- (void)testthatWeReturnPrivatePHPClassForType {
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    NSString *actualClassName = NSStringFromClass([_formatter class] );
    STAssertTrue([actualClassName isEqualToString:@"_CCFCodeFormatterPHP"], @"Incorrect private class returned for PHP type");
}

- (void)testThatWeCanReturnSimpleCodeOnlyPerlRegex {
    //  create our regex
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  set its options (no options this time)
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:0];
    regex.options = option;
    //  create our code formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePerl];
    [_formatter setRegex:regex];
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-perl-match" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple Perl example - failing %@",codeString);
}

- (void)testThatWeCanReturnSimpleCodeOnlyPythonRegex {
    //  save default then write new default
    BOOL pushedCompileFlag = [[NSUserDefaults standardUserDefaults] boolForKey:kPythonCompileRegex];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kPythonCompileRegex];
    //  create our regex
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  set its options (no options this time)
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:0];
    regex.options = option;
    //  create our code formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePython];
    [_formatter setRegex:regex];
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-python-uncompiled" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple python example");
    //  restore the original default
    [[NSUserDefaults standardUserDefaults] setBool:pushedCompileFlag forKey:kPythonCompileRegex];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)testThatWeCanReturnPythonCodeWithCompileOption {
    //  save default then write new default
    BOOL pushedCompileFlag = [[NSUserDefaults standardUserDefaults] boolForKey:kPythonCompileRegex];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kPythonCompileRegex];
    //  create a regex
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  set its options (none this time)
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:0];
    regex.options = option;
    //  create our formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePython];
    [_formatter setRegex:regex];
    //  get our returned code
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-python-compiled" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([codeString isEqualToString:expectedString], @"Incorrect compiled python code example.");
    //  restore the original default
    [[NSUserDefaults standardUserDefaults] setBool:pushedCompileFlag forKey:kPythonCompileRegex];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)testThatWeCanReturnPythonCodeWithNoCompileCaseInsensitive {
    //  for this test, we'll set the "use sample text" default to NO
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseSampleTextInCode];
    //  save default then write new default
    BOOL pushedCompileFlag = [[NSUserDefaults standardUserDefaults] boolForKey:kPythonCompileRegex];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kPythonCompileRegex];
    //  create our regex
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  set its options (no options this time)
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive];
    regex.options = option;
    //  create our code formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePython];
    [_formatter setRegex:regex];
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-python-uncompiled-case-insensitive" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple python example with re.I option");
    //  restore the original default
    [[NSUserDefaults standardUserDefaults] setBool:pushedCompileFlag forKey:kPythonCompileRegex];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)testThatWeCanReturnPythonCodeWithCompileCaseInsensitive {
    //  save default then write new default
    BOOL pushedCompileFlag = [[NSUserDefaults standardUserDefaults] boolForKey:kPythonCompileRegex];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kPythonCompileRegex];
    //  create our regex
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  set its options (no options this time)
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive];
    regex.options = option;
    //  create our code formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePython];
    [_formatter setRegex:regex];
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-python-compiled-case-insensitive" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple python example with re.I option, compiled");
    //  restore the original default
    [[NSUserDefaults standardUserDefaults] setBool:pushedCompileFlag forKey:kPythonCompileRegex];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)testThatWeCanReturnPythonCodeUncompiledWithMultipleOptions {
    //  save default then write new default
    BOOL pushedCompileFlag = [[NSUserDefaults standardUserDefaults] boolForKey:kPythonCompileRegex];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kPythonCompileRegex];
    //  create our regex
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  set its options (no options this time)
    CCFRegexOptions *option = [CCFRegexOptions optionsObjectWithOptions:(NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace)];
    regex.options = option;
    //  create our code formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePython];
    [_formatter setRegex:regex];
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-python-uncompiled-multiple-options" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple python example with re.I and re.X option");
    //  restore the original default
    [[NSUserDefaults standardUserDefaults] setBool:pushedCompileFlag forKey:kPythonCompileRegex];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark PHP

- (void)testThatWeCanReturnPHPCodeForMatch {
    //  for this test, we'll set the "use sample text" default to NO
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseSampleTextInCode];
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  no options
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [_formatter setRegex:regex];
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-php-match-no-options" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple PHP match expression");
}

- (void)testThatWeCanReturnPHPCodeForMatchWithOptions {
    //  for this test, we'll set the "use sample text" default to NO
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseSampleTextInCode];
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];

    //  options
    CCFRegexOptions *opts = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive];
    regex.options = opts;
    
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [_formatter setRegex:regex];
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-php-match-case-insensitive" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple PHP match expression");
}

- (void)textThatWeCanReturnPHPCodeForMatchWithMultipleOptions {
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    
    //  options
    CCFRegexOptions *opts = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace];
    regex.options = opts;
    
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [_formatter setRegex:regex];
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-php-match-multiple-options" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple PHP match expression");
}

- (void)testThatWeCanReturnPHPCodeForSubstitutionWithoutOptions {
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    
    //  no options
    
    //  not matching
    [[CCFOrnamentData sharedData] setIsMatching:NO];
    [[CCFOrnamentData sharedData] setSubstitutionString:@"cat"];
    
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [_formatter setRegex:regex];
    
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-php-substitute-no-options" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple PHP substitution expression");
    
    //  reset matching
    [[CCFOrnamentData sharedData] setIsMatching:YES];
}

- (void)testThatWeCanReturnPHPCodeForSubstitutionWithOptions {
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    
    //  options
    CCFRegexOptions *opts = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace];
    regex.options = opts;
    
    //  not matching
    [[CCFOrnamentData sharedData] setIsMatching:NO];
    [[CCFOrnamentData sharedData] setSubstitutionString:@"cat"];
    
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [_formatter setRegex:regex];
    
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-php-substitute-multiple-options" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple PHP substitution expression");
    
    //  reset matching
    [[CCFOrnamentData sharedData] setIsMatching:YES];
}

#pragma mark Ruby

- (void)testThatWeCanReturnRubyCodeForMatchWithoutOptions {
    //  for this test, we'll set the "use sample text" default to NO
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseSampleTextInCode];
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    //  no options
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypeRuby];
    [_formatter setRegex:regex];
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-ruby-match-no-options" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple Ruby match expression");
}

- (void)testThatWeCanReturnRubyCodeForMatchWithMultipleOptions {
    //  for this test, we'll set the "use sample text" default to NO
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseSampleTextInCode];
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    
    //  options
    CCFRegexOptions *opts = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace];
    regex.options = opts;
    
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypeRuby];
    [_formatter setRegex:regex];
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-ruby-match-multiple-options" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple Ruby match expression");
}

- (void)testThatWeCanReturnRubyCodeForSubstitutionWithoutOptions {
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    
    //  no options
    
    //  not matching
    [[CCFOrnamentData sharedData] setIsMatching:NO];
    [[CCFOrnamentData sharedData] setSubstitutionString:@"cat"];
    
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypeRuby];
    [_formatter setRegex:regex];
    
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-ruby-substitute-no-options" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple Ruby substitution expression");
    
    //  reset matching
    [[CCFOrnamentData sharedData] setIsMatching:YES];
    [[CCFOrnamentData sharedData] setSubstitutionString:@""];
}

- (void)testThatWeCanReturnRubyCodeForSubstitutionWithOptions {
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    
    //  options
    CCFRegexOptions *opts = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace];
    regex.options = opts;
    
    //  not matching
    [[CCFOrnamentData sharedData] setIsMatching:NO];
    [[CCFOrnamentData sharedData] setSubstitutionString:@"cat"];
    
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypeRuby];
    [_formatter setRegex:regex];
    
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    
    NSString *expectedStringPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-ruby-substitute-multiple-options" ofType:@"txt"];
    NSString *expectedString = [NSString stringWithContentsOfFile:expectedStringPath encoding:NSUTF8StringEncoding error:nil];
    STAssertTrue([expectedString isEqualToString:codeString], @"Incorrect code string for simple Ruby substitution expression");
    
    //  reset matching
    [[CCFOrnamentData sharedData] setIsMatching:YES];
    [[CCFOrnamentData sharedData] setSubstitutionString:@""];
}

#pragma mark JavaScript

- (void)testThatWeCanReturnJavaScriptCodeForMatch {
    //  for this test, we'll set the "use sample text" default to NO
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseSampleTextInCode];
    WithSubstitutionString(nil, ^{
        _formatter = formatterWithType(CCFCodeTypeJavaScript, @"[A-Za-z]{3}", 0);
        STAssertTrue(compareCodeString(self, @"expected-js-match-no-options", _formatter.codeString), @"Incorrect code string for simple JS match w/o options");
    });
    
}

- (void)testThatWeCanReturnJavaScriptCodeForMatchWithOptions {
    //  for this test, we'll set the "use sample text" default to NO
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseSampleTextInCode];
    WithSubstitutionString(nil, ^{
        _formatter = formatterWithType(CCFCodeTypeJavaScript, @"[A-Za-z]{3}", NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace);
        STAssertTrue(compareCodeString(self, @"expected-js-match-multiple-options", _formatter.codeString), @"Incorrect code str for JS match with options");
    });
}

- (void)testThatWeCanReturnJavaScriptCodeForSubstitutionWithoutOptions {
    WithSubstitutionString(@"cat", ^{
        _formatter = formatterWithType(CCFCodeTypeJavaScript, @"[A-Za-z]{3}", 0);
        STAssertTrue(compareCodeString(self, @"expected-js-substitute-no-options", _formatter.codeString), @"Inoccrect code string for JS sub without options");
    });
}

- (void)testThatWeCanReturnJavaScriptCodeForSubstitutionWithMultipleOptions {
    WithSubstitutionString(@"cat", ^{
        _formatter = formatterWithType(CCFCodeTypeJavaScript, @"[A-Za-z]{3}", NSRegularExpressionCaseInsensitive | NSRegularExpressionAllowCommentsAndWhitespace);
        STAssertTrue(compareCodeString(self, @"expected-js-substitute-multiple-options", _formatter.codeString), @"Incorrect code string for JS sub with options");
    });
}

#pragma mark ObjC

- (void)testThatWeCanReturnObjcCodeForMathWithoutOptions {
    //  for this test, we'll set the "use sample text" default to NO
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseSampleTextInCode];
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    
    //  no options
    
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypeObjC];
    _formatter.regex = regex;
    
    //  get our expected string
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-objc-match-no-options" ofType:@"txt"];
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    STAssertTrue([codeString isEqualToString:text], @"Incorrect code string for simple ObjC match");
}

- (void)testThatWeCanReturnObjcCodeForMatchWithOptions {
    //  for this test, we'll set the "use sample text" default to NO
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseSampleTextInCode];
    NSString *regexString = @"[A-Za-z]{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    
    //  options
    CCFRegexOptions *opts = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive];
    regex.options = opts;
    
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypeObjC];
    _formatter.regex = regex;
    
    //  get our expected string
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-objc-match-single-option" ofType:@"txt"];
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    STAssertTrue([codeString isEqualToString:text], @"Incorrect code string for simple ObjC match");
}

- (void)testThatWeCanReturnObjcCodeForMatchWithBackslashWithoutOptions {
    NSString *regexString = @"\\D{3}";
    CCFRegex *regex = [[CCFRegex alloc] initWithString:regexString];
    
    //  no options
    
    //  create formatter and set its regex
    _formatter = [[CCFCodeFormatter alloc] initWithCodeType:CCFCodeTypeObjC];
    _formatter.regex = regex;
    
    //  get our expected string
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"expected-objc-match-backslashes-no-options" ofType:@"txt"];
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    //  get our code string and assert
    NSString *codeString = [_formatter codeString];
    STAssertTrue([codeString isEqualToString:text], @"Incorrect code string for simple ObjC match");
}

@end
