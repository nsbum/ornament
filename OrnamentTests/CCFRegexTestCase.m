#import "CCFTestCase.h"
#import "CCFRegex.h"

@interface CCFRegexTestCase : CCFTestCase {
    CCFRegex *_regex;
}

@end

@implementation CCFRegexTestCase

#pragma mark - Test lifecycle

- (void)setUp {
    [super setUp];
    
    _regex = [[CCFRegex alloc] initWithString:@""];
}

- (void)tearDown {
    _regex = nil;
    [super tearDown];
}

#pragma mark - Properties

- (void)testThatRegexHasStringProperty {
    objc_property_t property = class_getProperty([CCFRegex class], "string");
    STAssertTrue(property != NULL, @"CCFRegex has no string property");
}

- (void)testThatRegexHasReplacementStringProperty {
    objc_property_t property = class_getProperty([CCFRegex class], "replacementString");
    STAssertTrue(property != NULL, @"CCFRegex has no replacementString property");
}

- (void)testThatRegexHasExpressionProperty {
    objc_property_t property = class_getProperty([CCFRegex class], "expression");
    STAssertTrue(property != NULL, @"CCFRegex has no expression property");
}

- (void)testThatRegexHasOptionsProperty {
    objc_property_t property = class_getProperty([CCFRegex class], "options");
    STAssertTrue(property != NULL, @"CCFRegex has no options property");
}

- (void)testThatRegexHasIsMatchingProperty {
    objc_property_t property = class_getProperty([CCFRegex class], "isMatching");
    STAssertTrue(property != NULL, @"CCFRegex has no isMatching property");
}

#pragma mark - Behaviors

- (void)testThatRegexDefaultIsToMatch {
    STAssertTrue(_regex.isMatching, @"Regex default is not matching as it should be");
}

- (void)testThatAValidRegexStringIsReportedAsValid {
    _regex.string = @"[A-Za-z]+\\d{3}";
    STAssertTrue([_regex regexIsValid], @"Valid regex string reported as invalid");
}

- (void)testThatAnInvalidRegexStringIsReportedAsInvalid {
    _regex.string = @"[A-Za-z\\d{2";
    STAssertTrue(![_regex regexIsValid], @"Invalid regex string is reported as valid");
}

- (void)testThatWeHaveANonZeroMatchCountWithValidRegexAndSource {
    _regex.string = @"[A-Za-z]+";
    [_regex regexIsValid];
    NSArray *matches = [_regex matchesWithSourceText:@"The quick brown fox"];
    STAssertTrue([matches count] > 0, @"Incorrect number of matches reported");
}

- (void)testThatWeHaveNilMatchesWithInvalidRegex {
    _regex.string = @"[A-Za-z";
    [_regex regexIsValid];
    NSArray *matches = [_regex matchesWithSourceText:@"The mentally-handicapped dog"];
    STAssertNil(matches, @"Non-nil matches reported with invalid regex");
}

#pragma mark Replacements

- (void)testThatWeCanDoSimpleReplacement {
    _regex.string = @"dog";
    //  must validate before we use
    [_regex regexIsValid];
    _regex.replacementString = @"cat";
    NSString *actual = [_regex stringWithReplacementsInString:@"I have a red dog."];
    STAssertTrue([actual isEqualToString:@"I have a red cat."], @"Incorrect replacement in simple regex");
}

- (void)testThatWeCanDoReplacementWithOptions {
    _regex.string = @"dog";
    CCFRegexOptions *options = [CCFRegexOptions optionsObjectWithOptions:NSRegularExpressionCaseInsensitive];
    _regex.options = options;
    [_regex regexIsValid];
    _regex.replacementString = @"cat";
    NSString *actual = [_regex stringWithReplacementsInString:@"I have a red DOG.  But my dOg is not very nice."];
    NSString *expected = @"I have a red cat.  But my cat is not very nice.";
    STAssertTrue([actual isEqualToString:expected], @"Incorrect replace with case insensitive.");
}

@end
