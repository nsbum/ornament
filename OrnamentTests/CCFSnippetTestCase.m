#import "CCFTestCase.h"
#import "CCFSnippet.h"

@interface CCFSnippetTestCase : CCFTestCase {
    CCFSnippet *_snippet;
}

@end


@implementation CCFSnippetTestCase

#pragma mark - Test lifecycle

- (void)setUp {
    [super setUp];
    _snippet = [[CCFSnippet alloc] init];
}

- (void)tearDown {
    _snippet = nil;
    [super tearDown];
}

#pragma mark - Properties

- (void)testThatCCFSnippetHasDisplayStringProperty {
    objc_property_t property = class_getProperty([CCFSnippet class], "displayString");
    STAssertTrue(property != NULL, @"CCFSnippet has no displayString property");
}

- (void)testThatCCFSnippetHasExplanationProperty {
    objc_property_t property = class_getProperty([CCFSnippet class], "explanation");
    STAssertTrue(property != NULL, @"CCFSnippet has no explanation property");
}

- (void)testThatCCFSnippetHasSnipProperty {
    objc_property_t property = class_getProperty([CCFSnippet class], "snip");
    STAssertTrue(property != NULL, @"CCFSnippet has no snip property");
}

- (void)testThatCCFSnippetHasTagProperty {
    objc_property_t property = class_getProperty([CCFSnippet class], "tag");
    STAssertTrue(property != NULL, @"CCFSnippet has no tag property");
}

@end
