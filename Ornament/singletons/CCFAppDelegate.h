//
//  CCFAppDelegate.h
//  Ornament
//
//  Created by alanduncan on 10/22/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CCFMenuManager,CCFMainWindowController;

@interface CCFAppDelegate : NSObject <NSApplicationDelegate>

//@property (assign) IBOutlet NSWindow *window;
@property (nonatomic, strong) IBOutlet CCFMainWindowController *windowController;
@property (nonatomic, weak) IBOutlet NSMenu *loadSnapshot;
@property (nonatomic, strong) IBOutlet  CCFMenuManager *menuManager;


@end
