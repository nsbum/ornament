/**
 *   @file CCFOrnamentData.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 14:29:12
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFOrnamentData.h"
//  helper
#import "CCFSnapshotSeeder.h"

static CCFOrnamentData *_sharedData;

@implementation CCFOrnamentData

+ (id)sharedData {
    return _sharedData;
}

- (id)init {
    @synchronized(self) {
        if( _sharedData == nil ) {
            _sharedData = [[CCFOrnamentData alloc] initCompletion];
            _snapshots = [[NSMutableArray alloc] init];
            
            //  when someone changes our sample text, we'll record the change here
            __block id sampleTextChanged = nil;
            sampleTextChanged = [[NSNotificationCenter defaultCenter] addObserverForName:kDidChangeSampleTextNotification
                                                                                  object:nil
                                                                                   queue:nil
                                                                              usingBlock:^(NSNotification *note) {
                                                                                  _sampleText = [note userInfo][kSampleTextStringKey];
                                                                              }];
            
            //  take note when someone changes the regex string
            __block id regexStringChanged = nil;
            regexStringChanged = [[NSNotificationCenter defaultCenter] addObserverForName:kDidChangeRegexNotification
                                                                                   object:nil
                                                                                    queue:nil
                                                                               usingBlock:^(NSNotification *note) {
                                                                                   _regexString = [note userInfo][kRegexStringKey];
                                                                               }];
            
            //  when someone changes our substitution text, record the change
            __block id substitutionTextChanged = nil;
            substitutionTextChanged = [[NSNotificationCenter defaultCenter] addObserverForName:kDidChangeSubstituteTextNotification
                                                                                        object:nil
                                                                                         queue:nil
                                                                                    usingBlock:^(NSNotification *note) {
                                                                                        _substitutionString = [note userInfo][kSubstituteStringKey];
                                                                                    }];
            if( ![[NSUserDefaults standardUserDefaults] boolForKey:kDidSeedSnapshots] ) {
                //[self seedSnapshots];
                //NSLog(@"%s - snapshots count = %ld",__FUNCTION__,[[self snapshots] count]);
            }
        }
    }
    return _sharedData;
}

- (id)initCompletion {
    self = [super init];
    if( !self ) return nil;
    
    return self;
}

#pragma mark - Accessors

- (NSMutableArray *)snapshots {
    if( !_snapshots ) {
        _snapshots = [[NSMutableArray alloc] init];
    }
    return _snapshots;
}

#pragma mark - Public

- (void)addSnapshot:(id)snapshot {
    [[self snapshots] addObject:snapshot];
}

#pragma mark - Private

- (void)loadSnapshots {
    BOOL alreadySeeded = [[NSUserDefaults standardUserDefaults] boolForKey:kDidSeedSnapshots];
    if( !alreadySeeded ) {
        [self seedSnapshots];
        NSLog(@"%s - snapshots count = %ld",__FUNCTION__,[[self snapshots] count]);
    }
    NSURL *snapshotURL = [[self applicationFilesDirectory] URLByAppendingPathComponent:@"snapshots.dat"];
    NSArray *snapshotObjects = nil;
    @try {
        snapshotObjects = (NSArray *)[NSKeyedUnarchiver unarchiveObjectWithFile:snapshotURL.path];
    }
    @catch (NSException *exception) {
        NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Unable to load Snapshots", @"snapshot load error alert title")
                                         defaultButton:@"OK"
                                       alternateButton:nil
                                           otherButton:nil
                             informativeTextWithFormat:NSLocalizedString(@"The Snapshot archive file is either missing or corrupt.  If this error continues to occur please report to developer.", @"snapshot load error alert explanatory text")];
        [alert runModal];
    }
    @finally {
        [[self snapshots] addObjectsFromArray:snapshotObjects];
    }

}

- (void)saveSnapshots {
    NSURL *snapshotURL = [[self applicationFilesDirectory] URLByAppendingPathComponent:@"snapshots.dat"];
    if( ![[NSFileManager defaultManager] fileExistsAtPath:[snapshotURL path]] ) {
        BOOL didCreateFile = [[NSFileManager defaultManager] createFileAtPath:[snapshotURL path] contents:nil attributes:nil];
        if( !didCreateFile ) {
            NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Unable to create Snapshot file", @"snapshot file creation alert title")
                                             defaultButton:@"OK"
                                           alternateButton:nil
                                               otherButton:nil
                                 informativeTextWithFormat:NSLocalizedString(@"RegexMatch encountered an error while trying to create Snapshot file.", @"snapshot file creation alert explanatory text")];
            [alert runModal];
            return;
        }
    }
    NSData *archiveData = [NSKeyedArchiver archivedDataWithRootObject:self.snapshots];
    NSError *archiveError = nil;
    if( ![archiveData writeToURL:snapshotURL options:NSDataWritingAtomic error:&archiveError] ) {
        NSLog(@"Error while writing snapshots: %@ : %@",archiveError, [archiveError userInfo] );
        NSAlert *alert = [NSAlert alertWithError:archiveError];
        [alert runModal];
    }
}

- (NSString *)sampleTextWithNewlinesEscaped {
    return [[self sampleText] stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
}

- (NSURL *)applicationFilesDirectory
{
    NSString *sandboxPath = NSHomeDirectory();
    return [NSURL fileURLWithPath:sandboxPath];
}

- (void)seedSnapshots {
    CCFSnapshotSeeder *seeder = [[CCFSnapshotSeeder alloc] init];

    [seeder importWithBlock:^(CCFSnapshot *snapshot) {
        if( snapshot ) {
            [self addSnapshot:snapshot];
        }
        else {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kDidSeedSnapshots];
        }
    }];
}


@end
