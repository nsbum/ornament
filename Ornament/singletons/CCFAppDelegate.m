//
//  CCFAppDelegate.m
//  Ornament
//
//  Created by alanduncan on 10/22/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFAppDelegate.h"
#import "DMTabBar.h"

#import "CCFMainWindowController.h"
//  refs for preferences window
#import "MASPreferencesWindowController.h"
#import "CCFHighlightPrefsViewController.h"
#import "CCFCodePrefsViewController.h"
#import "CCFManageSnapshotsWindowController.h"
#import "CCFLoadSnapshotViewController.h"
#import "CCFGeneralPreferencesViewController.h"

#import "CCFRatingReminder.h"

@implementation CCFAppDelegate {
    MASPreferencesWindowController *_prefsWindowController;
    CCFManageSnapshotsWindowController *_manageSnapshotsController;
}

- (id)init {
    self = [super init];
    if( !self ) return nil;
    
    [self _registerUserDefaults];
    
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //  see if we should present rating reminder
        [[CCFRatingReminder sharedReminder] remindIfNeeded];
    });

    return self;
}

- (void)awakeFromNib {
    [[self windowController] showWindow:self];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"NSConstraintBasedLayoutVisualizeMutuallyExclusiveConstraints"];
    [[CCFOrnamentData sharedData] loadSnapshots];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag  {
    if( !flag ) {
        [[self windowController] showWindow:self];
        return NO;
    }
    return YES;
}

- (NSURL *)applicationFilesDirectory
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [[fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL URLByAppendingPathComponent:@"com.cocoafactory.Ornament"];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    [[CCFOrnamentData sharedData] saveSnapshots];

    return NSTerminateNow;
}

#pragma mark - Interface actions

- (IBAction)manageSnapshotsAction:(id)sender {
    [[self manageSnapshotsController] showWindow:self];
}

- (IBAction)loadSnapshotAction:(id)sender {
    
}

#pragma mark - Accessors

- (MASPreferencesWindowController *)prefsWindowController {
    if( !_prefsWindowController ) {
        CCFHighlightPrefsViewController *highlightPrefsController = [[CCFHighlightPrefsViewController alloc] initWithNib];
        CCFCodePrefsViewController *codePrefsController = [[CCFCodePrefsViewController alloc] initWithNib];
        CCFGeneralPreferencesViewController *generalPrefsController = [[CCFGeneralPreferencesViewController alloc] initWithNib];
        NSArray *controllers = @[generalPrefsController,codePrefsController,highlightPrefsController];
        _prefsWindowController = [[MASPreferencesWindowController alloc] initWithViewControllers:controllers];
    }
    return _prefsWindowController;
}

- (CCFManageSnapshotsWindowController *)manageSnapshotsController {
    if( !_manageSnapshotsController ) {
        _manageSnapshotsController = [[CCFManageSnapshotsWindowController alloc] initWithNib];
    }
    return _manageSnapshotsController;
}

#pragma mark - Private

static void insertColorIntoDictionary(NSColor *color,NSMutableDictionary *dict, NSString *key) {
    dict[key] = [NSKeyedArchiver archivedDataWithRootObject:color];
}


- (void)_registerUserDefaults {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *defaultsPath = [bundle pathForResource:@"CCFInitialUserDefaults" ofType:@"plist"];
    if( !defaultsPath ) {
        //  deal with error loading defaults
        NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Error loading user defaults", @"error loading defaults msg")
                                         defaultButton:@"OK"
                                       alternateButton:nil
                                           otherButton:nil
                             informativeTextWithFormat:NSLocalizedString(@"The application must terminate because defaults are unavailable.  Please note this error and contact the developer.", @"user default loading explanatory text")];
        [alert runModal];
        [NSApp terminate:self];
    }
    
    
    NSMutableDictionary *initialDefaults = nil;
    initialDefaults = [NSMutableDictionary dictionaryWithDictionary:[NSDictionary dictionaryWithContentsOfFile:defaultsPath]];
    
    //  setup some default colors, though
    insertColorIntoDictionary([NSColor colorWithCalibratedRed:0.908 green:0.379 blue:0.236 alpha:1.000], initialDefaults, kQuantifierColorDataKey);
    insertColorIntoDictionary([NSColor colorWithCalibratedRed:0.185 green:0.413 blue:0.575 alpha:1.000], initialDefaults, kGroupingColorDataKey);
    insertColorIntoDictionary([NSColor colorWithCalibratedRed:0.713 green:0.207 blue:0.504 alpha:1.000], initialDefaults, kCharClassColorDataKey);
    insertColorIntoDictionary([NSColor colorWithCalibratedRed:0.709 green:0.692 blue:0.369 alpha:1.000], initialDefaults, kMetacharacterColorDataKey);
    insertColorIntoDictionary([NSColor colorWithCalibratedRed:0.506 green:0.751 blue:0.344 alpha:1.000], initialDefaults, kNumberedGroupColorDataKey);
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:initialDefaults];
    
    NSInteger launchCount = [[NSUserDefaults standardUserDefaults] integerForKey:kLaunchCount];
    if( launchCount == 0 ) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:kFirstLaunchDate];
    }
    [[NSUserDefaults standardUserDefaults] setInteger:++launchCount forKey:kLaunchCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (IBAction)showPreferencesWindow:(id)sender {
    [[self prefsWindowController] showWindow:self];
}

@end
