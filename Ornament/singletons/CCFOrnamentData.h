/**
 *   @file CCFOrnamentData.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 14:28:38
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import <CCFBase/CCFBase.h>

@class CCFRegex;

@interface CCFOrnamentData : CCFObject

@property (nonatomic, strong) CCFRegex *regex;
@property (nonatomic, copy) NSString *regexString;
@property (nonatomic, copy) NSString *sampleText;
@property (nonatomic, copy) NSString *substitutionString;
@property (nonatomic, strong) NSString *substitutedSampleText;
@property (nonatomic, assign) NSRegularExpressionOptions currentOptions;

@property (nonatomic, assign) BOOL isMatching;

@property (nonatomic, strong) NSMutableArray *snapshots;

@property (nonatomic, strong) NSAttributedString *sampleAttributedString;

+ (id)sharedData;

- (void)addSnapshot:(id)snapshot;
- (void)loadSnapshots;
- (void)saveSnapshots;
- (NSString *)sampleTextWithNewlinesEscaped;

@end
