/**
 *   @file CCFRegexOptions.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 14:40:21
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFRegexOptions.h"

@implementation CCFRegexOptions

+ (id)optionsObjectWithOptions:(NSRegularExpressionOptions)options {
    CCFRegexOptions *optionsObject = [[CCFRegexOptions alloc] init];
    
    optionsObject.caseInsensitive = (options & NSRegularExpressionCaseInsensitive);
    optionsObject.verbose = (options & NSRegularExpressionAllowCommentsAndWhitespace);
    optionsObject.multiLine = (options & NSRegularExpressionAnchorsMatchLines);
    optionsObject.singleLine = (options & NSRegularExpressionDotMatchesLineSeparators);
    
    return optionsObject;
}

- (NSRegularExpressionOptions)regexOptions {
    NSRegularExpressionOptions options = 0;
    if( self.caseInsensitive ) {
        options |= NSRegularExpressionCaseInsensitive;
    }
    if( self.verbose ) {
        options |= NSRegularExpressionAllowCommentsAndWhitespace;
    }
    if( self.multiLine ) {
        options |= NSRegularExpressionAnchorsMatchLines;
    }
    if( self.singleLine ) {
        options |= NSRegularExpressionDotMatchesLineSeparators;
    }
    
    return options;
}

@end