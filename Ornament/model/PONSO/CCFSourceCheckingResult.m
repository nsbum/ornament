/**
 *   @file CCFSourceCheckingResult.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 04:31:28
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSourceCheckingResult.h"

@implementation CCFSourceCheckingResult

- (id)initWithSource:(NSString *)source index:(NSInteger)index {
    self = [super init];
    if( !self ) return nil;
    
    _source = source;
    _index = index;
    
    return self;
}

#pragma mark - Accessors



@end
