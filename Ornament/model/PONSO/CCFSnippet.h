//
//  CCFSnippet.h
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCFSnippet : NSObject

@property (nonatomic, copy) NSString *displayString;
@property (nonatomic, copy) NSString *explanation;
@property (nonatomic, copy) NSString *snip;
@property (nonatomic, assign) NSInteger tag;

@end
