/**
 *   @file CCFSnapshot.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-27 08:44:31
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSnapshot.h"

static NSString * const RegexStringKey = @"regexString";
static NSString * const RegexOptionsKey = @"regexOptions";
static NSString * const SampleTextKey = @"sampleText";
static NSString * const ReplacementTextKey = @"replacementText";
static NSString * const DescriptorTextKey = @"descriptor";

@implementation CCFSnapshot

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if( !self ) return nil;
    
    _regexString = [aDecoder decodeObjectForKey:RegexStringKey];
    _regexOptions = [aDecoder decodeIntegerForKey:RegexOptionsKey];
    _sampleText = [aDecoder decodeObjectForKey:SampleTextKey];
    _replacementText = [aDecoder decodeObjectForKey:ReplacementTextKey];
    _descriptor = [aDecoder decodeObjectForKey:DescriptorTextKey];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_regexString forKey:RegexStringKey];
    [aCoder encodeInteger:_regexOptions forKey:RegexOptionsKey];
    [aCoder encodeObject:_sampleText forKey:SampleTextKey];
    [aCoder encodeObject:_replacementText forKey:ReplacementTextKey];
    [aCoder encodeObject:_descriptor forKey:DescriptorTextKey];
}

@end
