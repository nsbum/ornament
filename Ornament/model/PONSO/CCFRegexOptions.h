/**
 *   @file CCFRegexOptions.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 14:40:30
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */


@interface CCFRegexOptions : NSObject

@property (nonatomic, assign) BOOL caseInsensitive;
@property (nonatomic, assign) BOOL verbose;
@property (nonatomic, assign) BOOL multiLine;
@property (nonatomic, assign) BOOL singleLine;

+ (id)optionsObjectWithOptions:(NSRegularExpressionOptions)options;
- (NSRegularExpressionOptions)regexOptions;


@end
