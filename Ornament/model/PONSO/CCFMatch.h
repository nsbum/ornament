/**
 *   @file CCFMatch.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 04:10:56
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSourceCheckingResult.h"

@interface CCFMatch : CCFSourceCheckingResult

@property (nonatomic, strong) NSTextCheckingResult *result;

@property (nonatomic, readonly) NSArray *groups;

- (id)initWithResult:(NSTextCheckingResult *)result forSourceString:(NSString *)source index:(NSInteger)index ;

@end
