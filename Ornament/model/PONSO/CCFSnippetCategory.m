//
//  CCFSnippetCategory.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFSnippetCategory.h"

@implementation CCFSnippetCategory

- (id)init {
    self = [super init];
    if( !self ) return nil;
    
    _snippets = [NSMutableArray new];
    
    return self;
}

- (void)addSnippet:(CCFSnippet *)snippet {
    [_snippets addObject:snippet];
}

- (CCFSnippet *)snippetWithTag:(NSInteger)tag {
    NSPredicate *tagPredicate = [NSPredicate predicateWithFormat:@"tag == %ld",tag];
    NSArray *matchingSnippets = [_snippets filteredArrayUsingPredicate:tagPredicate];
    if( matchingSnippets && [matchingSnippets count] != 0 ) {
        return matchingSnippets[0];
    }
    return nil;
}

@end
