/**
 *   @file CCFGroup.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 04:29:26
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSourceCheckingResult.h"

@interface CCFGroup : CCFSourceCheckingResult

@property (nonatomic, assign) NSRange range;

- (id)initWithSource:(NSString *)source index:(NSInteger)index range:(NSRange)range;

@end
