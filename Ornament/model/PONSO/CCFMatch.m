/**
 *   @file CCFMatch.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 04:11:05
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFMatch.h"
#import "CCFGroup.h"

@implementation CCFMatch

@synthesize groups = _groups;

- (id)initWithResult:(NSTextCheckingResult *)result forSourceString:(NSString *)source index:(NSInteger)index {
    self = [super initWithSource:source index:index];
    if( !self ) return nil;
    
    _result = result;
    _groups = nil;
    
    return self;
}

#pragma mark - Accessors

- (NSString *)title {
    NSString *template = NSLocalizedString(@"Match %d", @"Match + integer");
    return [NSString stringWithFormat:template,self.index];
}

- (NSString *)rangeString {
    NSRange range = self.result.range;
#if TARGET_OS_IPHONE
    return [NSString stringWithFormat:@"%d-%d",range.location,NSMaxRange(range)-1];
#else
    return [NSString stringWithFormat:@"%ld-%ld",range.location,NSMaxRange(range)-1];
#endif
    
}

- (NSString *)matchedText {
    //TODO: range checking
    NSRange range = self.result.range;
    if( range.location + range.length > self.source.length ) {
        return nil;
    }
    NSString *matched = [[self source] substringWithRange:self.result.range];
    return matched;
}

- (NSArray *)groups {
    NSMutableArray *mutableGroups = [[NSMutableArray alloc] init];
    NSInteger rangeCount = self.result.numberOfRanges;
    
    NSInteger realIndex = 0;
    
    for( NSUInteger index = 0; index < rangeCount; index++ ) {
        NSRange range = [[self result] rangeAtIndex:index];
        if( range.location != NSNotFound ) {
            CCFGroup *group = [[CCFGroup alloc] initWithSource:self.source index:realIndex range:range];
            [mutableGroups addObject:group];
            realIndex++;
        }
    }
    return [NSArray arrayWithArray:mutableGroups];
}


@end
