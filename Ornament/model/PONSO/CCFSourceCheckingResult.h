/**
 *   @file CCFSourceCheckingResult.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 04:31:21
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFSourceCheckingResult : NSObject

@property (nonatomic, copy) NSString *source;
@property (nonatomic, assign) NSInteger index;

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *rangeString;
@property (nonatomic, readonly) NSString *matchedText;

- (id)initWithSource:(NSString *)source index:(NSInteger)index;

@end
