//
//  CCFSnippetCategory.h
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CCFSnippet;

@interface CCFSnippetCategory : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSMutableArray *snippets;

- (void)addSnippet:(CCFSnippet *)snippet;
- (CCFSnippet *)snippetWithTag:(NSInteger)tag;

@end
