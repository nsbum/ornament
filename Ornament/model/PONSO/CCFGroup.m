/**
 *   @file CCFGroup.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 04:29:34
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFGroup.h"

@implementation CCFGroup

- (id)initWithSource:(NSString *)source index:(NSInteger)index range:(NSRange)range {
    self = [super initWithSource:source index:index];
    if( !self ) return nil;
    
    _range = range;
    
    return self;
}

#pragma mark - Accessors

- (NSString *)title {
    NSString *format = NSLocalizedString(@"Group %d",@"Group + integer");
    return [NSString stringWithFormat:format,self.index];
}

- (NSString *)rangeString {
    NSRange range = self.range;
#if TARGET_OS_IPHONE
    return [NSString stringWithFormat:@"%d-%d",range.location,NSMaxRange(range)-1];
#else
    return [NSString stringWithFormat:@"%ld-%ld",range.location,NSMaxRange(range)-1];
#endif
    
}

- (NSString *)matchedText {
    if( self.source == nil ) return nil;
    if( self.range.location == NSNotFound ) return nil;
    if( self.range.location + self.range.length > self.source.length ) return  nil;
    
    NSString *matched = [[self source] substringWithRange:self.range];
    return matched;
}

@end
