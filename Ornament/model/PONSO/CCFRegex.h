//
//  CCFRegex.h
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCFRegexOptions.h"

@interface CCFRegex : NSObject

@property (nonatomic, copy) NSString *string;
@property (nonatomic, copy) NSString *replacementString;
@property (nonatomic, strong) NSRegularExpression *expression;
@property (nonatomic, strong) CCFRegexOptions *options;
@property (nonatomic, assign) BOOL isMatching;

- (id)initWithString:(NSString *)regexString;
- (BOOL)regexIsValid;
- (NSArray *)matchesWithSourceText:(NSString *)source;
- (NSString *)stringWithReplacementsInString:(NSString *)source;

@end
