//
//  CCFRegex.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFRegex.h"
#import "CCFRegexOptions.h"

static NSString *SingleBackslash;
static NSString *DoubleBackslash;

@implementation CCFRegex

+ (void)initialize {
    if( self == [CCFRegex class] ) {
        char asciiBackslash = 0x5C;
        SingleBackslash = [[NSString alloc] initWithBytes:&asciiBackslash length:1 encoding:NSASCIIStringEncoding];
        DoubleBackslash = [NSString stringWithFormat:@"%@%@",SingleBackslash,SingleBackslash];
    }
}

- (id)initWithString:(NSString *)regexString {
    self = [super init];
    if( !self ) return nil;
    
    //_string = [regexString stringByReplacingOccurrencesOfString:SingleBackslash withString:DoubleBackslash];
    _string = regexString;
    //  default is to match
    _isMatching = YES;
    
    return self;
}

- (BOOL)regexIsValid {
    if( self.string.length == 0 ) return YES;
    //NSString *escapedTemplate = [[self string] stringByReplacingOccurrencesOfString:SingleBackslash withString:DoubleBackslash];
    NSError *regexError = nil;
    
    NSRegularExpressionOptions options = (self.options != nil) ? [[self options] regexOptions] : 0;
    self.expression = [NSRegularExpression regularExpressionWithPattern:self.string
                                                                options:options
                                                                  error:&regexError];
    return (regexError == nil);
}

- (NSArray *)matchesWithSourceText:(NSString *)source {
    if( self.expression == nil ) {
        return nil;
    }
    NSRegularExpressionOptions options = (self.options != nil) ? [[self options] regexOptions] : 0;
    return [[self expression] matchesInString:source
                               options:options
                                  range:NSMakeRange(0, source.length)];
}

- (NSString *)stringWithReplacementsInString:(NSString *)source {
    if( !source ) { return  nil; }
    NSRegularExpressionOptions options = (self.options != nil) ? [[self options] regexOptions] : 0;
    NSString *result = [[self expression] stringByReplacingMatchesInString:source
                                                                   options:options
                                                                     range:entireStringRange(source)
                                                              withTemplate:self.replacementString];
    return result;
}

@end
