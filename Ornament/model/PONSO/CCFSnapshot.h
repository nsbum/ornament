/**
 *   @file CCFSnapshot.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-27 08:44:25
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */


@interface CCFSnapshot : NSObject <NSCoding>

@property (nonatomic, copy) NSString *regexString;
@property (nonatomic, assign) NSRegularExpressionOptions regexOptions;
@property (nonatomic, copy) NSString *sampleText;
@property (nonatomic, copy) NSString *replacementText;
@property (nonatomic, copy) NSString *descriptor;

@end
