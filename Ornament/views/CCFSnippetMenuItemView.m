//
//  CCFSnippetMenuItemView.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFSnippetMenuItemView.h"

@implementation CCFSnippetMenuItemView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    if( [[self enclosingMenuItem] isHighlighted] ) {
        [[NSColor selectedMenuItemColor] set];
        self.explanationField.textColor = [NSColor whiteColor];
        self.codeField.textColor = [NSColor whiteColor];
    }
    else {
        [[NSColor whiteColor] set];
        self.explanationField.textColor = [NSColor textColor];
        self.codeField.textColor = [NSColor colorWithCalibratedRed:0.700 green:0.290 blue:0.000 alpha:1.000];
    }
    NSRectFill(dirtyRect);
    
}

- (void)mouseUp:(NSEvent *)theEvent {
    //  pass the mouseUp event to the enclosing menu item
    NSMenu *menu = self.enclosingMenuItem.menu;
    [menu cancelTracking];
    [menu performActionForItemAtIndex:[menu indexOfItem:self.enclosingMenuItem]];
}

@end
