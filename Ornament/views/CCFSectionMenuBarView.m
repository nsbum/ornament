//
//  CCFSectionMenuBarView.m
//  Ornament
//
//  Created by alanduncan on 11/3/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFSectionMenuBarView.h"

@implementation CCFSectionMenuBarView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    NSColor* topColor = [NSColor colorWithCalibratedRed: 0.845 green: 0.845 blue: 0.845 alpha: 1];
    NSColor* bottomColor = [NSColor colorWithCalibratedRed: 0.705 green: 0.705 blue: 0.705 alpha: 1];
    
    NSGradient *bodyGradient = [[NSGradient alloc] initWithColors:@[topColor,bottomColor]];
    
    NSRect frame = self.bounds;
    [bodyGradient drawInRect:frame angle:-90];
    
    NSBezierPath *path = [NSBezierPath bezierPath];
    [path moveToPoint:NSMakePoint(NSMinX(frame), NSMaxY(frame))];
    [path lineToPoint:NSMakePoint(NSMaxX(frame), NSMaxY(frame))];
    
    [path moveToPoint:NSMakePoint(NSMinX(frame), NSMaxY(frame))];
    [path lineToPoint:NSMakePoint(NSMinX(frame), NSMinY(frame))];
    
    [path moveToPoint:NSMakePoint(NSMaxX(frame), NSMaxY(frame))];
    [path lineToPoint:NSMakePoint(NSMaxX(frame), NSMinY(frame))];
    
    [[NSColor colorWithCalibratedWhite:0.605 alpha:1.000] setStroke];
    [path stroke];
    
    //  new path for eatched line above bottom
    path = [NSBezierPath bezierPath];
    [path moveToPoint:NSMakePoint(NSMinX(frame), NSMinY(frame)+1)];
    [path lineToPoint:NSMakePoint(NSMaxX(frame), NSMinY(frame)+1)];
    [[NSColor colorWithCalibratedWhite:0.875f alpha:1.000f] setStroke];
    [path stroke];

}

@end
