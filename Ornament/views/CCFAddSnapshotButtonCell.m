//
//  CCFAddSnapshotButtonCell.m
//  Ornament
//
//  Created by alanduncan on 11/21/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFAddSnapshotButtonCell.h"

@implementation CCFAddSnapshotButtonCell


- (NSRect)drawTitle:(NSAttributedString *)title withFrame:(NSRect)frame inView:(NSView *)controlView {
    NSRect adjustedRect = NSMakeRect(frame.origin.x, frame.origin.y - 3.0, NSWidth(frame), NSHeight(frame));
    return [super drawTitle:title withFrame:adjustedRect inView:controlView];
}

@end
