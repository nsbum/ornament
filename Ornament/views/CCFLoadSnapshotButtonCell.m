//
//  CCFLoadSnapshotButtonCell.m
//  Ornament
//
//  Created by alanduncan on 11/21/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFLoadSnapshotButtonCell.h"

@implementation CCFLoadSnapshotButtonCell

- (NSRect)drawTitle:(NSAttributedString *)title withFrame:(NSRect)frame inView:(NSView *)controlView {
    NSRect adjustedRect = NSMakeRect(frame.origin.x, frame.origin.y - 2, NSWidth(frame), NSHeight(frame));
    NSMutableAttributedString *mutableTitle = [title mutableCopy];
    [mutableTitle addAttribute:NSBaselineOffsetAttributeName value:[NSNumber numberWithFloat:-2.0] range:NSMakeRange(1, 1)];
    return [super drawTitle:mutableTitle withFrame:adjustedRect inView:controlView];
}

@end
