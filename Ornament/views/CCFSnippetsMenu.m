//
//  CCFSnippetsMenu.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFSnippetsMenu.h"
#import "CCFSnippetImporter.h"
#import "CCFSnippetMenuItemView.h"
#import "CCFSnippetCategory.h"
#import "CCFSnippet.h"

static NSNib *MenuViewLoader;

@implementation CCFSnippetsMenu

+ (void)initialize {
    if( self == [CCFSnippetsMenu class] ) {
        NSBundle *bundle = [NSBundle bundleForClass:self];
        MenuViewLoader = [[NSNib alloc] initWithNibNamed:@"CCFSnippetMenuItem" bundle:bundle];
    }
}

- (id)initWithTitle:(NSString *)aTitle {
    self = [super initWithTitle:@"Snippets"];
    self.autoenablesItems = NO;
    if( !self ) return nil;
    
    CCFSnippetImporter *importer = [[CCFSnippetImporter alloc] initWithCompletionBlock:^(NSArray *snippetCategories) {
        for( CCFSnippetCategory *category in snippetCategories ) {
            NSMenuItem *categoryItem = [[NSMenuItem alloc] initWithTitle:@"C"
                                                                  action:NULL
                                                           keyEquivalent:@""];
            [categoryItem setEnabled:YES];
            NSMenu *categorySubmenu = [[NSMenu alloc] initWithTitle:category.title];
            categorySubmenu.autoenablesItems = NO;
            for( CCFSnippet *snippet in category.snippets ) {
                NSMenuItem *snippetItem = [[NSMenuItem alloc] initWithTitle:snippet.displayString
                                                                     action:NULL
                                                              keyEquivalent:@""];
                [snippetItem setEnabled:YES];
                
                NSArray *topLevelItems = nil;
                __block CCFSnippetMenuItemView *menuView = nil;
                [MenuViewLoader instantiateNibWithOwner:self topLevelObjects:&topLevelItems];
                [topLevelItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    if( [obj isKindOfClass:[CCFSnippetMenuItemView class]] ) {
                        menuView = obj;
                        *stop = YES;
                    }
                }];
                menuView.codeField.stringValue = snippet.displayString;
                menuView.explanationField.stringValue = snippet.explanation;
                snippetItem.view = menuView;
                
                [categorySubmenu addItem:snippetItem];
            
            }
            [categoryItem setSubmenu:categorySubmenu];
            [self addItem:categoryItem];
        }
    }];
    [self setAutoenablesItems:NO];
    [importer import];
    
    
    return self;
}

@end
