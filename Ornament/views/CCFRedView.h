//
//  CCFRedView.h
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#if TARGET_OS_IPHONE
#define VIEW UIView
#else
#define VIEW NSView
#endif

@interface CCFRedView : VIEW

@end
