//
//  CCFRedView.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFRedView.h"
#import <QuartzCore/QuartzCore.h>

@implementation CCFRedView

#if TARGET_OS_IPHONE
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    
    self.layer.backgroundColor = [[UIColor redColor] CGColor];
    
    return self;
}
#else
- (void)drawRect:(NSRect)dirtyRect {
    [[NSColor redColor] setFill];
    NSRectFill(dirtyRect);
}

#endif

@end
