/**
 *   @file CCFSourceResultParagraphStyle.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 10:13:43
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFSourceResultParagraphStyle : NSMutableParagraphStyle


@end
