/**
 *   @file CCFSourceResultParagraphStyle.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 10:13:34
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSourceResultParagraphStyle.h"

@implementation CCFSourceResultParagraphStyle

- (id)init {
    self = [super init];
    if( !self ) return nil;
    
    [self setLineSpacing:3.0f];
    return self;
}

@end
