/**
 *   @file CCFOptionMarkerView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-31 08:36:21
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#if TARGET_OS_IPHONE
#define VIEW UIView
#else
#define VIEW NSView
#endif

@interface CCFOptionMarkerView : VIEW

@property (nonatomic, assign) BOOL state;

@end
