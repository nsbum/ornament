//
//  CCFSnippetMenuItemView.h
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CCFSnippetMenuItemView : NSView

@property (nonatomic, weak) IBOutlet NSTextField *codeField;
@property (nonatomic, weak) IBOutlet NSTextField *explanationField;

@end
