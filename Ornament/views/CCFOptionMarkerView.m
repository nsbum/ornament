/**
 *   @file CCFOptionMarkerView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-31 08:36:13
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFOptionMarkerView.h"

#if TARGET_OS_IPHONE
#define CCFRECT CGRect
#define CCFBEZIERPATH UIBezierPath
#else
#define CCFRECT NSRect
#define CCFBEZIERPATH NSBezierPath
#endif

static CCFOptionMarkerView *commonInit(CCFOptionMarkerView *self) {
    [self addObserver:self forKeyPath:@"state" options:NSKeyValueObservingOptionNew context:NULL];
    return self;
}

@implementation CCFOptionMarkerView

- (id)initWithFrame:(CCFRECT)frame
{
    self = [super initWithFrame:frame];
    if( !self ) return nil;
    
    return commonInit(self);
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    
    return commonInit(self);
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"state"];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if( [keyPath isEqualToString:@"state"] ) {
#if TARGET_OS_IPHONE
        [self setNeedsDisplay];
#else
        [self setNeedsDisplay:YES];
#endif
    }
}

- (void)drawRect:(CCFRECT)dirtyRect
{
    //// Color Declarations
    
    
    //// Frames
#if TARGET_OS_IPHONE
    CGRect frame = CGRectMake(0, 0, 12.5, 12.5);
    UIColor *veryLightGrayColor = [UIColor colorWithRed:0.928 green:0.928 blue:0.928 alpha:1.000];
#else
    NSRect frame = NSMakeRect(0, 0, 12.5, 12.5);
    NSColor* veryLightGrayColor = [NSColor colorWithCalibratedRed: 0.928 green: 0.928 blue: 0.928 alpha: 1];
#endif
    
    //// Rounded Rectangle Drawing
#if TARGET_OS_IPHONE
    CGRect roundRectFrame = CGRectMake(CGRectGetMinX(frame) + 1.5, CGRectGetMinY(frame) + CGRectGetHeight(frame) - 11, 10, 10);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:roundRectFrame cornerRadius:3.0];
#else
    NSRect roundRectFrame = NSMakeRect(NSMinX(frame) + 1.5, NSMinY(frame) + NSHeight(frame) - 11, 10, 10);
    NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:roundRectFrame xRadius:3 yRadius:3];
#endif
    [veryLightGrayColor setFill];
    [path fill];
#if TARGET_OS_IPHONE
    [[UIColor lightGrayColor] setStroke];
#else
    [[NSColor headerColor] setStroke];
#endif
    [path setLineWidth: 1];
    [path stroke];
    
    if( self.state ) {
#if TARGET_OS_IPHONE
        CGRect rRect = CGRectMake(CGRectGetMinX(frame) + 4.5, CGRectGetMinY(frame) + CGRectGetHeight(frame) - 8, 4, 4);
        UIBezierPath *ovalPath = [UIBezierPath bezierPathWithOvalInRect:rRect];
#else
        NSBezierPath* ovalPath = [NSBezierPath bezierPathWithOvalInRect: NSMakeRect(NSMinX(frame) + 4.5, NSMinY(frame) + NSHeight(frame) - 8, 4, 4)];
#endif
        
#if TARGET_OS_IPHONE
        [[UIColor colorWithRed:0.149 green:0.600 blue:0.859 alpha:1.000] setFill];
#else
        [[NSColor keyboardFocusIndicatorColor] setFill];
#endif
        [ovalPath fill];
    }
    
}

@end
