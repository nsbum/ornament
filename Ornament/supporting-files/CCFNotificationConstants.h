#if TARGET_OS_IPHONE

extern NSString * const kDidSelectSnippetNotification;
extern NSString * const kSnippetKey;

#endif

extern NSString * const kDidValidateRegexNotification;
extern NSString * const kDidChangeHighlightColorsNotification;
extern NSString * const kDidChangeSampleTextNotification;
extern NSString * const kSampleTextStringKey;

extern NSString * const kDidChangeRegexNotification;
extern NSString * const kRegexStringKey;

extern NSString * const kDidChangeSubstituteTextNotification;
extern NSString * const kSubstituteStringKey;

extern NSString * const kDidMatchRegexWithResults;
extern NSString * const kRegexMatchesKey;
extern NSString * const kRegexKey;

extern NSString * const kDidInvalidateRegexNotification;

extern NSString * const kDidLoadSnapshotNotification;

//  iPad export
extern NSString * const kDidRequestExportNotification;
extern NSString * const kExportTypeNotificationKey; //  0 = copy, 1 = email
extern NSString * const kExportLanguageNotificationKey;

