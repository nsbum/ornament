#import "CCFNotificationConstants.h"

#if TARGET_OS_IPHONE

NSString * const  kDidSelectSnippetNotification = @"DidSelectSnippetNotification";
NSString * const  kSnippetKey = @"SnippetKey";

#endif

NSString * const  kDidValidateRegexNotification = @"DidValidateRegexNotification";
NSString * const  kDidChangeHighlightColorsNotification = @"DidChangeHighlightColorsNotification";
NSString * const  kDidChangeSampleTextNotification = @"DidChangeSampleTextNotification";
NSString * const  kSampleTextStringKey = @"SampleTextStringKey";

NSString * const  kDidChangeRegexNotification = @"DidChangeRegexNotification";
NSString * const  kRegexStringKey = @"RegexStringKey";

NSString * const  kDidChangeSubstituteTextNotification = @"DidChangeSubstituteTextNotification";
NSString * const  kSubstituteStringKey = @"SubstituteStringKey";

NSString * const  kDidMatchRegexWithResults = @"DidMatchRegexWithResults";
NSString * const  kRegexMatchesKey = @"RegexMatchesKey";
NSString * const  kRegexKey = @"RegexKey";

NSString * const  kDidInvalidateRegexNotification = @"DidInvalidateRegexNotification";

NSString * const  kDidLoadSnapshotNotification = @"kidLoadSnapshotNotification";
//  NOTE - this notification also uses the kSampleTextStringKey and kRegexStringKey

NSString * const  kDidRequestExportNotification = @"DidRequestExportNotification";
NSString * const  kExportTypeNotificationKey = @"ExportTypeNotificationKey";
NSString * const  kExportLanguageNotificationKey = @"ExportLanguageNotificationKey";