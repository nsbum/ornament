
#import "CCFUserDefaultConstants.h"

NSString * const  kRegexModeDefault = @"RegexModeDefault";
NSString * const  kQuantifierColorDataKey = @"QuantifierColorDataKey";
NSString * const  kCharClassColorDataKey = @"CharClassColorDataKey";
NSString * const  kGroupingColorDataKey = @"GroupingColorDataKey";
NSString * const  kMetacharacterColorDataKey = @"MetacharacterColorDataKey";
NSString * const  kPerlDelimeterKey = @"PerlDelimeterKey";
NSString * const  kCodeTypeKey = @"CodeTypeKey";
NSString * const  kPythonCompileRegex = @"PythonCompileRegex";
NSString * const  kDidSeedSnapshots = @"DidSeedSnapshots";
NSString * const  kNumberedGroupColorDataKey = @"NumberedGroupColorDataKey";
NSString * const  kShowCopiedCodeAlertKey = @"ShowCopiedCodeAlertKey";
NSString * const  kShouldExpandAllResultItemsKey = @"ShouldExpandAllResultItemsKey";
NSString * const  kShouldPreserveResultExpansionKey = @"ShouldPreserveResultExpansionKey";
NSString * const  kUseSampleTextInCode = @"UseSampleTextInCode";
NSString * const  kFirstLaunchDate = @"FirstLaunchDate";
NSString * const  kLaunchCount = @"LaunchCount";
NSString * const  kRatingShouldAsk = @"RatingShouldAsk";