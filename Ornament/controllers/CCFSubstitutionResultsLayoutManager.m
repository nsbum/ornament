/**
 *   @file CCFSubstitutionResultsLayoutManager.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 05:31:17
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSubstitutionResultsLayoutManager.h"

NSString * const  CCFHighlightColorKey = @"com.cocoafactory.HighlightColor";
NSString * const  CCFLineColorKey = @"com.cocoafactory.LineColor";
NSString * const  CCFSpecialHighlightAttributeName = @"com.cocoafactory.SpecialHighlightAttribute";

@implementation CCFSubstitutionResultsLayoutManager

- (void)drawGlyphsForGlyphRange:(NSRange)glyphsToShow atPoint:(NSPoint)origin {
    NSTextStorage *textStorage = self.textStorage;
    NSRange glyphRange = glyphsToShow;
    while( glyphRange.length > 0 ) {
        //  get the character range for this glyph range to see if our attribute applies
        NSRange charRange = [self characterRangeForGlyphRange:glyphRange actualGlyphRange:NULL];
        NSRange attributeCharRange, attributeGlyphRange;
        id attribute = [textStorage attribute:CCFSpecialHighlightAttributeName
                                      atIndex:charRange.location longestEffectiveRange:&attributeCharRange
                                      inRange:charRange];
        attributeGlyphRange = [self glyphRangeForCharacterRange:attributeCharRange actualCharacterRange:NULL];
        if( attribute ) {
            QuietLog(@"Attribute at %@",NSStringFromRange(attributeCharRange));
            
            NSColor *bgColor = [attribute objectForKey:CCFHighlightColorKey];
            NSColor *lineColor = [attribute objectForKey:CCFLineColorKey];
            
            //  prepare to draw
            [NSGraphicsContext saveGraphicsState];
            
            NSUInteger rectCount;
            NSRectArray rectArray = [self rectArrayForCharacterRange:attributeCharRange
                                        withinSelectedCharacterRange:NSMakeRange(NSNotFound, 0)
                                                     inTextContainer:self.textContainers[0]
                                                           rectCount:&rectCount];
            for( NSInteger idx = 0; idx < rectCount; idx++ ) {
                //QuietLog(@"%ld -> %@",rectCount, NSStringFromRect(rectArray[idx]));
                NSRect originalBoundingRect = rectArray[idx];
                NSRect fragmentBoundingRect = NSMakeRect(originalBoundingRect.origin.x,
                                                         originalBoundingRect.origin.y - 1.0,
                                                         NSWidth(originalBoundingRect),
                                                         NSHeight(originalBoundingRect) - 2.0f);
                [bgColor setFill];
                NSRectFill(fragmentBoundingRect);
                
                NSRect bottomRect = NSMakeRect(NSMinX(fragmentBoundingRect), NSMaxY(fragmentBoundingRect) - 1.0, NSWidth(fragmentBoundingRect), 1.0f);
                [lineColor setFill];
                NSRectFill(bottomRect);
                
                NSRect topRect = NSMakeRect(NSMinX(fragmentBoundingRect), NSMinY(fragmentBoundingRect) - 1.0, NSWidth(fragmentBoundingRect), 1.0);
                NSRectFill(topRect);
            }
        }
        
        glyphRange.length = NSMaxRange(glyphRange) - NSMaxRange(attributeGlyphRange);
        glyphRange.location = NSMaxRange(attributeGlyphRange);
    }
    
    
    [super drawGlyphsForGlyphRange:glyphsToShow atPoint:origin];
    /*
    while (glyphRange.length > 0) {
        QuietLog(@"glphRange.length = %ld",glyphRange.length);
        NSRange charRange = [self characterRangeForGlyphRange:glyphRange actualGlyphRange:NULL];
        NSRange attributeCharRange, attributeGlyphRange;
        id attribute = [textStorage attribute:CCFSpecialHighlightAttributeName
                                      atIndex:charRange.location longestEffectiveRange:&attributeCharRange
                                      inRange:charRange];
        attributeGlyphRange = [self glyphRangeForCharacterRange:attributeCharRange actualCharacterRange:NULL];
        
        //attributeGlyphRange = NSIntersectionRange(attributeGlyphRange, glyphRange);
        if( attribute != nil ) {
            [NSGraphicsContext saveGraphicsState];
            
            NSUInteger rectCount;
            NSRectArray rectArray = [self rectArrayForCharacterRange:charRange
                                        withinSelectedCharacterRange:NSMakeRange(NSNotFound, 0)
                                                     inTextContainer:self.textContainers[0]
                                                           rectCount:&rectCount];
            
            if( rectCount > 0 ) {
                while( rectCount ) {
                    //QuietLog(@"%ld -> %@",rectCount, NSStringFromRect(rectArray[rectCount]));
                    //rectCount--;//
                    NSColor *bgColor = [attribute objectForKey:CCFHighlightColorKey];
                    NSColor *lineColor = [attribute objectForKey:CCFLineColorKey];
                    
                    //NSTextContainer *textContainer = self.textContainers[0];
                    NSRect boundingRect = rectArray[rectCount];
                    
                    [bgColor setFill];
                    NSRectFill(boundingRect);
                    
                    NSRect bottom = NSMakeRect(NSMinX(boundingRect), NSMaxY(boundingRect)-1.0, NSWidth(boundingRect), 1.0f);
                    [lineColor setFill];
                    NSRectFill(bottom);
                    
                    NSRect topRect = NSMakeRect(NSMinX(boundingRect), NSMinY(boundingRect), NSWidth(boundingRect), 1.0);
                    NSRectFill(topRect);
                    
                    rectCount--;
                    
                }
            }
            
            
            
            //[super drawGlyphsForGlyphRange:attributeGlyphRange atPoint:origin];
            [NSGraphicsContext restoreGraphicsState];
            //[super drawGlyphsForGlyphRange:glyphsToShow atPoint:origin];
        }
        else {
            //[super drawGlyphsForGlyphRange:glyphsToShow atPoint:origin];
        }
        glyphRange.length = NSMaxRange(glyphRange) - NSMaxRange(attributeGlyphRange);
        glyphRange.location = NSMaxRange(attributeGlyphRange);
    }
    [super drawGlyphsForGlyphRange:glyphsToShow atPoint:origin];
     */
}


@end
