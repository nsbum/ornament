/**
 *   @file CCFResultsOutlineView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 06:20:09
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFResultsOutlineView : NSOutlineView

@end
