/**
 *   @file CCFRegexViewController+CopyCode.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-31 11:31:48
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFRegexViewController.h"

@interface CCFRegexViewController (CopyCode)

@end
