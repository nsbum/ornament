/**
 *   @file CCFGeneralPreferencesViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-12-20 04:44:44
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "MASPreferencesViewController.h"

@interface CCFGeneralPreferencesViewController : NSViewController <MASPreferencesViewController>

- (id)initWithNib;

@end
