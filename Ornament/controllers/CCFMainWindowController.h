//
//  CCFMainWindowController.h
//  Ornament
//
//  Created by alanduncan on 11/27/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class CCFMainViewController;

@interface CCFMainWindowController : NSWindowController <NSWindowDelegate>

@property (nonatomic, strong) IBOutlet CCFMainViewController *viewController;

@end
