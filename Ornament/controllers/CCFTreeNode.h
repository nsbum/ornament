/**
 *   @file CCFTreeNode.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 05:00:00
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFTreeNode : CCFObject

@property (nonatomic, copy) NSString *nodeTitle;
@property (nonatomic, copy) NSString *rangeString;
@property (nonatomic, copy) NSString *matchText;
@property (nonatomic, strong) NSMutableArray *children;
@property (nonatomic, assign) BOOL isLeaf;
@property (nonatomic, assign) BOOL isGroup;

- (void)addChild:(CCFTreeNode *)node;
- (NSInteger)numberOfChildren;

@end
