/**
 *   @file CCFSubstitutionResultsLayoutManager.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 05:31:10
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

extern NSString * const CCFHighlightColorKey;
extern NSString * const CCFLineColorKey;
extern NSString * const CCFSpecialHighlightAttributeName;

@interface CCFSubstitutionResultsLayoutManager : NSLayoutManager

@end
