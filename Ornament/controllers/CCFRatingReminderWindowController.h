//
//  CCFRatingReminderWindowController.h
//  Ornament
//
//  Created by alanduncan on 2/4/13.
//  Copyright (c) 2013 Cocoa Factory, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol CCFRatingReminderWindowControllerDelegate;

@interface CCFRatingReminderWindowController : NSWindowController

@property (nonatomic, assign) id <CCFRatingReminderWindowControllerDelegate> delegate;

- (id)initWithNib;

@end
