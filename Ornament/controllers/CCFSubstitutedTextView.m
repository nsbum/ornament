/**
 *   @file CCFSubstitutedTextView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 05:28:03
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSubstitutedTextView.h"

static CCFSubstitutedTextView *commonInit(CCFSubstitutedTextView *self) {
    //  set up our initial text size
    NSFont *font = [[NSFontManager sharedFontManager] fontWithFamily:@"Helvetica" traits:0 weight:5 size:13.0];
    NSDictionary *attributes = @{NSFontAttributeName : font};
    [self setTypingAttributes:attributes];
    
    NSMutableParagraphStyle *mutableStyle = [[NSMutableParagraphStyle alloc] init];
    [mutableStyle setLineSpacing:13.0];
    [self setDefaultParagraphStyle:mutableStyle];
    
    return self;
}

@implementation CCFSubstitutedTextView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if( !self ) return nil;
    
    return commonInit(self);
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    
    return commonInit(self);
}

- (BOOL)readSelectionFromPasteboard:(NSPasteboard *)pboard {
    //  reject pboard contents that aren't interesting to us
    NSArray *classes = @[[NSString class],[NSAttributedString class]];
    NSDictionary *options = [NSDictionary dictionary];
    NSArray *strings = [pboard readObjectsForClasses:classes options:options];
    if( strings != nil ) {
        [self setIsPasteOperation:YES];
        return [super readSelectionFromPasteboard:pboard];
    }
    return NO;
}

@end
