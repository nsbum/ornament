/**
 *   @file CCFCopiedCodeWindowController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-26 05:08:33
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

typedef void(^CCFCopiedCodeCloseBlock)(void);

@interface CCFCopiedCodeWindowController : NSWindowController <NSWindowDelegate>

- (id)initWithCloseBlock:(CCFCopiedCodeCloseBlock)block;

@end
