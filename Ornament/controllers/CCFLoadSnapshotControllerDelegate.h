/**
 *   @file CCFLoadSnapshotControllerDelegate.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-31 05:07:24
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFSnapshot;

@protocol CCFLoadSnapshotControllerDelegate <NSObject>

- (void)loadSnapshotController:(id)controller didSelectSnapshot:(CCFSnapshot *)snapshot;

@end
