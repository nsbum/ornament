//
//  CCFMatchViewController.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFMatchViewController.h"
#import "CCFRegexViewController.h"
#import "CCFSampleTextViewController.h"
#import "CCFRegex.h"
#import "CCFSourceTextView.h"

static NSString * const MatchControllerNibName = @"CCFMatchView";

@interface CCFMatchViewController () {
    CCFRegexViewController *_regexController;
    CCFSampleTextViewController *_sampleTextController;
}

@end

@implementation CCFMatchViewController

- (id)initWithNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self = [super initWithNibName:MatchControllerNibName bundle:bundle];
    if( !self ) return nil;
    
    return self;
}

- (void)awakeFromNib {
    [self _installRegexView];
    [[self regexController] setDelegate:self];
    
    [self _installSampleTextView];
    [[self sampleTextController] setDelegate:self];
    
    
    
    
    int64_t delayInSeconds = 0.05;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
    });
    
    
   // NSLog(@"%s - rtv nextKeyView = %@",__FUNCTION__,regexTextView.nextValidKeyView);
}


- (void)setupResponderChain {
    CCFRegexTextView *regexTextView = [[self regexController] textView];
    CCFSourceTextView *sourceTextView = [[self sampleTextController] textView];
    [regexTextView setNextKeyView:sourceTextView];
    [sourceTextView setNextKeyView:regexTextView];
    
    NSWindow *mainWindow = [[self view] window];
    [mainWindow makeFirstResponder:regexTextView];
}

- (void)_installRegexView {
    NSView *regexView = [[self regexController] view];
    regexView.frame = self.regexContainerView.bounds;
    [[self regexContainerView] addSubview:regexView];
}

- (void)_installSampleTextView {
    NSView *sampleTextView = [[self sampleTextController] view];
    sampleTextView.frame = self.sampleTextContainerView.bounds;
    [[self sampleTextContainerView] addSubview:sampleTextView];
}

#pragma mark - Public

- (NSString *)regexString {
    return [[[self regexController] textView] string];
}
- (void)setRegexString:(NSString *)regexString {
    if( !regexString ) return;
    [[[self regexController] textView] setString:@""];
    [[[self regexController] textView] insertText:regexString];
}


#pragma mark - Accessors

- (CCFRegexViewController *)regexController {
    if( !_regexController ) {
        _regexController = [[CCFRegexViewController alloc] initWithNib];
    }
    return _regexController;
}

- (CCFSampleTextViewController *)sampleTextController {
    if( !_sampleTextController ) {
        _sampleTextController = [[CCFSampleTextViewController alloc] initWithNib];
    }
    return _sampleTextController;
}

- (NSString *)sourceText {
    return _sampleTextController.textView.string;
}

#pragma mark - CCFRegexViewControllerDelegate

- (void)regexController:(id)controller didValidateRegex:(CCFRegex *)regex {
    //  the regex is valid, so we need to try to match against the
    //  source text
    [self _matchAndNotifyDelegateUsingRegex:regex];
}

- (void)regexController:(id)controller didInvalidateRegex:(CCFRegex *)regex {
    (void)0;
    //[[self delegate] matchViewControllerDidInvalidateResults:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidInvalidateRegexNotification object:self];
}

#pragma mark - CCFSampleTextViewControllerDelegate

- (void)sampleTextControllerDidChangeText:(id)controller {
    //  create a regex object to run
    CCFRegex *regex = [[CCFRegex alloc] initWithString:[[[self regexController] textView] string]];
    //  find any options that have been selected
    NSRegularExpressionOptions options = [[self regexController] regexOptions];
    regex.options = [CCFRegexOptions optionsObjectWithOptions:options];
    
    if( [regex regexIsValid] ) {
        //  if we have valid regex then try to run text
        [self _matchAndNotifyDelegateUsingRegex:regex];
    }
    else {
        //  fire notification that we invalidated match
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidInvalidateRegexNotification object:self];
    }
}

- (void)_matchAndNotifyDelegateUsingRegex:(CCFRegex *)regex {
    NSArray *matches = [regex matchesWithSourceText:self.sampleTextController.textView.string];
    if( [matches count] != 0 ) {
        //  fire notification that we matched
        NSDictionary *userInfo = @{kRegexMatchesKey : matches, kRegexKey : regex};
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidMatchRegexWithResults object:self userInfo:userInfo];
    }
    else {
        //  no matches (even though regex is valid)
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidInvalidateRegexNotification object:self];
    }
}


@end
