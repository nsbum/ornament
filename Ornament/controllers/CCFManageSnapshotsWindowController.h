/**
 *   @file CCFManageSnapshotsWindowController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 14:55:06
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFManageSnapshotsWindowController : NSWindowController <NSWindowDelegate>

@property (nonatomic,strong) IBOutlet NSArrayController *snapshotsArrayController;
@property (nonatomic, weak) IBOutlet NSTableView *tableView;

- (id)initWithNib;

@end
