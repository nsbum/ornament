/**
 *   @file CCFRegexTextView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 11:40:09
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#define MAX_CODE_EXPORT_MENU_TAG 100
#define CLEAR_CODE_MENU_TAG 103
#define COPY_MENU_TAG 101
#define PASTE_MENU_TAG 102

#import "CCFRegexTextView.h"
// helper
#import "CCFCodeCopierHelper.h"

@implementation CCFRegexTextView {
    IBOutlet NSMenu *_customMenu;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) { return nil; }
    
    //  load our custom 
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSArray *topLevelObjects = nil;
    if( [NSBundle instancesRespondToSelector:@selector(loadNibNamed:owner:topLevelObjects:)] ) {
        [bundle loadNibNamed:@"CCFCopyCodeMenu" owner:self topLevelObjects:&topLevelObjects];
        
        [topLevelObjects enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if( [obj isKindOfClass:[NSMenu class]] ) {
                _customMenu = obj;
            }
        }];
    }
    else {
        [NSBundle loadNibNamed:@"CCFCopyCodeMenu" owner:self];
    }
    [[_customMenu itemArray] enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSMenuItem *item = (NSMenuItem *)obj;
        [item setAction:@selector(contextualMenuAction:)];
    }];
    return self;
}

- (void)insertTab:(id)sender {
    [[self window] selectNextKeyView:self];
}


- (void)insertBacktab:(id)sender {
    [[self window] selectPreviousKeyView:self];
}

- (NSMenu *)menuForEvent:(NSEvent *)event {
    if( event.type == NSRightMouseDown ) {
        return _customMenu;
    }
    else {
        return [super menuForEvent:event];
    }
}

#pragma mark - Public

- (void)disableCodeMenuItems {
    [self setCodeMenuItemsState:NO];
}

- (void)enableCodeMenuItems {
    [self setCodeMenuItemsState:YES];
}

#pragma mark - Interface actions

- (void)contextualMenuAction:(id)sender {
    NSInteger tag = [sender tag];
    if( tag < MAX_CODE_EXPORT_MENU_TAG ) {
        //  this is a language export item
        CCFCodeCopierHelper *helper = [[CCFCodeCopierHelper alloc] initWithCodeType:(CCFCodeType)tag];
        [helper exportCode];
    }
    else if( tag == CLEAR_CODE_MENU_TAG ) {
        self.string = @"";
    }
    else if( tag == COPY_MENU_TAG ) {
        [[NSPasteboard generalPasteboard] clearContents];
        [[NSPasteboard generalPasteboard] writeObjects:@[self.string]];
    }
    else if( tag == PASTE_MENU_TAG ) {
        [self pasteAsPlainText:self];
    }
}

#pragma mark - Private

- (void)setCodeMenuItemsState:(BOOL)state {
    [[_customMenu itemArray] enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSMenuItem *item = (NSMenuItem *)obj;
        if( item.tag < MAX_CODE_EXPORT_MENU_TAG ) {
            [item setEnabled:state];
        }
    }];
}

@end
