//
//  CCFCodePrefsViewController.m
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFCodePrefsViewController.h"

static NSString * const CodePrefsNibName = @"CCFCodePrefsView";

@interface CCFCodePrefsViewController ()

@end

@implementation CCFCodePrefsViewController

- (id)initWithNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self = [super initWithNibName:CodePrefsNibName bundle:bundle];
    if( !self ) return nil;
    
    return self;
}

- (NSString *)identifier {
    return @"Code";
}

- (NSString *)toolbarItemLabel {
    return _(@"Code");
}

- (NSImage *)toolbarItemImage {
    return [NSImage imageNamed:@"code-pref-icon-160px.png"];
}

- (NSView *)initialKeyView {
    return self.view;
}

@end
