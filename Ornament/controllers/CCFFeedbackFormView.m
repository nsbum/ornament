//
//  CCFFeedbackForView.m
//  Ornament
//
//  Created by alanduncan on 11/28/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFFeedbackFormView.h"

@implementation CCFFeedbackFormView

- (BOOL)isOpaque {
    return YES;
}

- (NSInteger)white {
    return 92;
}

@end
