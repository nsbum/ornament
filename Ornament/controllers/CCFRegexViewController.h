/**
 *   @file CCFRegexViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 11:41:15
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFRegexTextView.h"

@protocol CCFRegexViewControllerDelegate;

@interface CCFRegexViewController : NSViewController <NSTextViewDelegate, NSPopoverDelegate>

@property (nonatomic, weak) IBOutlet NSPopUpButton *snippetsButton;
@property (nonatomic, assign) IBOutlet CCFRegexTextView *textView;
@property (nonatomic, strong) IBOutlet CCFOrnamentData *sharedData;

@property (nonatomic, assign) id <CCFRegexViewControllerDelegate> delegate;
@property (nonatomic, assign) NSRegularExpressionOptions regexOptions;

@property (nonatomic, assign) NSInteger regexIsValid;
@property (nonatomic, assign) NSInteger canCopyCode;

- (id)initWithNib;

@end
