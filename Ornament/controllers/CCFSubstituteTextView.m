/**
 *   @file CCFSubstituteTextView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 13:07:28
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSubstituteTextView.h"

@implementation CCFSubstituteTextView

- (void)insertTab:(id)sender {
    [[self window] selectNextKeyView:self];
}


- (void)insertBacktab:(id)sender {
    [[self window] selectPreviousKeyView:self];
}

@end
