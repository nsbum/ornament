//
//  CCFFeedbackSuccessView.h
//  Ornament
//
//  Created by alanduncan on 11/28/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <CCFAppKit/CCFFlexibleGrayView.h>

@interface CCFFeedbackSuccessView : CCFFlexibleGrayView


@end
