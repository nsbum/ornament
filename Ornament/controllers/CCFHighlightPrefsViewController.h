/**
 *   @file CCFHighlightPrefsViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 09:57:43
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */


#import "MASPreferencesViewController.h"

@interface CCFHighlightPrefsViewController : NSViewController <MASPreferencesViewController>

- (id)initWithNib;

@end
