/**
 *   @file CCFResultsViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-22 22:02:26
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFResultsOutlineView;

@interface CCFResultsViewController : NSViewController <NSOutlineViewDataSource, NSOutlineViewDelegate>

/** Regex results
 
 NSArray of CCFMatch and child CCFGroup objects
 */
@property (nonatomic, strong) NSArray *regexResults;

/** Match outline view
 
 Outline view in which we display the regex matches
 */
@property (nonatomic, weak) IBOutlet CCFResultsOutlineView *matchOutlineView;

- (id)initWithNib;


@end
