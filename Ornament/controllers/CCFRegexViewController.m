/**
 *   @file CCFRegexViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 11:41:23
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFRegexViewController.h"
#import "CCFRegexViewControllerDelegate.h"
#import <CCFAppKit/CCFInvertingImageButton.h>

#import "CCFAppDelegate.h"
#import "CCFMenuManager.h"

#import "CCFSnippetsMenu.h"
#import "CCFSnippetImporter.h"
#import "CCFSnippetCategory.h"
#import "CCFSnippet.h"
#import "CCFSnippetMenuItemView.h"
#import "CCFRegexHighlighter.h"
#import "CCFRegexOptionsViewController.h"
#import "CCFRegex.h"
#import "CCFRegexTextView.h"


static NSString * const RegexNibName = @"CCFRegexView";
static CCFRegexHighlighter *Highlighter;
static NSNib *MenuViewLoader;

@interface CCFRegexViewController ()

@end

@implementation CCFRegexViewController {
    NSArray *_snippetCategories;
    NSPopover *_optionsPopover;
}

+ (void)initialize {
    if( self == [CCFRegexViewController class] ) {
        NSBundle *bundle = [NSBundle bundleForClass:self];
        MenuViewLoader = [[NSNib alloc] initWithNibNamed:@"CCFSnippetMenuItem" bundle:bundle];
        
        Highlighter = [[CCFRegexHighlighter alloc] init];
    }
}

- (id)initWithNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self = [super initWithNibName:RegexNibName bundle:bundle];
    if( !self ) return nil;
    
    [self willChangeValueForKey:@"regexIsValid"];
    _regexIsValid = YES;
    [self didChangeValueForKey:@"regexIsValid"];
    
    [self addObserver:self forKeyPath:@"regexIsValid" options:NSKeyValueObservingOptionNew context:NULL];
    
    self.canCopyCode = NO;
    
    [self setupNotificationObservations];
        
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"regexIsValid"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if( [keyPath isEqualToString:@"regexIsValid"] ) {
        if( !self.regexIsValid ) {
            self.canCopyCode = NO;
        }
        else {
            self.canCopyCode = (self.textView.string.length != 0);
        }
    }
}

- (void)loadView {
    [super loadView];
    [self _replaceSnipMenu];
    
    [[self textView] disableCodeMenuItems];
}


- (void)_replaceSnipMenu {
    NSPopUpButtonCell *cell = self.snippetsButton.cell;
    __block NSMenu *menu = cell.menu;
    //  remove items 2 and 3
    [menu removeItemAtIndex:1];
    [menu removeItemAtIndex:1];
    
    CCFSnippetImporter *importer = [[CCFSnippetImporter alloc] initWithCompletionBlock:^(NSArray *snippetCategories) {
        _snippetCategories = snippetCategories;
        
        for( CCFSnippetCategory *category in snippetCategories ) {
            NSMenuItem *categoryItem = [[NSMenuItem alloc] initWithTitle:category.title
                                                                  action:NULL
                                                           keyEquivalent:@""];
            [categoryItem setEnabled:YES];
            NSMenu *categorySubmenu = [[NSMenu alloc] initWithTitle:category.title];
            categorySubmenu.autoenablesItems = NO;
            for( CCFSnippet *snippet in category.snippets ) {
                NSMenuItem *snippetItem = [[NSMenuItem alloc] initWithTitle:snippet.displayString
                                                                     action:NULL
                                                              keyEquivalent:@""];
                [snippetItem setEnabled:YES];
                [snippetItem setTarget:self];
                [snippetItem setAction:@selector(snippetMenuAction:)];
                
                NSArray *topLevelItems = nil;
                __block CCFSnippetMenuItemView *menuView = nil;
                [MenuViewLoader instantiateNibWithOwner:self topLevelObjects:&topLevelItems];
                [topLevelItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    if( [obj isKindOfClass:[CCFSnippetMenuItemView class]] ) {
                        menuView = obj;
                        *stop = YES;
                    }
                }];
                menuView.codeField.stringValue = snippet.displayString;
                menuView.explanationField.stringValue = snippet.explanation;
                snippetItem.view = menuView;
                snippetItem.tag = snippet.tag;
                
                [categorySubmenu addItem:snippetItem];
                
            }
            [categoryItem setSubmenu:categorySubmenu];
            [menu addItem:categoryItem];
        }
    }];
    [menu setAutoenablesItems:NO];
    [importer import];
    
}

- (IBAction)optionsAction:(id)sender {
    //  get our current regex options and set them on the options view controller
    CCFRegexOptionsViewController *optionsViewController = (CCFRegexOptionsViewController *)[[self optionsPopover] contentViewController];
    optionsViewController.regexOption = [CCFRegexOptions optionsObjectWithOptions:self.regexOptions];
    
    [[self optionsPopover] showRelativeToRect:[sender frame] ofView:[sender superview] preferredEdge:0];
}

- (void)snippetMenuAction:(id)sender {
    NSLog(@"%s - tag = %ld",__FUNCTION__,[sender tag]);
    CCFSnippetCategory *selectedCategory = [self _snippetCategoryByTag:[sender tag]];
    CCFSnippet *snippet = [selectedCategory snippetWithTag:[sender tag]];
    NSLog(@"%s - code = %@",__FUNCTION__,snippet.displayString);
    
    NSRange insertionRange = self.textView.selectedRange;
    //NSInteger currentInsertionPoint = insertionRange.location;
    NSInteger cursorOffset = 0;
    NSMutableString *mutableSnip = [[snippet snip] mutableCopy];
    //  offset to the cursor mark
    NSRange cursorRange = [mutableSnip rangeOfString:@"ß"];
    if( cursorRange.location != NSNotFound ) {
        cursorOffset = cursorRange.location;
        //  remove the marker now that we measured the location
        [mutableSnip deleteCharactersInRange:cursorRange];
    }
    
    [[self textView] insertText:mutableSnip replacementRange:insertionRange];
    if( cursorOffset != 0 ) {
        insertionRange.location = insertionRange.location + cursorOffset;
        [[self textView] setSelectedRange:insertionRange];
    }
    
}

- (CCFSnippetCategory *)_snippetCategoryByTag:(NSInteger)tag {
    NSInteger index = tag/1000;
    return _snippetCategories[index];
}

#pragma mark - Accessors

- (NSPopover *)optionsPopover {
    if( !_optionsPopover ) {
        CCFRegexOptionsViewController *vc = [[CCFRegexOptionsViewController alloc] initWithNib];
        _optionsPopover = [[NSPopover alloc] init];
        _optionsPopover.contentViewController = vc;
        _optionsPopover.behavior = NSPopoverBehaviorTransient;
        _optionsPopover.delegate = self;
    }
    return _optionsPopover;
}

#pragma mark - NSTextViewDelegate

- (void)textDidChange:(NSNotification *)notification {
    [self _highlightSyntax];
    [self _validateSyntaxInBackground];
    
    //  post notification that we changed regex so that orthogonal entities
    //  can take note of the change.
    NSDictionary *userInfo = @{kRegexStringKey:self.textView.string};
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeRegexNotification object:self userInfo:userInfo];
    
    [[CCFOrnamentData sharedData] setRegexString:self.textView.string];
 
}

- (void)_validateSyntaxInBackground {
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(backgroundQueue, ^{
        CCFRegex *regex = [[CCFRegex alloc] initWithString:self.textView.string];
        CCFRegexOptions *optionsObject = [CCFRegexOptions optionsObjectWithOptions:_regexOptions];
        regex.options = optionsObject;
        
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        if( ![regex regexIsValid] ) {
            dispatch_async(mainQueue, ^{
                [[self delegate] regexController:self didInvalidateRegex:regex];
                self.regexIsValid = NO;
                
                //  prevent the user from copying invalid regex
                [[self textView] disableCodeMenuItems];
            });
        }
        else {
            self.regexIsValid = YES;
            [[self delegate] regexController:self didValidateRegex:regex];
            
            //  if this regex is valid, copy it to our shared data
            [[CCFOrnamentData sharedData] setRegex:regex];
            
            NSLog(@"%ld",self.textView.string.length);
            dispatch_async(mainQueue, ^{
                CCFAppDelegate *appDelegate = (CCFAppDelegate *)[NSApp delegate];
                CCFMenuManager *menuManager = appDelegate.menuManager;
                
                if( self.textView.string.length != 0 ) {
                    [[self textView] enableCodeMenuItems];
                    [menuManager setMenuItemState:YES];
                }
                else {
                    [[self textView] disableCodeMenuItems];
                    [menuManager setMenuItemState:NO];
                }
            });
            if( self.textView.string.length != 0 ) {
                
            }
        }
    });
}

#pragma mark - Private

- (void)setupNotificationObservations {
    __block id snapshotLoaded = nil;
    snapshotLoaded = [[NSNotificationCenter defaultCenter] addObserverForName:kDidLoadSnapshotNotification
                                                                       object:nil
                                                                        queue:nil
                                                                   usingBlock:^(NSNotification *note) {
                                                                       NSAssert([[NSThread currentThread] isMainThread], @"Only post kDidLoadSnapshotNotification on main queue");
                                                                       self.textView.string = [note userInfo][kRegexStringKey];
                                                                       [self _highlightSyntax];
                                                                       [self _validateSyntaxInBackground];
                                                                   }];
    
    __block id syntaxColorsChanges = nil;
    syntaxColorsChanges = [[NSNotificationCenter defaultCenter] addObserverForName:kDidChangeHighlightColorsNotification
                                                                            object:nil
                                                                             queue:nil
                                                                        usingBlock:^(NSNotification *note) {
                                                                            [self _highlightSyntax];
                                                                        }];
}

- (void)_highlightSyntax {
    NSRange savedRange = self.textView.selectedRange;
    NSAttributedString *sourceString = self.textView.attributedString;
    Highlighter.sourceAttributedString = sourceString;
    self.textView.textStorage.attributedString = [Highlighter highlightedString];
    self.textView.selectedRange = savedRange;
}

#pragma mark - NSPopoverDelegate

- (void)popoverDidClose:(NSNotification *)notification {
    CCFRegexOptionsViewController *optionsViewController = (CCFRegexOptionsViewController *)[[self optionsPopover] contentViewController];
    self.regexOptions = [[optionsViewController regexOption] regexOptions];
    [[CCFOrnamentData sharedData] setCurrentOptions:self.regexOptions];
    
    //  when the popover closes, we need to generate matches again (as long as the regex is still valid)
    [self _validateSyntaxInBackground];
}

@end
