//
//  CCFReferenceSheetWindowController.h
//  Ornament
//
//  Created by alanduncan on 12/14/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CCFReferenceSheetWindowController : NSWindowController

- (id)initWithNib;

@end
