/**
 *   @file CCFTreeNode.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 05:00:07
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFTreeNode.h"

@implementation CCFTreeNode

- (void)addChild:(CCFTreeNode *)node {
    [[self children] addObject:node];
}

- (NSInteger)numberOfChildren {
    return (self.isLeaf) ? -1 : [[self children] count];
}

#pragma mark - Accessors

- (NSMutableArray *)children {
    if( !_children ) {
        _children = [[NSMutableArray alloc] init];
    }
    return _children;
}

@end
