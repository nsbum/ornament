/**
 *   @file CCFManageSnapshotsWindowController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 14:55:13
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFManageSnapshotsWindowController.h"
#import "CCFSnapshot.h"

static NSString * const ManageSnapshotsNib = @"CCFManageSnapshotsWindow";

@interface CCFManageSnapshotsWindowController ()

@end

@implementation CCFManageSnapshotsWindowController

- (id)initWithNib {
    self = [super initWithWindowNibName:ManageSnapshotsNib owner:self];
    if( !self ) return nil;

    return self;
}

- (void)awakeFromNib {
    [[self window] setDelegate:self];
    
}

- (void)windowDidBecomeKey:(NSNotification *)note {
    [[self snapshotsArrayController] removeObjects:self.snapshotsArrayController.arrangedObjects];
    for( CCFSnapshot *snapshot in [[CCFOrnamentData sharedData] snapshots] ) {
        NSMutableDictionary *mutableSnapshotInfo = [[NSMutableDictionary alloc] init];
        [mutableSnapshotInfo setObject:snapshot.regexString forKey:@"regexString"];
        [mutableSnapshotInfo setObject:snapshot.descriptor forKey:@"descriptor"];
        [mutableSnapshotInfo setObject:snapshot.sampleText forKey:@"sampleText"];
        [[self snapshotsArrayController] addObject:mutableSnapshotInfo];
    }
}

#pragma mark - NSWindowDelegate

- (void)windowWillClose:(NSNotification *)notification {
    NSMutableArray *snapshotArray = [[NSMutableArray alloc] init];
    NSMutableArray *objs = [[self snapshotsArrayController] arrangedObjects];
    for( NSDictionary *info in objs ) {
        CCFSnapshot *snapshot = [[CCFSnapshot alloc] init];
        snapshot.regexString = info[@"regexString"];
        snapshot.descriptor = info[@"descriptor"];
        snapshot.sampleText = info[@"sampleText"];
        
        [snapshotArray addObject:snapshot];
    }
    [[CCFOrnamentData sharedData] setSnapshots:snapshotArray];
}


#pragma mark - Interface actions

- (IBAction)delegeItem:(id)sender {
    NSInteger selectedRow = [[self tableView] selectedRow];
    [[self snapshotsArrayController] removeObjectAtArrangedObjectIndex:selectedRow];
}


@end
