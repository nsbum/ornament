/**
 *   @file CCFSubstitutionSourceTextViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-27 19:18:30
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFParentSampleTextViewController.h"

@class  CCFSourceTextView;

@protocol CCFSubstitutionSourceTextControllerDelegate;


@interface CCFSubstitutionSourceTextViewController : CCFParentSampleTextViewController <NSTextViewDelegate>

@property (nonatomic, assign) id <CCFSubstitutionSourceTextControllerDelegate> delegate;


@end
