/**
 *   @file CCFNewSnippetWindowController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 13:19:32
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@protocol CCFNewSnapshotControllerDelegate;

@interface CCFNewSnapshotWindowController : NSWindowController

@property (nonatomic, weak) IBOutlet NSTextField *regexField;
@property (nonatomic, weak) IBOutlet NSTextField *sampleTextField;
@property (nonatomic, weak) IBOutlet NSTextField *descriptionField;

@property (nonatomic, assign) id <CCFNewSnapshotControllerDelegate> delegate;

- (id)initWithNib;

@end
