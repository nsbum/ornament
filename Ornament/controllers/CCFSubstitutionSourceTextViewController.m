/**
 *   @file CCFSubstitutionSourceTextViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-27 19:19:38
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSubstitutionSourceTextViewController.h"
#import "CCFSubstitutionSourceTextControllerDelegate.h"
#import "CCFRegex.h"
#import "CCFSourceTextView.h"

static NSString * const SubstitutionSourceTextViewNibName = @"CCFSubstitutionSourceTextView";

@interface CCFSubstitutionSourceTextViewController ()
@end

@implementation CCFSubstitutionSourceTextViewController

+ (NSString *)nibName {
    return SubstitutionSourceTextViewNibName;
}

#pragma mark - NSTextViewDelegate

- (void)textDidChange:(NSNotification *)notification {
    [self handleTextDidChangeNotification:notification withSelector:@selector(substitutionSourceTextControllerDidChangeText:)];
}

- (BOOL)textView:(NSTextView *)textView shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString {
    return [self shouldHandleChangeInTextView:textView range:affectedCharRange replacementString:replacementString];
}

- (void)setupNotificationObservations {
    [super setupNotificationObservations];
    
    __block id sampleTextChanged = nil;
    sampleTextChanged = [[NSNotificationCenter defaultCenter] addObserverForName:kDidChangeSampleTextNotification
                                                                          object:nil
                                                                           queue:nil
                                                                      usingBlock:^(NSNotification *note) {
                                                                          NSObject *sender = note.object;
                                                                          NSString *className = NSStringFromClass([sender class]);
                                                                          if( [@"CCFSampleTextViewController" isEqualToString:className] ) {
                                                                              NSString *text = [note userInfo][kSampleTextStringKey];
                                                                              self.textView.string = text;
                                                                          }
                                                                      }];
    
   
}



@end
