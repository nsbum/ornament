/**
 *   @file CCFHighlightPrefsViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 09:57:51
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFHighlightPrefsViewController.h"

DEFINE_NSSTRING(CCFHighlightPrefs);

@interface CCFHighlightPrefsViewController ()

@end

@implementation CCFHighlightPrefsViewController

- (id)initWithNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self = [super initWithNibName:CCFHighlightPrefs bundle:bundle];
    if( !self ) return nil;
    
    return self;
}

- (NSString *)identifier {
    return @"Highlight";
}

- (NSString *)toolbarItemLabel {
    return _(@"Highlighting");
}

- (NSImage *)toolbarItemImage {
    return [NSImage imageNamed:NSImageNameColorPanel];
}

- (NSView *)initialKeyView {
    return self.view;
}

@end
