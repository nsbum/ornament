//
//  CCFSampleTextViewController.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFSampleTextViewController.h"
#import "CCFSampleTextViewControllerDelegate.h"
#import "CCFSourceTextView.h"
#import "CCFSourceResultParagraphStyle.h"

static NSString * const SampleTextControllerNibName = @"CCFSampleTextView";

@interface CCFSampleTextViewController ()
@end

@implementation CCFSampleTextViewController

+ (NSString *)nibName {
    return SampleTextControllerNibName;
}

- (BOOL)_shouldRespondToTextChange:(NSNotification *)note {
    NSString *className = NSStringFromClass([[note object] class] );
    return [@"CCFSubstitutionSourceTextViewController" isEqualToString:className];
}



- (void)removeRegexMarkupWithNotification:(NSNotification *)note {
    [[[self textView] textStorage] enumerateAttribute:NSBackgroundColorAttributeName
                                              inRange:entireStringRange(self.textView.textStorage)
                                              options:0
                                           usingBlock:^(id value, NSRange range, BOOL *stop) {
                                               [[[self textView] textStorage] removeAttribute:NSBackgroundColorAttributeName range:range];
                                           }];
}


#pragma mark - NSTextViewDelegate

- (void)textDidChange:(NSNotification *)notification {
    /*
    //  since we have changed the attributed sample string, we need to save
    //  to the data singleton
    [[CCFOrnamentData sharedData] setSampleText:self.textView.string];
    
    //  pass the change notice along so that the delegate
    //  can take whatever action is needed - like re-running
    //  the regex against the new text.
    [[self delegate] sampleTextControllerDidChangeText:self];
    
    //  post notification to update sister controller (substitution controller)
    [self postSampleTextChangeNotification];
     */
    
     [self handleTextDidChangeNotification:notification withSelector:@selector(sampleTextControllerDidChangeText:)];

}

- (BOOL)textView:(NSTextView *)textView shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString {
    return [self shouldHandleChangeInTextView:textView range:affectedCharRange replacementString:replacementString];
}



#pragma mark - Private

- (void)setupNotificationObservations {
    [super setupNotificationObservations];
    
    __block id snapshotLoaded = nil;
    snapshotLoaded = [[NSNotificationCenter defaultCenter] addObserverForName:kDidLoadSnapshotNotification
                                                                       object:nil
                                                                        queue:nil
                                                                   usingBlock:^(NSNotification *note) {
                                                                       self.textView.string = [note userInfo][kSampleTextStringKey];
                                                                       [self postSampleTextChangeNotification];
                                                                   }];
    __block id sampleTextChanged = nil;
    sampleTextChanged = [[NSNotificationCenter defaultCenter] addObserverForName:kDidChangeSampleTextNotification
                                                                          object:nil
                                                                           queue:nil
                                                                      usingBlock:^(NSNotification *note) {
                                                                          if( [self _shouldRespondToTextChange:note] ) {
                                                                              NSString *text = [note userInfo][kSampleTextStringKey];
                                                                              self.textView.string = text;
                                                                          }
                                                                      }];
    
    
    __block id regexInvalidated = nil;
    regexInvalidated = [[NSNotificationCenter defaultCenter] addObserverForName:kDidInvalidateRegexNotification
                                                                         object:nil
                                                                          queue:nil
                                                                     usingBlock:^(NSNotification *note) {
                                                                         dispatch_queue_t mainQueue = dispatch_get_main_queue();
                                                                         dispatch_async(mainQueue, ^{
                                                                             [self removeRegexMarkupWithNotification:note];
                                                                         });
                                                                     }];
}

@end
