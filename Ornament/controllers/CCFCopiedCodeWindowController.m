/**
 *   @file CCFCopiedCodeWindowController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-26 05:08:40
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFCopiedCodeWindowController.h"

@interface CCFCopiedCodeWindowController ()

@end

@implementation CCFCopiedCodeWindowController {
    CCFCopiedCodeCloseBlock _closeBlock;
}

- (id)initWithCloseBlock:(CCFCopiedCodeCloseBlock)block {
    self = [super initWithWindowNibName:@"CCFCopiedCodeAlert"];
    if( !self ) { return nil; }
    
    _closeBlock = block;
    self.window.delegate = self;
    return self;
}

- (IBAction)closeAction:(id)sender {
    self.window.delegate = nil;
    _closeBlock();
}

#pragma mark - NSWindowDelegate

- (void)windowWillClose:(NSNotification *)notification {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        _closeBlock();
    });
}

@end
