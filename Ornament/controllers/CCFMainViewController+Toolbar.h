/**
 *   @file CCFMainViewController+Toolbar.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-31 04:05:00
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFMainViewController.h"
#import "CCFNewSnapshotControllerDelegate.h"
#import "CCFLoadSnapshotControllerDelegate.h"

@interface CCFMainViewController (Toolbar) <CCFNewSnapshotControllerDelegate, CCFLoadSnapshotControllerDelegate, NSPopoverDelegate>

//@property (nonatomic, weak) IBOutlet NSToolbarItem *loadSnapshotItem;
- (void)setLoadSnapshotToolbarItemImage;

@end
