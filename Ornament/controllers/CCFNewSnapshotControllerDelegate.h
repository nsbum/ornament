/**
 *   @file CCFNewSnippetControllerDelegate.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 13:23:19
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFSnapshot;

@protocol CCFNewSnapshotControllerDelegate <NSObject>

- (void)newSnapshotController:(id)controller didAddSnapshot:(CCFSnapshot *)snapshot;
- (void)newSnapshotControllerDidCancel:(id)controller;

@end
