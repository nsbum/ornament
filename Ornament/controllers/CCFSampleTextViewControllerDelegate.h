/**
 *   @file CCFSampleTextViewControllerDelegate.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-23 21:35:47
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@protocol CCFSampleTextViewControllerDelegate <NSObject>

- (void)sampleTextControllerDidChangeText:(id)controller;

@end
