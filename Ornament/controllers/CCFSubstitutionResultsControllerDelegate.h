/**
 *   @file CCFSubstitutionResultsControllerDelegate.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 10:36:56
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@protocol CCFSubstitutionResultsControllerDelegate <NSObject>

- (void)substitutionResultsControllerDidChangeText:(id)controller;

@end
