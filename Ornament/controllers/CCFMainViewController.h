/**
 *   @file CCFMainViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-22 21:51:29
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */



@class DMTabBar;

@interface CCFMainViewController : NSViewController {
    //  IBoutlets
    IBOutlet NSButton *_loadSnapshotToolbarItemButton;
}

@property (nonatomic, weak) IBOutlet DMTabBar *tabBar;
@property (nonatomic, weak) IBOutlet NSView *contentView;
@property (nonatomic, weak) IBOutlet NSView *resultsContainerView;
@property (nonatomic, strong) NSUndoManager *undoManager;

@end
