/**
 *   @file CCFLoadSnapshotTableCellView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 22:30:05
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFOptionMarkerView;

@interface CCFLoadSnapshotTableCellView : NSTableCellView

@property (nonatomic, weak) IBOutlet NSTextField *descriptorField;
@property (nonatomic, weak) IBOutlet NSTextField *regexField;
@property (nonatomic, weak) IBOutlet NSTextField *sampleField;
@property (nonatomic, weak) IBOutlet CCFOptionMarkerView *multiLineMarkerView;
@property (nonatomic, weak) IBOutlet CCFOptionMarkerView *singleLineMarkerView;
@property (nonatomic, weak) IBOutlet CCFOptionMarkerView *verboseMarkerView;
@property (nonatomic, weak) IBOutlet CCFOptionMarkerView *ignoreCaseMarkerView;

@end
