//
//  CCFMainViewController.m
//  Ornament
//
//  Created by alanduncan on 10/22/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFMainViewController.h"
#import "CCFMainViewController+Toolbar.h"

#import "DMTabBar.h"
#import "DMTabBarItem.h"

#import "CCFMatchViewController.h"
#import "CCFSubstitutionViewController.h"
#import "CCFResultsViewController.h"

#import "CCFRegexResultsPackager.h"
#import "CCFRegex.h"

#import "CCFNewSnapshotWindowController.h"

typedef enum : NSInteger {
    CCFRegexModeMatch,
    CCFRegexModeSubstitute,
    CCFRegexModeIndeterminate = 0xFE
} CCFRegexMode;

static DMTabBarItem *tabBarItemWithImage(NSString *imageName, NSInteger tag) {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *imagePath = [bundle pathForResource:imageName ofType:nil];
    
    
    
    NSImage *img = [[NSImage alloc] initWithContentsOfFile:imagePath];
    CGFloat scaleFactor = (img.size.width > 24.0)?0.35:0.5;
    img.size = NSMakeSize(img.size.width * scaleFactor, img.size.height * scaleFactor);
    [img setTemplate:YES];
    
    DMTabBarItem *matchItem = [DMTabBarItem tabBarItemWithIcon:img tag:tag];
    [matchItem setRefusesFirstResponder:YES];
    
    if( tag == CCFRegexModeMatch ) {
        matchItem.toolTip = _(@"Match mode.  Regex will match against sample text.");
    }
    else if( tag == CCFRegexModeSubstitute ) {
        matchItem.toolTip = _(@"Substitution mode.  Regex will substitute for matches in sample text.");
    }
    return matchItem;
}

@interface CCFMainViewController ()

@end

@implementation CCFMainViewController {
    CCFMatchViewController *_matchController;               //  main view portion for doing simple matches
    CCFSubstitutionViewController *_substitutionController; //  alternate main view for doing substitutions
    CCFResultsViewController *_resultsViewController;       //  tabular view of matches
    CCFNewSnapshotWindowController *_newSnapshotSheetController;
    
    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    
    __block id regexDidMatch = nil;
    regexDidMatch = [[NSNotificationCenter defaultCenter] addObserverForName:kDidMatchRegexWithResults
                                                                      object:nil
                                                                       queue:nil
                                                                  usingBlock:^(NSNotification *note) {
                                                                      NSArray *matches = [note userInfo][kRegexMatchesKey];
                                                                      [self _showResults:matches];  // critical portion executes on main queue
                                                                  }];
    
    __block id regexDidInvalidate = nil;
    regexDidInvalidate = [[NSNotificationCenter defaultCenter] addObserverForName:kDidInvalidateRegexNotification
                                                                           object:nil
                                                                            queue:nil
                                                                       usingBlock:^(NSNotification *note) {
                                                                           [self _showResults:nil];
                                                                       }];
    return self;
}


- (void)awakeFromNib {
    [[self tabBar] setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self _setupTabBar];
    
    NSInteger defaultTab = [[NSUserDefaults standardUserDefaults] integerForKey:kRegexModeDefault];
    [self _setViewForRegexMode:defaultTab];
    
    [[self tabBar] handleTabBarItemSelection:^(DMTabBarItemSelectionType selectionType, DMTabBarItem *targetTabBarItem, NSUInteger targetTabBarItemIndex) {
        if( selectionType == DMTabBarItemSelectionType_DidSelect ) {
            [self _setViewForRegexMode:targetTabBarItemIndex];
            if( targetTabBarItemIndex == CCFRegexModeMatch ) {
                //  we're about to select the match mode; so copy the regex and the sample text over
                NSString *regexString = [[self substitutionController] regexString];
                [[self matchController] setRegexString:regexString];
                
                //  record centrally that we are matching
                [[CCFOrnamentData sharedData] setIsMatching:YES];
            }
            else if( targetTabBarItemIndex == CCFRegexModeSubstitute ) {
                // we've selected sub mode; so copy regex and sample text over
                NSString *regexString = [[self matchController] regexString];
                [[self substitutionController] setRegexString:regexString];
                
                //  record that we are substituting
                [[CCFOrnamentData sharedData] setIsMatching:NO];
            }
            
        }
    }];
    
    self.tabBar.selectedIndex = defaultTab;
    [self setLoadSnapshotToolbarItemImage];
    [self _setupResultsView];
}

- (void)_setupTabBar {
    self.tabBar.tabBarItems = @[tabBarItemWithImage(@"06-magnify@2x.png", CCFRegexModeMatch), tabBarItemWithImage(@"CCFPencil.png", CCFRegexModeSubstitute)];
}

- (void)_setupResultsView {
    self.resultsController.view.frame = self.resultsContainerView.bounds;
    [[self resultsContainerView] addSubview:self.resultsController.view];
}

- (void)_setViewForRegexMode:(CCFRegexMode)mode {
    if( mode == CCFRegexModeMatch ) {
        NSArray *subviews = [[self contentView] subviews];
        if( [subviews containsObject:[[self substitutionController] view]] ) {
            [[[self substitutionController] view] removeFromSuperview];
        }
        
        NSView *matchView = [[self matchController] view];
        matchView.frame = self.contentView.bounds;
        [[self contentView] addSubview:matchView];
        
        [[self matchController] setupResponderChain];
        
        [[CCFOrnamentData sharedData] setIsMatching:YES];
    }
    else if( mode == CCFRegexModeSubstitute ) {
        //  remove the match controller's view if it is present
        NSArray *subviews = [[self contentView] subviews];
        if( [subviews containsObject:[[self matchController] view]] ) {
            [[[self matchController] view] removeFromSuperview];
        }
        
        //  add the substitution view
        NSView *substitutionView = [[self substitutionController] view];
        substitutionView.frame = self.contentView.bounds;
        [[self contentView] addSubview:substitutionView];
        
        [[self substitutionController] setupResponderChain];
        
        [[CCFOrnamentData sharedData] setIsMatching:NO];
    }
}

- (CCFRegexMode)_currentMode {
    NSArray *subviews = [[self contentView] subviews];
    if( [subviews containsObject:[[self matchController] view]] ) {
        return CCFRegexModeMatch;
    }
    if( [subviews containsObject:[[self substitutionController] view]] ) {
        return CCFRegexModeSubstitute;
    }
    return CCFRegexModeIndeterminate;
}

#pragma mark - Interface actions


/*
 
#pragma mark - CCFMatchViewControllerDelegate

- (void)matchViewController:(id)controller didMatchWithResults:(NSArray *)results {
    //  we have matched results, must refresh the results view
    [self _showResults:results];
}

- (void)matchViewControllerDidInvalidateResults:(id)controller {
    //  the regex is invalid - so we should clear the results
    [self _showResults:nil];
}



#pragma mark - CCFSubstitutionViewControllerDelegate

- (void)substitutionViewController:(id)controller didMatchWithResults:(NSArray *)matches {
    [self _showResults:matches];
}

- (void)substitutionViewControllerDidInvalidateResults:(id)controller {
    [self _showResults:nil];
}

- (void)substitutionViewController:(id)controller didChangeRegex:(CCFRegex *)regex {
    //  the substitution view controller changed its regex
    //  pass the string back down the chain
    NSString *regexString = [regex string];
    [[self matchController] setRegexString:regexString];
}
 
 */


#pragma mark - Results

- (void)_showResults:(NSArray *)results {
    //  what is our current mode (match or substitute)?
    CCFRegexMode mode = [self _currentMode];
    
    //  do this regardless of mode
    NSString *source = self.matchController.sourceText;
    CCFRegexResultsPackager *packager = [[CCFRegexResultsPackager alloc] initWithTextCheckingResults:results source:source];
    //  package array of NSTextCheckingResult objects and source into array of CCFGroup objects
    NSArray *packagedResults = [packager packagedResults];
    //  provide packaged results to the results controller
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    dispatch_async(mainQueue, ^{
        [[self resultsController] setRegexResults:packagedResults];
    });
    
    if( mode == CCFRegexModeMatch ) {
        //  do this if matching
    }
    else if( mode == CCFRegexModeSubstitute ) {
        //  do this if substituting
    }
}

#pragma mark - Accessors

- (CCFMatchViewController *)matchController {
    if( !_matchController ) {
        _matchController = [[CCFMatchViewController alloc] initWithNib];
    }
    return _matchController;
}

- (CCFSubstitutionViewController *)substitutionController {
    if( !_substitutionController ) {
        _substitutionController = [[CCFSubstitutionViewController alloc] initWithNib];
    }
    return _substitutionController;
}

- (CCFResultsViewController *)resultsController {
    if( !_resultsViewController ) {
        _resultsViewController = [[CCFResultsViewController alloc] initWithNib];
    }
    return _resultsViewController;
}

@end
