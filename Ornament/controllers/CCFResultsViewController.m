/**
 *   @file CCFResultsViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-22 22:02:26
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFResultsViewController.h"
#import "CCFResultsOutlineView.h"
#import "CCFMatch.h"
#import "CCFGroup.h"
#import "CCFTreeNode.h"

//  name of our nib
static NSString * const ResultsControllerNibName = @"CCFResultsView";

//  column identifiers
static NSString * const ResultsTextIdentifier = @"com.cocoafactory.ResultsOutlineText";
static NSString * const ResultsRangeIdentifier = @"com.cocoafactory.ResultsOutlineRange";
static NSString * const ResultsGroupIdentifier = @"com.cocoafactory.ResultsOutlineGroupName";

//  converts user defaults key to the value we'll observe on NSUserDefaultsController
static NSString *userDefaultsControllerKeyWithKey(NSString *key) {
    return [@"values." stringByAppendingString:key];
}

//  adds user defaults key to observers of NSUserDefaultsController
static void addObserverForPreference(NSString *key, id self) {
    [[NSUserDefaultsController sharedUserDefaultsController] addObserver:self
                                                              forKeyPath:userDefaultsControllerKeyWithKey(key)
                                                                 options:NSKeyValueObservingOptionNew
                                                                 context:NULL];
}

//  removes user defaults key from observers of NSUserDefaultsController
static void removeObserverForPreference(NSString *key, id self) {
    [[NSUserDefaultsController sharedUserDefaultsController] removeObserver:self
                                                                 forKeyPath:userDefaultsControllerKeyWithKey(key)];
}

@interface CCFResultsViewController ()
@property (nonatomic, strong) NSMutableArray *sourceArray;      //  main source array for our outline view
@property (nonatomic, strong) NSMutableArray *expandedItems;    //  list of NSNumber (boxed NSUInteger) representing the item indices to keep expanded
@end

@implementation CCFResultsViewController {
    BOOL _isReloading;
    BOOL _shouldExpandAll;                  //  linked to NSUserDefaults preference kShouldPreserveResultExpansionKey
    BOOL _shouldPreserveExpansionState;     //  keep explicitly expanded notes expanded
}

#pragma mark - Object lifecycle

- (id)initWithNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self = [super initWithNibName:ResultsControllerNibName bundle:bundle];
    if( !self ) return nil;
    
    _isReloading = NO;
    
    self.expandedItems = [[NSMutableArray alloc] init];
    
    return self;
}

- (void)dealloc {
    removeObserverForPreference(kShouldPreserveResultExpansionKey, self);
    removeObserverForPreference(kShouldExpandAllResultItemsKey, self);
}

#pragma mark - Nib awaking

- (void)awakeFromNib {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        addObserverForPreference(kShouldExpandAllResultItemsKey, self);
        addObserverForPreference(kShouldPreserveResultExpansionKey, self);
    });
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if( [object isEqualTo:[NSUserDefaultsController sharedUserDefaultsController]] ) {
        NSKeyValueChange changeKind = [[change valueForKey:NSKeyValueChangeKindKey] unsignedIntegerValue];
        if( changeKind == NSKeyValueChangeSetting ) {
            NSNumber *changeNumber = [object valueForKeyPath:keyPath];
            BOOL flag = [changeNumber boolValue];
            if( [keyPath isEqualToString:@"values.ShouldExpandAllResultItemsKey"] ) {
                _shouldExpandAll = flag;
                //  if the user just asked to expand all, do it; otherwise collapse
                if( _shouldExpandAll ) {
                    [self expandAllItems];
                }
                else {
                    [self collapseAllItems];
                }
            }
            else if( [keyPath isEqualToString:@"values.ShouldPreserveResultExpansionKey"] ) {
                _shouldPreserveExpansionState = flag;
            }
        }
    }
}

#pragma mark - NSOutlineViewDataSource

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
    if( item == nil ) {
        return self.sourceArray.count;
    }
    else {
        return [item numberOfChildren];
    }
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
    return (item == nil) ? YES : ![item isLeaf];
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
    if( item == nil ) {
        if( index < [[self sourceArray] count] ) {
            return [[self sourceArray] objectAtIndex:index];
        }
        else {
            return nil;
        }
    }
    else {
        if( index < [[item children] count] ) {
            return [[item children] objectAtIndex:index];
        }
        else {
            return nil;
        }
    }
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
    return item;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isGroupItem:(id)item {
    return [item isGroup];
}

#pragma mark - NSOutlineViewDelegate

- (NSView *)outlineView:(NSOutlineView *)outlineView viewForTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    NSString *columnIdentifier = tableColumn.identifier;
    if( [columnIdentifier isEqualToString:ResultsGroupIdentifier] ) {
        return [outlineView makeViewWithIdentifier:columnIdentifier owner:self];
    }
    else if( [columnIdentifier isEqualToString:ResultsRangeIdentifier] ) {
        return [outlineView makeViewWithIdentifier:columnIdentifier owner:self];
    }
    else if( [columnIdentifier isEqualToString:ResultsTextIdentifier] ) {
        return [outlineView makeViewWithIdentifier:columnIdentifier owner:self];
    }
    return nil;
}

#pragma mark Outline expansion

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldExpandItem:(id)item {
    if( [item isLeaf] ) {
        return NO;
    }
    if( _shouldExpandAll ) {
        return YES;
    }
    else {
        if( _shouldPreserveExpansionState ) {
            NSUInteger index = [self indexOfItem:item];
            [[self expandedItems] addObject:[NSNumber numberWithUnsignedInteger:index]];
        }
    }
    
    return YES;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldCollapseItem:(id)item {
    if( !_shouldExpandAll && _shouldPreserveExpansionState )  {
        NSUInteger index = [self indexOfItem:item];
        [[self expandedItems] removeObject:[NSNumber numberWithUnsignedInteger:index]];
    }
    return YES;
}


#pragma mark - Accessors

- (void)setRegexResults:(NSArray *)regexResults {
    _regexResults = regexResults;
    _sourceArray = nil;
    [[self matchOutlineView] reloadData];
    [[self sourceArray] self];
    [[self matchOutlineView] reloadData];
    if( _shouldExpandAll ) {    
        [self expandAllItems];
    }
    else {
        if( !_shouldPreserveExpansionState ) {
            return;
        }
        //  user does not want to expand all items
        //  user wnats to preserve state of previsouly-expanded items
        //  expand items that were previously explicitly expanded
        NSMutableArray *expandedItemsCopy = [[NSArray arrayWithArray:self.expandedItems] mutableCopy];
        for( id indexNumber in expandedItemsCopy ) {
            // get the index from the boxed value in our array
            NSUInteger index = [indexNumber unsignedIntegerValue];
            if( index < [[self sourceArray] count] ) {
                id item = [[self sourceArray] objectAtIndex:index];
                [[self matchOutlineView] expandItem:item];
            }
            else {
                //  if this is no longer a valid item, then remove it
                [[self expandedItems] removeObject:indexNumber];
            }
        }
    }
}

- (NSMutableArray *)sourceArray {
    if( !_sourceArray ) {
        _sourceArray = [[NSMutableArray alloc] init];
        
        for( CCFMatch *match in self.regexResults ) {
            CCFTreeNode *matchNode = [[CCFTreeNode alloc] init];
            matchNode.nodeTitle = match.title;
            matchNode.rangeString = match.rangeString;
            matchNode.matchText = match.matchedText;
            matchNode.isGroup = NO;
            matchNode.isLeaf = NO;
            
            for( CCFGroup *group in match.groups ) {
                CCFTreeNode *groupNode = [[CCFTreeNode alloc] init];
                groupNode.nodeTitle = group.title;
                groupNode.rangeString = group.rangeString;
                groupNode.matchText = group.matchedText;
                groupNode.isGroup = NO;
                groupNode.isLeaf = YES;
                
                [matchNode addChild:groupNode];
            }
            [_sourceArray addObject:matchNode];
        }
    }
    return _sourceArray;
}


#pragma mark - Private

- (void)expandAllItems {
    for( CCFTreeNode *node in self.sourceArray ) {
        [[self matchOutlineView] expandItem:node];
    }
}

- (void)collapseAllItems {
    for( CCFTreeNode *node in self.sourceArray ) {
        [[self matchOutlineView] collapseItem:node];
    }
}

//  returns the index of item in our source array
- (NSUInteger)indexOfItem:(id)item {
    return [[self sourceArray] indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if( [item isEqual:obj] ) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
}
@end
