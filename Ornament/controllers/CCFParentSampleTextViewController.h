/**
 *   @file CCFParentSampleTextViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-25 06:21:15
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFSourceTextView;

@interface CCFParentSampleTextViewController : NSViewController {
    id _classDelegate;
}

extern void WhilePreservingSelectionInTextView(NSObject *self, BasicBlock block);

@property (nonatomic, assign) IBOutlet CCFSourceTextView *textView;

+ (NSString *)nibName;
- (id)initWithNib;
- (BOOL)shouldHandleChangeInTextView:(NSTextView *)textView range:(NSRange)affectedCharRange replacementString:(NSString *)replacementString;
- (void)fixAttributes;
- (NSDictionary *)attributesForSampleText;
- (void)postSampleTextChangeNotification;
- (void)performMarkupWithNotification:(NSNotification *)note;
- (void)handleTextDidChangeNotification:(NSNotification *)note withSelector:(SEL)selector;
- (IBAction)openFileAction:(id)sender;
- (void)setupNotificationObservations;

@end
