//
//  CCFRatingReminderWindowControllerDelegate.h
//  Ornament
//
//  Created by alanduncan on 2/4/13.
//  Copyright (c) 2013 Cocoa Factory, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CCFRatingReminderWindowControllerDelegate <NSObject>

- (void)ratingWindowControllerDidSelectRate:(id)controller;
- (void)ratingWindowControllerDidSelectDefer:(id)controller;
- (void)ratingWindowControllerDidSelectCancel:(id)controller;

@end
