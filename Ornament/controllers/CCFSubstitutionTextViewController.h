/**
 *   @file CCFSubstitutionTextViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 08:59:57
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSubstituteTextView.h"

@interface CCFSubstitutionTextViewController : NSViewController <NSTextViewDelegate>

@property (nonatomic, assign) IBOutlet CCFSubstituteTextView *textView;

- (id)initWithNib;

@end
