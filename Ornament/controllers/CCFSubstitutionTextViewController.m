/**
 *   @file CCFSubstitutionTextViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 09:00:04
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSubstitutionTextViewController.h"
#import "CCFRegex.h"

static NSString * const SubstituteTextNibName = @"CCFSubstituteTextView";

@interface CCFSubstitutionTextViewController ()

@end

@implementation CCFSubstitutionTextViewController

- (id)initWithNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self = [super initWithNibName:SubstituteTextNibName bundle:bundle];
    if( !self ) return nil;
    
    return self;
}

- (void)awakeFromNib {
    self.textView.delegate = self;
    
    //  add attributes for our view
    [[self textView] setTypingAttributes:[self _attributesForSubstitutionText]];
}

#pragma mark - NSTextViewDelegate

- (void)textDidChange:(NSNotification *)notification {
    //TODO: unsure whether both of these are actually required
    // need to sort this out
    [[CCFOrnamentData sharedData] setSubstitutionString:self.textView.string];
    [[[CCFOrnamentData sharedData] regex] setReplacementString:self.textView.string];
    //  notify observers that the substitution text has change
    NSDictionary *userInfo = @{kSubstituteStringKey:self.textView.string};
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeSubstituteTextNotification object:self userInfo:userInfo];
}

//  Provides dictionary of attributes used to reformat substitution text
- (NSDictionary *)_attributesForSubstitutionText {
    NSFont *fixedWidth = [[NSFontManager sharedFontManager] fontWithFamily:@"Menlo" traits:0 weight:5 size:13.0];
    NSDictionary *attributes = @{ NSFontAttributeName : fixedWidth,NSForegroundColorAttributeName : [NSColor textColor] };
    
    return attributes;
}

@end
