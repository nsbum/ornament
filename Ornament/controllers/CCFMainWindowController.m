//
//  CCFMainWindowController.m
//  Ornament
//
//  Created by alanduncan on 11/27/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFMainWindowController.h"
#import "CCFMainViewController.h"

@interface CCFMainWindowController ()

@end

@implementation CCFMainWindowController


- (void)showWindow:(id)sender {
    [super showWindow:sender];
    self.viewController.undoManager = self.window.undoManager;
}


@end
