/**
 *   @file CCFCodePrefsViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-25 14:20:58
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "MASPreferencesViewController.h"

@interface CCFCodePrefsViewController : NSViewController <MASPreferencesViewController>

- (id)initWithNib;

@end
