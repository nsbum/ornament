/**
 *   @file CCFSubstitutedTextView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 05:27:56
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSubstitutionResultsLayoutManager.h"

@interface CCFSubstitutedTextView : NSTextView

@property (nonatomic, assign) BOOL isPasteOperation;
@property (nonatomic, assign) BOOL isChanging;


@end
