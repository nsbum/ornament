/**
 *   @file CCFMainViewController+Toolbar.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-31 04:04:52
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFMainViewController+Toolbar.h"
#import <objc/runtime.h>

#import "CCFRegexOptions.h"
#import "CCFRegex.h"
#import "CCFSnapshot.h"

#import "CCFLoadSnapshotViewController.h"
#import "CCFNewSnapshotWindowController.h"
#import "CCFNewSnapshotControllerDelegate.h"

static char LoadSnapshotPopoverKey;
static char AddSnapshotControllerKey;

@implementation CCFMainViewController (Toolbar)


- (IBAction)loadSnapshotAction:(id)sender {
    [[self loadSnapshotPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

- (IBAction)addSnapshotAction:(id)sender {
    //  check if the snapshot is source is empty or not
    NSString *regexString = [[CCFOrnamentData sharedData] regexString];
    NSString *sampleString = [[CCFOrnamentData sharedData] sampleText];
    if( regexString == nil || sampleString == nil ) {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"OK button title")];
        [alert setMessageText:NSLocalizedString(@"Missing information", @"Missing snapshot source text")];
        [alert setInformativeText:NSLocalizedString(@"Unable to make a Snapshot because either the expression or the source text is empty", @"Missing snapshot detail")];
        [alert setAlertStyle:NSWarningAlertStyle];
        [alert beginSheetModalForWindow:self.view.window modalDelegate:nil didEndSelector:NULL contextInfo:NULL];
        return;
    }
    
    NSWindow *sheet = [[self newSnapshotController] window];
    [NSApp beginSheet:sheet modalForWindow:self.view.window modalDelegate:nil didEndSelector:NULL contextInfo:NULL];
}

#pragma mark - Public

- (void)setLoadSnapshotToolbarItemImage {
    return;
    NSImage *image = [NSImage imageNamed:@"CCFLoadSnapshot.pdf"];
    [image setSize:CGSizeMake(16.25, 13)];
    //[image setBackgroundColor:[NSColor clearColor]];
    [_loadSnapshotToolbarItemButton setImage:image];
}

#pragma mark - Accessors

- (NSPopover *)loadSnapshotPopover {
    NSPopover *popover = (NSPopover *)objc_getAssociatedObject(self, &LoadSnapshotPopoverKey);
    if( !popover ) {
        CCFLoadSnapshotViewController *vc = [[CCFLoadSnapshotViewController alloc] initWithNib];
        vc.delegate = self;
        popover = [[NSPopover alloc] init];
        popover.contentViewController = vc;
        popover.behavior = NSPopoverBehaviorTransient;
        [popover setDelegate:(id)self];
        objc_setAssociatedObject(self, &LoadSnapshotPopoverKey, popover, OBJC_ASSOCIATION_RETAIN);
    }
    return popover;
}

- (CCFNewSnapshotWindowController *)newSnapshotController {
    CCFNewSnapshotWindowController *controller = (CCFNewSnapshotWindowController *)objc_getAssociatedObject(self, &AddSnapshotControllerKey);
    if( !controller ) {
        controller = [[CCFNewSnapshotWindowController alloc] initWithNib];
        controller.delegate = self;
        objc_setAssociatedObject(self, &AddSnapshotControllerKey, controller, OBJC_ASSOCIATION_RETAIN);
    }
    return controller;
}

#pragma mark - NSPopoverDelegate

- (void)popoverWillClose:(NSNotification *)note {
    objc_setAssociatedObject(self, &LoadSnapshotPopoverKey, nil, OBJC_ASSOCIATION_ASSIGN);
}

#pragma mark - CCFNewSnapshotControllerDelegate

- (void)newSnapshotControllerDidCancel:(id)controller {
    [[[self newSnapshotController] window] orderOut:self];
    objc_setAssociatedObject(self, &AddSnapshotControllerKey, nil, OBJC_ASSOCIATION_ASSIGN);
}

- (void)newSnapshotController:(id)controller didAddSnapshot:(CCFSnapshot *)snapshot {
    [[CCFOrnamentData sharedData] addSnapshot:snapshot];
    [[[self newSnapshotController] window] orderOut:self];
    objc_setAssociatedObject(self, &AddSnapshotControllerKey, nil, OBJC_ASSOCIATION_ASSIGN);
}

#pragma mark - CCFLoadSnapShotControllerDelegate

- (void)loadSnapshotController:(id)controller didSelectSnapshot:(CCFSnapshot *)snapshot {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [[self loadSnapshotPopover] close];
        objc_setAssociatedObject(self, &LoadSnapshotPopoverKey, nil, OBJC_ASSOCIATION_ASSIGN);
    });
    
    CCFRegex *regex = [[CCFRegex alloc] initWithString:snapshot.regexString];
    NSString *sampleText = snapshot.sampleText;
    [self loadSnapshotWithRegex:regex sampleString:sampleText];
}

- (void)loadSnapshotWithRegex:(CCFRegex *)regex sampleString:(NSString *)sampleText {
    CCFRegex *currentRegex = [[CCFOrnamentData sharedData] regex];
    NSString *currentSampleText = [[CCFOrnamentData sharedData] sampleText];
    if( !currentRegex ) {
        currentRegex = [[CCFRegex alloc] initWithString:@""];
    }
    if( !currentSampleText ) {
        currentSampleText = @"";
    }
    
    [[CCFOrnamentData sharedData] setRegex:regex];
    [[CCFOrnamentData sharedData] setSampleText:sampleText];
    
    NSDictionary *userInfo = @{kSampleTextStringKey:sampleText, kRegexStringKey:regex.string};
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidLoadSnapshotNotification object:self userInfo:userInfo];
    
}

- (void)undoLoadSnapshotWithRegex:(CCFRegex *)regex sampleText:(NSString *)sampleText {
    if( !regex.string ) { regex.string = @""; }
    if( !sampleText ) { sampleText = @""; }
    
    [[CCFOrnamentData sharedData] setRegex:regex];
    [[CCFOrnamentData sharedData] setSampleText:sampleText];
    
    NSDictionary *userInfo = @{kSampleTextStringKey:sampleText, kRegexStringKey:regex.string};
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidLoadSnapshotNotification object:self userInfo:userInfo];
}


@end
