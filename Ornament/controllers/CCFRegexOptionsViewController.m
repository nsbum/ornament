//
//  CCFRegexOptionsViewController.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFRegexOptionsViewController.h"



static NSString * const RegexOptionsNibName = @"CCFRegexOptionsView";

@interface CCFRegexOptionsViewController ()

@end

@implementation CCFRegexOptionsViewController

- (id)initWithNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self = [super initWithNibName:RegexOptionsNibName bundle:bundle];
    if( !self ) return nil;
    
    return self;
}

@end
