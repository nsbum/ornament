//
//  CCFMatchViewController.h
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFRegexViewControllerDelegate.h"
#import "CCFSampleTextViewControllerDelegate.h"

//@protocol CCFMatchViewControllerDelegate;

@interface CCFMatchViewController : NSViewController <CCFRegexViewControllerDelegate, CCFSampleTextViewControllerDelegate>

@property (nonatomic, weak) IBOutlet NSView *regexContainerView;
@property (nonatomic, weak) IBOutlet NSView *sampleTextContainerView;
//@property (nonatomic, assign) id <CCFMatchViewControllerDelegate> delegate;

@property (nonatomic, readonly) NSString *sourceText;

- (id)initWithNib;

- (NSString *)regexString;
- (void)setRegexString:(NSString *)regexString;

- (void)setupResponderChain;


@end
