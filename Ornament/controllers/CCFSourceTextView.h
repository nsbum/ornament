/**
 *   @file CCFSourceTextView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-27 04:21:38
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFSourceTextView : NSTextView <NSTextViewDelegate>

@property (nonatomic, assign) BOOL isPasteOperation;
@property (nonatomic, assign) BOOL isChanging;

@end
