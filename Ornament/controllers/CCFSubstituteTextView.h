/**
 *   @file CCFSubstituteTextView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 13:07:18
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFSubstituteTextView : NSTextView

@end
