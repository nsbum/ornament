/**
 *   @file CCFRegexTextView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 11:40:16
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFRegexTextView : NSTextView

- (void)disableCodeMenuItems;
- (void)enableCodeMenuItems;

@end
