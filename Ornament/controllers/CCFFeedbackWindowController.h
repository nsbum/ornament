//
//  CCFFeedbackWindowController.h
//  Ornament
//
//  Created by alanduncan on 11/27/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CCFFeedbackWindowController : NSWindowController

@property (nonatomic, strong) NSString *appVersionString;
@property (nonatomic, strong) NSString *systemInfoString;
@property (nonatomic, strong) NSString *systemVersionString;
@property (nonatomic, strong) NSString *emailString;
@property (nonatomic, assign) NSInteger selectedIssueType;
@property (nonatomic, strong) NSString *messageString;
@property (nonatomic, strong) NSString *ticketNumberString;

@property (nonatomic, assign) BOOL missingEmailAddress;
@property (nonatomic, assign) BOOL missingMessage;
@property (nonatomic, assign) BOOL taskRunning;

- (id)initWithNibCompletionBlock:(BasicBlock)block;

@end
