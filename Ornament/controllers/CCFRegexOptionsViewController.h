//
//  CCFRegexOptionsViewController.h
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFRegexOptions.h"

@interface CCFRegexOptionsViewController : NSViewController

@property (nonatomic, strong) CCFRegexOptions *regexOption;

- (id)initWithNib;

@end
