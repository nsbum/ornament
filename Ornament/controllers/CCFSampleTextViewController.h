//
//  CCFSampleTextViewController.h
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFParentSampleTextViewController.h"

@protocol CCFSampleTextViewControllerDelegate;

@class CCFSourceTextView;

@interface CCFSampleTextViewController : CCFParentSampleTextViewController

@property (nonatomic, strong) IBOutlet CCFOrnamentData *sharedData;
@property (nonatomic, assign) id <CCFSampleTextViewControllerDelegate> delegate;


@end
