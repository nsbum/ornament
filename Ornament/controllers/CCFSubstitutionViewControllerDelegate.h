/**
 *   @file CCFSubstitutionViewControllerDelegate.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 11:31:16
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFRegex;

@protocol CCFSubstitutionViewControllerDelegate <NSObject>

@required
- (void)substitutionViewControllerDidInvalidateResults:(id)controller;
- (void)substitutionViewController:(id)controller didMatchWithResults:(NSArray *)matches;

@optional
- (void)substitutionViewController:(id)controller didChangeRegex:(CCFRegex *)regex;

@end
