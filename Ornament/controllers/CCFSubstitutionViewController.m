/**
 *   @file CCFSubstitutionViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 09:03:46
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSubstitutionViewController.h"
#import "CCFSubstitutionViewControllerDelegate.h"
#import "CCFSubstitutionTextViewController.h"
#import "CCFRegexViewController.h"
#import "CCFSampleTextViewController.h"
#import "CCFSubstitutionResultsViewController.h"
#import "CCFSubstitutionSourceTextViewController.h"

#import "CCFRegex.h"
#import "CCFRegexOptions.h"
#import "CCFSourceTextView.h"
#import "CCFSubstitutedTextView.h"

static NSString * const SubstitutionControllerNibName = @"CCFSubstitutionView";

@interface CCFSubstitutionViewController ()

@end

@implementation CCFSubstitutionViewController {
    CCFRegexViewController *_regexController;
    CCFSubstitutionSourceTextViewController *_sourceTextController;
    CCFSubstitutionTextViewController *_substitutionTextViewController;
    CCFSubstitutionResultsViewController *_substitutionResultsController;
}

- (id)initWithNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self = [super initWithNibName:SubstitutionControllerNibName bundle:bundle];
    if( !self ) return nil;
    
    //  observe for changes in the substitution text so that we can redo the
    //  the substitution regex to keep up with it
    __block id substitutionTextChanged = nil;
    substitutionTextChanged = [[NSNotificationCenter defaultCenter] addObserverForName:kDidChangeSubstituteTextNotification
                                                                                object:nil
                                                                                 queue:nil
                                                                            usingBlock:^(NSNotification *note) {
                                                                                [self _validateAndRunRegex];
                                                                            }];
    
    return self;
}

- (void)awakeFromNib {
    //  setup the regex view and set us as the delegate
    [self _installRegexView];
    [[self regexController] setDelegate:self];
    
    //  setup the substitution text view (the view where user enters text
    //  text that gets substituted
    [self _installSubstitutionTextView];
    
    //  setup the sample text view (Source)
    [self _installSourceTextView];
    [[self sourceTextController] setDelegate:self];
    
    //  lastly the results view
    [self _installSubstitutionResultsView];
}

- (void)_installRegexView {
    NSView *regexView = [[self regexController] view];
    regexView.frame = self.regexContainerView.bounds;
    [[self regexContainerView] addSubview:regexView];
}

- (void)_installSubstitutionTextView {
    //  add the substitution text view
    NSView *substitutionTextView = [[self substitutionTextController] view];
    substitutionTextView.frame = self.substitutionContainerView.bounds;
    [[self substitutionContainerView] addSubview:substitutionTextView];
}

- (void)_installSourceTextView {
    NSView *sourceTextView = [[self sourceTextController] view];
    sourceTextView.frame = self.sampleTextContainerView.bounds;
    [[self sampleTextContainerView] addSubview:sourceTextView];
}


- (void)_installSubstitutionResultsView {
    //  add the substitution results view
    NSView *substitutionResultsView = [[self substitutionResultsController] view];
    substitutionResultsView.frame = self.substitutionResultsContainerView.bounds;
    [[self substitutionResultsContainerView] addSubview:substitutionResultsView];
}

#pragma mark - Public

- (NSString *)regexString {
    return [[[self regexController] textView] string];
}

- (void)setRegexString:(NSString *)string {
    if( !string || [string length] == 0 ) return;
    [[[self regexController] textView] setString:@""];
    [[[self regexController] textView] insertText:string];
}

- (void)setupResponderChain {
    CCFRegexTextView *regexView = [[self regexController] textView];
    NSTextView *subInputView = [[self substitutionTextController] textView];
    CCFSourceTextView *sourceView = [[self sourceTextController] textView];
    
    [regexView setNextKeyView:subInputView];
    [subInputView setNextKeyView:sourceView];
    [sourceView setNextKeyView:regexView];
}

- (void)makeRegexViewFirstResponder {
    CCFRegexTextView *regexTextView = [[self regexController] textView];
    NSWindow *mainWindow = [[self view] window];
    [mainWindow makeFirstResponder:regexTextView];
}

#pragma mark - Accessors

- (CCFRegexViewController *)regexController {
    if( !_regexController ) {
        _regexController = [[CCFRegexViewController alloc] initWithNib];
    }
    return _regexController;
}

- (CCFSubstitutionTextViewController *)substitutionTextController {
    if( !_substitutionTextViewController ) {
        _substitutionTextViewController = [[CCFSubstitutionTextViewController alloc] initWithNib];
    }
    return _substitutionTextViewController;
}

//  returns the source (sample text) controller instance
- (CCFSubstitutionSourceTextViewController *)sourceTextController {
    if( !_sourceTextController ) {
        _sourceTextController = [[CCFSubstitutionSourceTextViewController alloc] initWithNib];
    }
    return _sourceTextController;
}


- (CCFSubstitutionResultsViewController *)substitutionResultsController {
    if( !_substitutionResultsController ) {
        _substitutionResultsController = [[CCFSubstitutionResultsViewController alloc] initWithNib];
    }
    return _substitutionResultsController;
}


#pragma mark - CCFRegexViewControllerDelegate


- (void)regexController:(id)controller didValidateRegex:(CCFRegex *)regex {
    //  the regex is valid, so we need to try to match against the
    //  source text
    [self _matchAndNotifyDelegateUsingRegex:regex];
}

- (void)regexController:(id)controller didInvalidateRegex:(CCFRegex *)regex {
    //  there was a change in regex that invalidated syntax, notify observers
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidInvalidateRegexNotification object:self];
}

- (void)regexController:(id)controller didChangeRegex:(CCFRegex *)regex {
    if( [[self delegate] respondsToSelector:@selector(substitutionViewController:didChangeRegex:)] ) {
        [[self delegate] substitutionViewController:self didChangeRegex:regex];
    }
}

#pragma mark - CCFSubstitutionSourceTextControllerDelegate

- (void)substitutionSourceTextControllerDidChangeText:(id)controller {
    [self _validateAndRunRegex];
}

- (void)_validateAndRunRegex {
    //  create a regex object to run
    CCFRegex *regex = [[CCFRegex alloc] initWithString:[[[self regexController] textView] string]];
    //  find any options that have been selected
    NSRegularExpressionOptions options = [[self regexController] regexOptions];
    regex.options = [CCFRegexOptions optionsObjectWithOptions:options];
    
    if( [regex regexIsValid] ) {
        [self _matchAndNotifyDelegateUsingRegex:regex];
    }
    else {
        //TODO: finish this
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidInvalidateRegexNotification object:self];
    }
}

//TODO: this is duplicated with CCFMatchViewController - may need to move to a helper
- (void)_matchAndNotifyDelegateUsingRegex:(CCFRegex *)regex {
    NSString *source = [[[self sourceTextController] textView] string];
    NSArray *matches = [regex matchesWithSourceText:source];
    if( [matches count] != 0 ) {
        //  fire notification that we have new match results
        NSDictionary *userInfo = @{kRegexMatchesKey : matches, kRegexKey:regex};
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidMatchRegexWithResults object:self userInfo:userInfo];
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidInvalidateRegexNotification object:self];
    }
}

@end
