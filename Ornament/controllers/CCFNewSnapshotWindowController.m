/**
 *   @file CCFNewSnippetWindowController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 13:19:39
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFNewSnapshotWindowController.h"
#import "CCFNewSnapshotControllerDelegate.h"
#import "CCFSnapshot.h"
#import "CCFRegex.h"

static NSString * const NewSnapshotNibNib = @"CCFNewSnapshotSheet";

@interface CCFNewSnapshotWindowController ()

@end

@implementation CCFNewSnapshotWindowController

- (id)initWithNib {
    self = [super initWithWindowNibName:NewSnapshotNibNib owner:self];
    if( !self ) return nil;
    
    return self;
}

#pragma mark - Interface actions

- (void)awakeFromNib {
    NSString *regexString = [[CCFOrnamentData sharedData] regexString];
    if( regexString ) {
        self.regexField.stringValue = regexString;
    }
    else {
        self.regexField.stringValue = @"No regex";
    }
    NSString *sampleString = [[CCFOrnamentData sharedData] sampleText];
    if( sampleString ) {
        self.sampleTextField.stringValue = sampleString;
    }
    else {
        self.sampleTextField.stringValue = @"No sample data";
    }
}

- (IBAction)buttonAction:(id)sender {
    if( [sender tag] == NSCancelButton ) {
        [[self delegate] newSnapshotControllerDidCancel:self];
    }
    else {
        CCFSnapshot *snapShot = [[CCFSnapshot alloc] init];
        [snapShot setRegexString:[[CCFOrnamentData sharedData] regexString]];
        [snapShot setSampleText:[[CCFOrnamentData sharedData] sampleText]];
        [snapShot setDescriptor:self.descriptionField.stringValue];
        
        NSRegularExpressionOptions options = [[CCFOrnamentData sharedData] currentOptions];
        snapShot.regexOptions = options;
        
        [[self delegate] newSnapshotController:self didAddSnapshot:snapShot];
    }
    [NSApp endSheet:self.window];
}

@end
