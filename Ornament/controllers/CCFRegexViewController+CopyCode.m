/**
 *   @file CCFRegexViewController+CopyCode.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-31 11:31:37
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFRegexViewController+CopyCode.h"
#import "CCFCodeFormatter.h"

@implementation CCFRegexViewController (CopyCode)

- (IBAction)copyCodeMenuAction:(id)sender {
    CCFCodeFormatter *formatter = [[CCFCodeFormatter alloc] initWithCodeType:(CCFCodeType)[sender tag]];
    CCFRegex *regex = [[CCFOrnamentData sharedData] regex];
    formatter.regex = regex;
    NSString *code = formatter.codeString;
    formatter = nil;
    
    NSPasteboard *pasteBoard = [NSPasteboard generalPasteboard];
    [pasteBoard clearContents];
    if( ![pasteBoard writeObjects:@[code]] ) {
        NSLog(@"%s - Unable to write to pb!",__FUNCTION__);
    }
    
}

@end
