//
//  CCFRatingReminderWindowController.m
//  Ornament
//
//  Created by alanduncan on 2/4/13.
//  Copyright (c) 2013 Cocoa Factory, LLC. All rights reserved.
//

static NSString * const NibName = @"CCFReminderWindowController";

typedef NS_ENUM(NSInteger, CCFRatingButton) {
    CCFRatingButtonRate,
    CCFRatingButtonDefer,
    CCFRatingButtonCancel
};

#import "CCFRatingReminderWindowController.h"
#import "CCFRatingReminderWindowControllerDelegate.h"

///---------------------------------------------------------------------------------------
///    Private methods and properties
///---------------------------------------------------------------------------------------
@interface CCFRatingReminderWindowController()

@end



@implementation CCFRatingReminderWindowController

- (id)initWithNib {
    self = [super initWithWindowNibName:NibName];
    if( !self ) return nil;
    
    return self;
}

#pragma mark - Interface actions

- (IBAction)buttonClicked:(id)sender {
    CCFRatingButton button = (CCFRatingButton)[sender tag];
    switch( button ) {
        case CCFRatingButtonRate:
            [[self delegate] ratingWindowControllerDidSelectRate:self];
            break;
        case CCFRatingButtonCancel:
            [[self delegate] ratingWindowControllerDidSelectCancel:self];
            break;
        case CCFRatingButtonDefer:
            [[self delegate] ratingWindowControllerDidSelectDefer:self];
            break;
    }
}

@end
