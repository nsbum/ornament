/**
 *   @file CCFLoadSnapshotTableRowView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 22:33:18
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFLoadSnapshotTableRowView.h"

@implementation CCFLoadSnapshotTableRowView


- (void)drawBackgroundInRect:(NSRect)dirtyRect {
    [super drawBackgroundInRect:dirtyRect];
    
    CGFloat white = (self.even)?0.91:0.94;
    [[NSColor colorWithCalibratedWhite:white alpha:1.000] setFill];
    [NSBezierPath fillRect:dirtyRect];
}

- (void)drawSeparatorInRect:(NSRect)dirtyRect {
    NSRect rect = self.bounds;
    [[NSColor colorWithCalibratedWhite:0.86 alpha:1.000] setFill];
    NSRect separatorRect = NSMakeRect(NSMinX(rect), rect.size.height - 1.0f, NSWidth(rect), 1.0f);
    [NSBezierPath fillRect:separatorRect];
}

@end
