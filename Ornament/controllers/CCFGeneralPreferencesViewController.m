//
//  CCFGeneralPreferencesViewController.m
//  Ornament
//
//  Created by alanduncan on 12/20/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFGeneralPreferencesViewController.h"

DEFINE_NSSTRING(CCFGeneralPreferences);

@interface CCFGeneralPreferencesViewController ()

@end

@implementation CCFGeneralPreferencesViewController

- (id)initWithNib {
    self = [super initWithNibName:CCFGeneralPreferences bundle:nil];
    if( !self ) return nil;
    
    return self;
}

#pragma mark - MASPreferencesViewController

- (NSString *)identifier {
    return @"General";
}

- (NSString *)toolbarItemLabel {
    return NSLocalizedString(@"General", @"General preferences");
}

- (NSImage *)toolbarItemImage {
    return [NSImage imageNamed:@"PrefApp.icns"];
}

- (NSView *)initialKeyView {
    return self.view;
}

@end
