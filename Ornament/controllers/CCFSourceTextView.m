/**
 *   @file CCFSourceTextView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-27 04:21:46
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSourceTextView.h"
#import "CCFSourceResultParagraphStyle.h"

@implementation CCFSourceTextView {

}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    
    CCFSourceResultParagraphStyle *style = [[CCFSourceResultParagraphStyle alloc] init];
    [self setDefaultParagraphStyle:style];
    
    
    return self;
}


- (BOOL)readSelectionFromPasteboard:(NSPasteboard *)pboard {
    //  reject pboard contents that aren't interesting to us
    NSArray *classes = @[[NSString class],[NSAttributedString class]];
    NSDictionary *options = [NSDictionary dictionary];
    NSArray *strings = [pboard readObjectsForClasses:classes options:options];
    if( strings != nil ) {
        [self setIsPasteOperation:YES];
        return [super readSelectionFromPasteboard:pboard];
    }
    return NO;
}

- (void)insertTab:(id)sender {
    [[self window] selectNextKeyView:self];
}

- (void)insertBacktab:(id)sender {
    [[self window] selectPreviousKeyView:self];
}

@end
