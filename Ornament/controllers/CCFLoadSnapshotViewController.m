/**
 *   @file CCFLoadSnapshotViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 22:19:35
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFLoadSnapshotViewController.h"
#import "CCFLoadSnapshotControllerDelegate.h"
#import "CCFSnapshot.h"
#import "CCFLoadSnapshotTableCellView.h"
#import "CCFLoadSnapshotTableRowView.h"
#import "CCFOptionMarkerView.h"


static NSString * const LoadSnapshotNib = @"CCFLoadSnapshotPopupView";
static NSString * const LoadSnapshotCellID = @"com.cocoafactory.LoadSnapshotMainCell";

@interface CCFLoadSnapshotViewController ()

@end

@implementation CCFLoadSnapshotViewController {
    NSMutableArray *_tableContents;
}

- (id)initWithNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self = [super initWithNibName:LoadSnapshotNib bundle:bundle];
    if( !self ) return nil;
    
    return self;
}

#pragma mark - NSNibAwaking

- (void)awakeFromNib {
    _tableContents = [[CCFOrnamentData sharedData] snapshots];
}

#pragma mark - NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return _tableContents.count;
}

- (NSTableRowView *)tableView:(NSTableView *)tableView rowViewForRow:(NSInteger)row {
    CCFLoadSnapshotTableRowView *rowView = [[CCFLoadSnapshotTableRowView alloc] initWithFrame:NSMakeRect(0, 0, 100, 100)];
    rowView.objectValue = [self entityForRow:row];
    if( row % 2 ) {
        rowView.even = YES;
    }
    return rowView;
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    CCFLoadSnapshotTableCellView *cellView = [tableView makeViewWithIdentifier:LoadSnapshotCellID owner:self];
    CCFSnapshot *snapshot = [self entityForRow:row];
    cellView.descriptorField.stringValue = snapshot.descriptor;
    cellView.regexField.stringValue = snapshot.regexString;
    cellView.sampleField.stringValue = snapshot.sampleText;
    NSRegularExpressionOptions options = snapshot.regexOptions;
    
    cellView.ignoreCaseMarkerView.state = (options & NSRegularExpressionCaseInsensitive);
    cellView.verboseMarkerView.state = (options & NSRegularExpressionAllowCommentsAndWhitespace);
    cellView.multiLineMarkerView.state = (options & NSRegularExpressionAnchorsMatchLines);
    cellView.singleLineMarkerView.state = (options & NSRegularExpressionDotMatchesLineSeparators);
    
    return cellView;
}

#pragma mark - NSTableViewDelegate

//  deselect and message delegate with a little pacing.
- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [[self tableView] deselectRow:row];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            if( row < _tableContents.count ) {
                [[self delegate] loadSnapshotController:self didSelectSnapshot:[_tableContents objectAtIndex:row]];
            }
        });
    });

    return YES;
}


#pragma mark - Private

- (CCFSnapshot *)entityForRow:(NSInteger)row {
    if( row < _tableContents.count ) {
        return [_tableContents objectAtIndex:row];
    }
    return nil;
}


@end
