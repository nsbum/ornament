//
//  CCFReferenceSheetWindowController.m
//  Ornament
//
//  Created by alanduncan on 12/14/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFReferenceSheetWindowController.h"
#import <WebKit/WebKit.h>

static NSString * const NibName = @"CCFReferenceSheetWindow";

@interface CCFReferenceSheetWindowController ()
@property (nonatomic, weak) IBOutlet WebView *webView;
@end

@implementation CCFReferenceSheetWindowController

- (id)initWithNib {
    self = [super initWithWindowNibName:NibName];
    if( !self ) return nil;
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *path = [bundle pathForResource:@"RegexSyntaxCheatSheet" ofType:@"html"];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    WebFrame *mainFrame = [[self webView] mainFrame];
    [mainFrame loadRequest:request];
}

@end
