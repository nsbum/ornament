/**
 *   @file CCFSubstitutionResultsViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 09:26:47
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@protocol CCFSubstitutionResultsControllerDelegate;
@class  CCFSubstitutedTextView;
@class CCFOrnamentData;

@interface CCFSubstitutionResultsViewController : NSViewController <NSTextViewDelegate>

@property (nonatomic, assign) IBOutlet CCFSubstitutedTextView *textView;
@property (nonatomic, weak) IBOutlet NSSegmentedControl *modeSegmentedControl;
@property (nonatomic, strong) IBOutlet CCFOrnamentData *sharedData;
@property (nonatomic, assign) id delegate;

- (id)initWithNib;

- (NSAttributedString *)sampleText;
- (void)setSampleText:(NSAttributedString *)sampleText;

@end
