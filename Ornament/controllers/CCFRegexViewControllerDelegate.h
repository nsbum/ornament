/**
 *   @file CCFRegexViewControllerDelegate.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-23 21:17:02
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */
@class CCFRegex;

@protocol CCFRegexViewControllerDelegate <NSObject>

- (void)regexController:(id)controller didValidateRegex:(CCFRegex *)regex;
- (void)regexController:(id)controller didInvalidateRegex:(CCFRegex *)regex;


@optional

- (void)regexController:(id)controller didChangeRegex:(CCFRegex *)regex;

@end
