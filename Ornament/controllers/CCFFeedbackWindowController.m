//
//  CCFFeedbackWindowController.m
//  Ornament
//
//  Created by alanduncan on 11/27/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFFeedbackWindowController.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#import <QuartzCore/QuartzCore.h>
#import "CCFFeedbackUploader.h"
// view
#import "CCFFeedbackFormView.h"
#import "CCFFeedbackSuccessView.h"
#import <CCFAppKit/CCFFlexibleGrayView.h>

@interface CCFFeedbackWindowController ()


@end

@implementation CCFFeedbackWindowController {
    BasicBlock _completionBlock;
    IBOutlet CCFFeedbackSuccessView *_successView;
    IBOutlet CCFFeedbackFormView *_formView;
    IBOutlet CCFFlexibleGrayView *_errorView;
}

- (id)initWithNibCompletionBlock:(BasicBlock)block {
    self = [super initWithWindowNibName:@"CCFFeedbackWindow"];
    if( !self ) return nil;
    
    _completionBlock = block;
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    self.appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    self.systemInfoString = [self machineModel];
    self.systemVersionString = [[[NSProcessInfo processInfo] operatingSystemVersionString] stringByReplacingOccurrencesOfString:@"Version " withString:@""];
    
    self.missingEmailAddress = NO;
    self.missingMessage = NO;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self.taskRunning = NO;
    });
    
}

- (NSString *)machineModel
{
    size_t len = 0;
    sysctlbyname("hw.model", NULL, &len, NULL, 0);
    
    if (len)
    {
        char *model = malloc(len*sizeof(char));
        sysctlbyname("hw.model", model, &len, NULL, 0);
        NSString *model_ns = [NSString stringWithUTF8String:model];
        free(model);
        return model_ns;
    }
    
    return @"Mac"; //incase model name can't be read
}

#pragma mark - Interface actions

- (IBAction)okButtonAction:(id)sender {
    //  look for missing data
    if( self.emailString == nil || self.emailString.length == 0 ) {
        self.missingEmailAddress = YES;
    }
    else {
        self.missingEmailAddress = NO;
    }
    if( self.messageString == nil || self.messageString.length == 0 ) {
        self.missingMessage = YES;
    }
    else {
        self.missingMessage = NO;
    }
    if( self.missingMessage || self.missingEmailAddress ) {
        return;
    }
    NSMutableDictionary *mutableParams = [NSMutableDictionary new];
    [mutableParams setObject:self.appVersionString forKey:CCFFeedbackAppVersionKey];
    [mutableParams setObject:self.systemVersionString forKey:CCFFeedbackSysVersionKey];
    [mutableParams setObject:self.systemInfoString forKey:CCFMachineNameKey];
    [mutableParams setObject:self.emailString forKey:CCFFeedbackEmailKey];
    [mutableParams setObject:self.messageString forKey:CCFFeedbackMessageKey];
    [mutableParams setObject:[NSNumber numberWithInteger:self.selectedIssueType] forKey:CCFIssueTypeKey];
    
    self.taskRunning = YES;
    CCFFeedbackUploader *uploader = [[CCFFeedbackUploader alloc] initWithDictionary:mutableParams];
    [uploader downloadWithCompletionBlock:^(CCFAPIStatus status, NSArray *remoteObjects) {
        self.taskRunning = NO;
        NSLog(@"upload results = %ld",status);
        //  prep animations
        CATransition *transition = [CATransition animation];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        NSView *mainView = [[self window] contentView];
        [mainView setAnimations:@{@"subviews" : transition}];
        mainView.wantsLayer = YES;
        if( status == CCFAPIStatusSuccess || status == CCFAPIStatusSuccessNoRows ) {
            if( remoteObjects && remoteObjects.count != 0 ) {
                self.ticketNumberString = remoteObjects[0];
                                
                [[mainView animator] replaceSubview:_formView with:_successView];
                [_successView setFrame:mainView.bounds];
            }
        }
        else {
            [[mainView animator] replaceSubview:_formView with:_errorView];
            [_errorView setFrame:mainView.bounds];
        }
    }];
}

- (IBAction)cancelButtonAction:(id)sender {
    _completionBlock();
}

- (IBAction)successOKButton:(id)sender {
    _completionBlock();
}

@end
