/**
 *   @file CCFSubstitutionSourceTextControllerDelegate.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-27 19:33:28
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@protocol CCFSubstitutionSourceTextControllerDelegate <NSObject>

- (void)substitutionSourceTextControllerDidChangeText:(id)controller;

@end
