/**
 *   @file CCFLoadSnapshotViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 22:19:28
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@protocol CCFLoadSnapshotControllerDelegate;

@interface CCFLoadSnapshotViewController : NSViewController <NSTableViewDataSource, NSTableViewDelegate>

@property (nonatomic, weak) IBOutlet NSTableView *tableView;
@property (nonatomic, assign) id <CCFLoadSnapshotControllerDelegate> delegate;

- (id)initWithNib;

@end
