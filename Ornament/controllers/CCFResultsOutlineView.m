/**
 *   @file CCFResultsOutlineView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 06:20:00
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFResultsOutlineView.h"

@implementation CCFResultsOutlineView {
    BOOL _isReloading;
}

- (void)reloadData {
    //  don't try to reload data on other than main thread
    NSAssert([NSThread isMainThread], @"Should only call on main thread");
    
    if( !_isReloading ) {
        _isReloading = YES;
        [super reloadData];
        _isReloading = NO;
    }
    else {
        //  delay the reloading
        int64_t delayInSeconds = 0.05;
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, mainQueue, ^(void){
            [self reloadData];
        });
    }
    
}

@end
