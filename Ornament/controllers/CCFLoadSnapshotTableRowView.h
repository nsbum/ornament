/**
 *   @file CCFLoadSnapshotTableRowView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-30 22:33:11
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFLoadSnapshotTableRowView : NSTableRowView

@property (nonatomic, strong) id objectValue;
@property (nonatomic, assign) BOOL even;

@end
