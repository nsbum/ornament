/**
 *   @file CCFSubstitutionViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 09:03:39
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSubstitutionSourceTextControllerDelegate.h"
#import "CCFRegexViewControllerDelegate.h"

@protocol CCFSubstitutionViewControllerDelegate;

@interface CCFSubstitutionViewController : NSViewController <CCFSubstitutionSourceTextControllerDelegate, CCFRegexViewControllerDelegate>

@property (nonatomic, weak) IBOutlet NSView *regexContainerView;
@property (nonatomic, weak) IBOutlet NSView *substitutionContainerView;
@property (nonatomic, weak) IBOutlet NSView *sampleTextContainerView;
@property (nonatomic, weak) IBOutlet NSView *substitutionResultsContainerView;

@property (nonatomic, assign) id <CCFSubstitutionViewControllerDelegate> delegate;

- (id)initWithNib;
- (NSString *)regexString;
- (void)setRegexString:(NSString *)string;

- (void)setupResponderChain;

@end
