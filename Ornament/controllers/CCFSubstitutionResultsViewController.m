/**
 *   @file CCFSubstitutionResultsViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 09:26:55
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSubstitutionResultsViewController.h"
#import "CCFSubstitutionResultsControllerDelegate.h"
#import "CCFRegex.h"
#import "CCFSourceTextView.h"
#import "CCFSubstitutedTextView.h"
#import "CCFSourceResultParagraphStyle.h"

enum {
    CCFSampleTextTag,
    CCFSubstitutedTextTag
};

static NSString * const SubstitutionResultsNibName = @"CCFSubstitutionResultsView";

@interface CCFSubstitutionResultsViewController () {
    NSAttributedString *_originalAttributedString;
    NSAttributedString *_substitutedAttributedString;
}

@end

@implementation CCFSubstitutionResultsViewController {
    NSString *_originalText;
}

- (id)initWithNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self = [super initWithNibName:SubstitutionResultsNibName bundle:bundle];
    if( !self ) return nil;
    
    __block id sampleTextChanged = nil;
    sampleTextChanged = [[NSNotificationCenter defaultCenter] addObserverForName:kDidChangeSampleTextNotification
                                                                          object:nil
                                                                           queue:nil
                                                                      usingBlock:^(NSNotification *note) {
                                                                          NSString *text = [note userInfo][kSampleTextStringKey];
                                                                          self.textView.string = text;
                                                                          _originalText = text;
                                                                          [[self view] setNeedsDisplay:YES];
                                                                          /*
                                                                          NSObject *sender = note.object;
                                                                          NSString *className = NSStringFromClass([sender class]);
                                                                          if( [@"CCFSubstitutionSourceTextViewController" isEqualToString:className] ) {
                                                                          }
                                                                           */
                                                                      }];
    
    __block id regexMatched = nil;
    regexMatched = [[NSNotificationCenter defaultCenter] addObserverForName:kDidMatchRegexWithResults
                                                                     object:nil
                                                                      queue:nil
                                                                 usingBlock:^(NSNotification *note) {
                                                                     dispatch_queue_t mainQueue = dispatch_get_main_queue();
                                                                     dispatch_async(mainQueue, ^{
                                                                         [self _performSubstitutionWithNotification:note];
                                                                     });
                                                                 }];
    
    __block id regexInvalidated = nil;
    regexInvalidated = [[NSNotificationCenter defaultCenter] addObserverForName:kDidInvalidateRegexNotification
                                                                         object:nil
                                                                          queue:nil
                                                                     usingBlock:^(NSNotification *note) {
                                                                         NSString *text = [[CCFOrnamentData sharedData] sampleText];
                                                                         if( text ) {
                                                                             self.textView.string = text;
                                                                             _originalText = text;
                                                                             [[self view] setNeedsDisplay:YES];
                                                                         }
                                                                     }];
        
    return self;
}

- (void)_performSubstitutionWithNotification:(NSNotification *)note {
    if( !_originalText ) { return; }
    
    //  get basic attributes and setup our attributed string with them.
    NSDictionary *initialAttributes = [self _attributesForSampleText];
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:_originalText attributes:initialAttributes];
    NSMutableString *mutableText = [mutableAttributedString mutableString];
    NSRange currentRange = NSMakeRange(0, self.textView.string.length);

    // the replacement text
    NSString *substituteText = [[CCFOrnamentData sharedData] substitutionString];
    
    //  only try to replace and highlight if we have substitute text
    if( substituteText && substituteText.length != 0 ) {
        //  get our expression
        CCFRegex *regex = [note userInfo][kRegexKey];
        NSRegularExpression *exp = regex.expression;
        
        for( NSTextCheckingResult *match in [note userInfo][kRegexMatchesKey] ) {
            NSRange range = match.range;
            //[mutableAttributedString addAttribute:CCFSpecialHighlightAttributeName value:[self customAttributeColors] range:range];
            [mutableAttributedString addAttribute:NSUnderlineColorAttributeName value:[NSColor redColor] range:range];
            [mutableAttributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleDouble] range:range];
            [mutableAttributedString addAttribute:NSBackgroundColorAttributeName value:[NSColor colorWithCalibratedRed:0.998 green:0.887 blue:0.625 alpha:1.000] range:range];
        }
        
        [exp replaceMatchesInString:mutableText options:0 range:entireStringRange(mutableText) withTemplate:substituteText];
        [[[self textView] textStorage] replaceCharactersInRange:currentRange withAttributedString:mutableAttributedString];
    }
    else {
        self.textView.string = mutableText;
    }
}

- (void)awakeFromNib {
    [[self textView] setTypingAttributes:[self _attributesForSampleText]];
}

#pragma mark - Public

- (NSAttributedString *)sampleText {
    return _originalAttributedString;
}

- (void)setSampleText:(NSAttributedString *)sampleText {
    _originalAttributedString = sampleText;
    if( sampleText == nil || [sampleText length] == 0 ) {
        return;
    }
    NSInteger mode = [[self modeSegmentedControl] selectedSegment];
    if( mode == CCFSampleTextTag ) {
        //  we are showing the original text
        _originalAttributedString = sampleText;
    }
    else {
        _originalAttributedString = sampleText;
        //  access our shared regex
        CCFRegex *regex = SHARED_REGEX;
        regex.string = [_originalAttributedString string];
        //  do substitution
        NSString *sampleText = [[CCFOrnamentData sharedData] sampleText];
        NSString *substitution = [regex stringWithReplacementsInString:sampleText];
        //  save substituted string to shared data
        [[CCFOrnamentData sharedData] setSubstitutedSampleText:substitution];
        //  display it now
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:substitution];
        //  apply our attributes
        NSFont *fixedWidthFont = [[NSFontManager sharedFontManager] fontWithFamily:@"Menlo" traits:0 weight:5 size:13.0];
        [attributedString addAttribute:NSFontAttributeName value:fixedWidthFont range:entireStringRange(substitution)];
        //  add attributed text to our text view
        [[self textView] setString:@""];
        [[self textView] insertText:attributedString];
    }
}


#pragma mark - Interface actions

- (IBAction)contentSegmentedControl:(id)sender {
    NSInteger selectedSegment = [sender selectedSegment];
    [self _setViewForMode:selectedSegment];
}

- (void)_setViewForMode:(NSInteger)mode {
    if( mode == CCFSampleTextTag ) {
        //  we should display the original text (without substitutions)
        //  get the shared sample text
        NSString *sampleString = [[CCFOrnamentData sharedData] sampleText];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:sampleString];
        //  apply our default attributes
        NSFont *fixedWidthFont = [[NSFontManager sharedFontManager] fontWithFamily:@"Menlo" traits:0 weight:5 size:13.0];
        [attributedString addAttribute:NSFontAttributeName value:fixedWidthFont range:entireStringRange(sampleString)];
        //  add attributed text to our text view
        [[self textView] setString:@""];
        [[self textView] insertText:attributedString];
    }
    else {
        //  we should display the substituted text
        //  get the shared substituted text
        NSString *subString = [[CCFOrnamentData sharedData] substitutedSampleText];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:subString];
        //  apply our attributes
        NSFont *fixedWidthFont = [[NSFontManager sharedFontManager] fontWithFamily:@"Menlo" traits:0 weight:5 size:13.0];
        [attributedString addAttribute:NSFontAttributeName value:fixedWidthFont range:entireStringRange(subString)];
        //  add attributed text to our text view
        [[self textView] setString:@""];
        [[self textView] insertText:attributedString];
    }
}

- (IBAction)openFileAction:(id)sender {
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    if( !openPanel ) {
        NSAlert *noOpenPanelAlert = [NSAlert alertWithMessageText:@"Unable to access file system" defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"Due an entitlements issue, RegexMatch is unable to access the filesystem."];
        [noOpenPanelAlert runModal];
    }
    else {
        openPanel.canChooseDirectories = NO;
        openPanel.canChooseFiles = YES;
        openPanel.canCreateDirectories = NO;
        [openPanel beginWithCompletionHandler:^(NSInteger result) {
            if( result == NSFileHandlingPanelOKButton ) {
                NSURL *fileURL = [openPanel URL];
                NSError *readError = nil;
                NSString *text = [[NSString alloc] initWithContentsOfFile:[fileURL path]
                                                                 encoding:NSUTF8StringEncoding
                                                                    error:&readError];
                if( readError ) {
                    NSAlert *alert = [NSAlert alertWithError:readError];
                    [alert runModal];
                }
                else {
                    _originalAttributedString = [[NSAttributedString alloc] initWithString:text];
                    _substitutedAttributedString = [[NSAttributedString alloc] initWithString:text];
                    [self _setViewForMode:CCFSampleTextTag];
                }
            }
        }];
    }
}

#pragma mark - Accessors

- (NSAttributedString *)substitutedAttributedString {
    if( !_substitutedAttributedString ) {
        _substitutedAttributedString = [_originalAttributedString copy];
    }
    return _substitutedAttributedString;
}

#pragma mark - NSTextViewDelegate

- (void)textDidChange:(NSNotification *)notification {
    //  pass the change notice so that the delegate can
    //  take actions - like re-running the regex against the new text
    [[CCFOrnamentData sharedData] setSampleText:[[self textView] string]];
    [self _postSampleTextChangeNotification];
    [[self delegate] substitutionResultsControllerDidChangeText:self];
}

- (BOOL)textView:(NSTextView *)textView shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString {
    //  unless this change was generated by an insert operation (that we triggered)
    if( ![(CCFSourceTextView *)textView isChanging] ) {
        //  if the user pasted
        if( [(CCFSourceTextView *)textView isPasteOperation] ) {
            int64_t delayInSeconds = 0.005;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self _fixAttributes];
                self.textView.isPasteOperation = NO;
            });
        }
    }
    return YES;
}

#define LOG_ATTRIBUTE_DELETION 0
- (void)_fixAttributes {
    //  flag that we are making a change ourselves so we don't generate delegate
    //  messages unnecessarily
    self.textView.isChanging = YES;
    //  get the raw text from the text view
    NSString *rawText = [[self textView] string];
    if( rawText != nil && rawText.length != 0 ) {

        NSTextStorage *textStorage = [[self textView] textStorage];
        [textStorage beginEditing];
        [textStorage enumerateAttributesInRange:entireStringRange(textStorage) options:0 usingBlock:^(NSDictionary *attrs, NSRange range, BOOL *stop) {
            [attrs enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
#if LOG_ATTRIBUTE_DELETION
                QuietLog(@"NOTE | remove attr %@",key);
#endif
                [textStorage removeAttribute:key range:range];
            }];
        }];
        [textStorage addAttributes:[self _attributesForSampleText] range:entireStringRange(rawText)];
        [textStorage endEditing];
        
        //  since we have changed the attributed sample string, we need to save
        //  to the data singleton
        [[CCFOrnamentData sharedData] setSampleText:rawText];
        
        [self _postSampleTextChangeNotification];
    }
    
    //  unflag that we generate this change ourselves
    self.textView.isChanging = NO;
}

//  Provides dictionary of attributes used to reformat sample text
- (NSDictionary *)_attributesForSampleText {
    NSFont *fixedWidth = [[NSFontManager sharedFontManager] fontWithFamily:@"Menlo" traits:0 weight:5 size:13.0];
    CCFSourceResultParagraphStyle *style = [[CCFSourceResultParagraphStyle alloc] init];
    NSDictionary *attributes = @{   NSFontAttributeName : fixedWidth,
                                    NSForegroundColorAttributeName : [NSColor textColor],
                                    NSParagraphStyleAttributeName : style
    };
    
    return attributes;
}


- (void)_postSampleTextChangeNotification {
    //  fire notification so that our sister view (for matches) can update its text
    NSDictionary *userInfo = @{kSampleTextStringKey : self.textView.string};
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeSampleTextNotification object:self userInfo:userInfo];
}

- (NSDictionary *)customAttributeColors {
    NSColor *bgColor = [NSColor colorWithCalibratedRed:0.998 green:0.887 blue:0.625 alpha:1.000];
    NSColor *lineColor = [NSColor colorWithCalibratedRed:0.635 green:0.308 blue:0.100 alpha:1.000];
    return @{CCFHighlightColorKey : bgColor, CCFLineColorKey : lineColor};
}


@end
