/**
 *   @file CCFCodeCopierHelper.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-26 08:39:54
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

// helper
#import "CCFCodeFormatter.h"

@interface CCFCodeCopierHelper : NSObject

- (id)initWithCodeType:(CCFCodeType)codeType;
- (void)exportCode;

@end
