//
//  CCFSnippetImporter.h
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CCFSnippetImporterCompletionBlock)(NSArray *snippetCategories);

@interface CCFSnippetImporter : NSObject

- (id)initWithCompletionBlock:(CCFSnippetImporterCompletionBlock)block __attribute__((nonnull (1)));
- (void)import;


@end
