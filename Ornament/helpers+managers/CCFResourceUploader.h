/**
 *   @file CCFResourceUploader.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-28 05:01:27
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */
#import "CCFNetworkMediator.h"

typedef void(^CCFDownloadCompletionBlock)(CCFAPIStatus status,NSArray *remoteObjects);

@interface CCFResourceUploader : NSObject

@property (nonatomic, strong) CCFDownloadCompletionBlock handler;
@property (nonatomic, copy) NSMutableData *responseData;
@property (nonatomic, copy) NSMutableURLRequest *request;

- (id)initWithMethod:(NSString *)method url:(NSURL *)url parameters:(NSDictionary *)parameters;
- (void)downloadWithCompletionBlock:(CCFDownloadCompletionBlock)handler;

@end
