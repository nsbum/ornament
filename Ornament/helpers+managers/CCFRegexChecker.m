//
//  CCFRegexChecker.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFRegexChecker.h"

@implementation CCFRegexChecker

- (BOOL)regexIsValid:(NSString *)regexString {
    if( regexString.length == 0 ) return YES;
    NSString *escapedTemplate = [regexString stringByReplacingOccurrencesOfString:@"\\" withString:@"\\\\"];
    NSError *regexError = nil;
    NSRegularExpression *expression = nil;
    expression = [NSRegularExpression regularExpressionWithPattern:escapedTemplate
                                                           options:0
                                                             error:&regexError];
    if( regexError ) {
#if TARGET_OS_IPHONE
<<<<<<< HEAD
        NSLog(@"%s - ERROR with regex: %@,%@",__FUNCTION__,regexError,regexError.userInfo);
=======
        NSString *title = NSLocalizedString(@"Regex error", "regex validator regex creation error alert title");
        NSString *msg = [regexError localizedDescription];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title")
                                              otherButtonTitles:nil];
        [alert show];
>>>>>>> refs/heads/macos-1.1
#else
        NSAlert *alert = [NSAlert alertWithError:regexError];
        [alert runModal];
#endif
    }
    return (regexError == nil);
}

@end
