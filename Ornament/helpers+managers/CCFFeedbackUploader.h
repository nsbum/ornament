//
//  CCFFeedbackUploader.h
//  Ornament
//
//  Created by alanduncan on 11/28/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFResourceUploader.h"

extern NSString * const CCFFeedbackAppVersionKey;
extern NSString * const CCFFeedbackSysVersionKey;
extern NSString * const CCFFeedbackMessageKey;
extern NSString * const CCFFeedbackEmailKey;
extern NSString * const CCFMachineNameKey;
extern NSString * const CCFIssueTypeKey;
extern NSString * const CCFAppIDKey;

@interface CCFFeedbackUploader : CCFResourceUploader

- (id)initWithDictionary:(NSDictionary *)info;

@end
