/**
 *   @file CCFRegexHighlighter.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-23 06:27:49
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFRegexHighlighter : NSObject

@property (nonatomic, strong) NSAttributedString *sourceAttributedString;

- (NSAttributedString *)highlightedString;

@end
