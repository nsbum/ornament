/**
 *   @file CCFMenuManager.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-26 04:18:30
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFMenuManager.h"

//  model
#import "CCFSnapshot.h"
// helper
#import "CCFCodeFormatter.h"
#import "CCFCodeCopierHelper.h"
//  controller
#import "CCFMainViewController.h"
#import "CCFMainViewController+Toolbar.h"
#import "CCFFeedbackWindowController.h"
#import "CCFReferenceSheetWindowController.h"

#define CCF_CODE_MENU_TAG 600
#define CCF_SNAPSHOT_MENU_TAG 700
#define CCF_LOAD_SNAPSHOT_MENU_TAG 703

@implementation CCFMenuManager {
    CCFFeedbackWindowController *_feedbackController;
    CCFReferenceSheetWindowController *_referenceSheetController;
}

- (id)init {
    self = [super init];
    if( !self ) { return nil; }
    
    __block id regexInvalidated = nil;
    regexInvalidated = [[NSNotificationCenter defaultCenter] addObserverForName:kDidInvalidateRegexNotification
                                                                         object:nil
                                                                          queue:nil
                                                                     usingBlock:^(NSNotification *note) {
                                                                         [self setMenuItemState:NO];
                                                                     }];
    
    __block id regexValidated = nil;
    regexValidated = [[NSNotificationCenter defaultCenter] addObserverForName:kDidValidateRegexNotification
                                                                       object:nil
                                                                        queue:nil
                                                                   usingBlock:^(NSNotification *note) {
                                                                       [self setMenuItemState:YES];
                                                                   }];
    
    _mainMenu = [NSApp mainMenu];
    
    return self;
}

- (IBAction)exportMenuAction:(id)sender {
    CCFCodeCopierHelper *helper = [[CCFCodeCopierHelper alloc] initWithCodeType:(CCFCodeType)[sender tag]];
    [helper exportCode];
}

- (IBAction)showHelpAction:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://cocoafactory.com"];
    [[NSWorkspace sharedWorkspace] openURL:url];
}

- (IBAction)showReleaseNotesAction:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://cocoafactory.com/products/regexmatch/macos/support/releasenotes.html"];
    [[NSWorkspace sharedWorkspace] openURL:url];
}

- (IBAction)showRegexReferenceAction:(id)sender {
    /*
    NSURL *url = [NSURL URLWithString:@"http://www.cocoafactory.com/products/regexmatch/macos/support/regexreference.html"];
    [[NSWorkspace sharedWorkspace] openURL:url];
     */
    
    [[self referenceSheetWindowController] showWindow:self];
}

- (IBAction)sendFeedbackAction:(id)sender {
    NSWindow *mainWindow = self.mainViewController.view.window;
    [NSApp beginSheet:self.feedbackController.window
       modalForWindow:mainWindow modalDelegate:self
       didEndSelector:NULL
          contextInfo:NULL];
    //[NSApp beginSheet:sheet modalForWindow:self.view.window modalDelegate:nil didEndSelector:NULL contextInfo:NULL];
}

#pragma mark - Accessors

- (CCFFeedbackWindowController *)feedbackController {
    if( !_feedbackController ) {
        _feedbackController = [[CCFFeedbackWindowController alloc] initWithNibCompletionBlock:^{
            [NSApp endSheet:_feedbackController.window];
            [_feedbackController.window orderOut:self];
            _feedbackController = nil;
        }];
    }
    return _feedbackController;
}

- (CCFReferenceSheetWindowController *)referenceSheetWindowController {
    if( !_referenceSheetController ) {
        _referenceSheetController = [[CCFReferenceSheetWindowController alloc] initWithNib];
    }
    return _referenceSheetController;
}

#pragma mark - Private

- (void)setMenuItemState:(BOOL)state {
    NSMenuItem *codeItem = [[self mainMenu] itemWithTag:CCF_CODE_MENU_TAG];
    NSMenu *codeMenu = codeItem.submenu;
    [[codeMenu itemArray] enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj setEnabled:state];
    }];
}

- (void)handleLoadSnapshot:(NSMenuItem *)menuItem {
    [[self mainViewController] loadSnapshotController:nil didSelectSnapshot:menuItem.representedObject];
}

#pragma mark - NSMenuDelegate

- (void)menuWillOpen:(NSMenu *)menu {
    NSMenu *parentMenu = menu.supermenu;
    __block NSMenuItem *openedItem = nil;
    [[parentMenu itemArray] enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if( [obj tag] == CCF_SNAPSHOT_MENU_TAG ) {
            openedItem = (NSMenuItem *)obj;
            
            //  construct snapshot menu
            NSMenu *snapshotMenu = [[NSMenu alloc] initWithTitle:@"Snapshots"];
            [[[CCFOrnamentData sharedData] snapshots] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                CCFSnapshot *snapshot = (CCFSnapshot *)obj;
                NSMenuItem *menuItem = [[NSMenuItem alloc] initWithTitle:snapshot.descriptor action:@selector(handleLoadSnapshot:) keyEquivalent:@""];
                menuItem.target = self;
                menuItem.representedObject = obj;
                [snapshotMenu addItem:menuItem];
            }];
            snapshotMenu.autoenablesItems = NO;
            
            //  add it to the last item
            NSMenuItem *loadSnapshotItem = [menu itemWithTag:CCF_LOAD_SNAPSHOT_MENU_TAG];
            loadSnapshotItem.submenu = snapshotMenu;
        }
    }];
};

@end
