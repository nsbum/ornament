/**
 *   @file CCFRegexHighlighter.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-23 06:27:49
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFRegexHighlighter.h"

static NSRegularExpression *QuantifierRegex;
static NSRegularExpression *GroupRegex;
static NSRegularExpression *CharClassRegex;
static NSRegularExpression *MetaCharRegex;
static NSRegularExpression *NumberedGroupRegex;

#if TARGET_OS_IPHONE
#define COLOR UIColor
#else
#define COLOR NSColor
#endif

static COLOR *QuantifierColor;
static COLOR *GroupColor;
static COLOR *CharClassColor;
static COLOR *MetaCharColor;
static COLOR *NumberedGroupColor;

DEFINE_NSSTRING(RegexQuantifierKey);
DEFINE_NSSTRING(RegexGroupingKey);
DEFINE_NSSTRING(RegexCharClassKey);
DEFINE_NSSTRING(RegexMetaCharKey);
DEFINE_NSSTRING(RegexNumberedGroupKey);

static NSRange entireString(id string) {
    return NSMakeRange(0, [string length] );
}

static COLOR *colorWithKey(NSString *key) {
    NSData *colorData = [[NSUserDefaults standardUserDefaults] dataForKey:key];
    return (COLOR *)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
}

@implementation CCFRegexHighlighter

+ (void)initialize {
    if( self == [CCFRegexHighlighter class] ) {
        
        //  load the regex templates from a plist because it is *way* easier
        //  than having to escape complicated regex template strings
        NSBundle *bundle = [NSBundle bundleForClass:self];
        NSString *templatePath = [bundle pathForResource:@"CCFRegexMarkupTemplates" ofType:@"plist"];
        NSDictionary *templateInfo = [NSDictionary dictionaryWithContentsOfFile:templatePath];
        
        NSError *error = nil;
        QuantifierRegex = [NSRegularExpression regularExpressionWithPattern:templateInfo[RegexQuantifierKey]
                                                                    options:NSRegularExpressionDotMatchesLineSeparators
                                                                      error:&error];
        GroupRegex = [NSRegularExpression regularExpressionWithPattern:templateInfo[RegexGroupingKey]
                                                               options:NSRegularExpressionDotMatchesLineSeparators
                                                                 error:&error];
        CharClassRegex = [NSRegularExpression regularExpressionWithPattern:templateInfo[RegexCharClassKey]
                                                                   options:NSRegularExpressionDotMatchesLineSeparators error:&error];
        MetaCharRegex = [NSRegularExpression regularExpressionWithPattern:templateInfo[RegexMetaCharKey]
                                                                  options:NSRegularExpressionCaseInsensitive | NSRegularExpressionDotMatchesLineSeparators
                                                                    error:&error];
        NumberedGroupRegex = [NSRegularExpression regularExpressionWithPattern:templateInfo[RegexNumberedGroupKey]
                                                                       options:0
                                                                         error:&error];
        
        QuantifierColor = colorWithKey(kQuantifierColorDataKey);
        GroupColor = colorWithKey(kGroupingColorDataKey);
        CharClassColor = colorWithKey(kCharClassColorDataKey);
        MetaCharColor = colorWithKey(kMetacharacterColorDataKey);
        NumberedGroupColor = colorWithKey(kNumberedGroupColorDataKey);
    }
}

- (id)init {
    self = [super init];
    if( !self ) return nil;
    
    [ @[kCharClassColorDataKey,kGroupingColorDataKey,kQuantifierColorDataKey, kMetacharacterColorDataKey] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [[NSUserDefaults standardUserDefaults] addObserver:self forKeyPath:obj options:NSKeyValueObservingOptionNew context:NULL];
    }];
    
    return self;
}

- (void)dealloc {
    [ @[kCharClassColorDataKey,kGroupingColorDataKey,kQuantifierColorDataKey, kMetacharacterColorDataKey] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:obj];
    }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    NSRange colorDataRange = [keyPath rangeOfString:@"ColorData"];
    if( colorDataRange.location != NSNotFound ) {
        COLOR *color = (COLOR *)[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:keyPath]];
        if( [keyPath isEqualToString:kCharClassColorDataKey] ) {
            CharClassColor = color;
        }
        else if( [keyPath isEqualToString:kGroupingColorDataKey] ) {
            GroupColor = color;
        }
        else if( [keyPath isEqualToString:kQuantifierColorDataKey] ) {
            QuantifierColor = color;
        }
        else if( [keyPath isEqualToString:kMetacharacterColorDataKey] ) {
            MetaCharColor = color;
        }
        else if( [keyPath isEqualToString:kNumberedGroupColorDataKey] ) {
            NumberedGroupColor = color;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeHighlightColorsNotification object:nil];
    }
    
}

- (NSAttributedString *)highlightedString {
    NSString *baseString = self.sourceAttributedString.string;
    
    NSMutableAttributedString *outString = [[NSMutableAttributedString alloc] initWithString:baseString];
    //  apply fixed-width font
#if TARGET_OS_IPHONE
    UIFont *fixedWidthFont = [UIFont fontWithName:@"Menlo-Bold" size:18.0f];
#else
    NSFont *fixedWidthFont = [[NSFontManager sharedFontManager] fontWithFamily:@"Menlo" traits:0 weight:5 size:15.0];
#endif
    [outString addAttribute:NSFontAttributeName value:fixedWidthFont range:entireString(outString)];
    //  Quantifier markup
    NSArray *matches = [QuantifierRegex matchesInString:baseString options:0 range:entireString(baseString)];
    for( NSTextCheckingResult *match in matches ) {
        NSRange range = match.range;
        [outString addAttribute:NSForegroundColorAttributeName value:QuantifierColor range:range];
    }
    
    //  Group markup
    matches = [GroupRegex matchesInString:baseString options:0 range:entireString(baseString)];
    [GroupRegex enumerateMatchesInString:baseString
                                 options:0
                                   range:entireString(baseString)
                              usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                  [outString addAttribute:NSForegroundColorAttributeName value:GroupColor range:result.range];
                              }];
    
    //  Character class markup
    [CharClassRegex enumerateMatchesInString:baseString
                                     options:0
                                       range:entireString(baseString)
                                  usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                      [outString addAttribute:NSForegroundColorAttributeName value:CharClassColor range:result.range];
                                  }];
    
    //  Metacharacter markup
    [MetaCharRegex enumerateMatchesInString:baseString
                                    options:0
                                      range:entireString(baseString)
                                 usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                     [outString addAttribute:NSForegroundColorAttributeName value:MetaCharColor range:result.range];
                                 }];
    
    //  Numbered group markup
    [NumberedGroupRegex enumerateMatchesInString:baseString
                                         options:0
                                           range:entireString(baseString)
                                      usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                          [outString addAttribute:NSForegroundColorAttributeName value:NumberedGroupColor range:result.range];
                                      }];
    return outString;
}

@end


