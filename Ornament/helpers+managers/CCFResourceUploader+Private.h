//
//  CCFResourceDownloader+Private.h
//  AnxietyCoach
//
//  Created by alanduncan on 9/7/12.
//
//

#import "CCFResourceUploader.h"

static NSString * const kResouceUploadMode = @"com.cocoafactory.ResourceUpload";

@interface CCFResourceUploader (Private)

- (void)terminateRunLoop;

@end
