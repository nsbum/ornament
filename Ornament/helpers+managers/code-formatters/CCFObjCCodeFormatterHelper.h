/**
 *   @file CCFObjCCodeFormatterHelper.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-01 23:10:44
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */


@interface CCFObjCCodeFormatterHelper : NSObject

- (NSString *)optionNameForOption:(NSRegularExpressionOptions)option;
- (NSString *)optionStringForOptions:(NSRegularExpressionOptions)options;

@end
