//
//  _CCFCodeFormatterPython.h
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFCodeFormatter.h"

@class CCFRegexOptions;

@interface _CCFCodeFormatterPython : CCFCodeFormatter

- (NSString *)_reOptionsStringWithOptionsObject:(CCFRegexOptions  *)option;

@end
