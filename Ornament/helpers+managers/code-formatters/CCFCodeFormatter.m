/**
 *   @file CCFCodeFormatter.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-25 05:43:56
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFCodeFormatter.h"
#import "_CCFCodeFormatterObjC.h"
#import "_CCFCodeFormatterPerl.h"
#import "_CCFCodeFormatterPython.h"
#import "_CCFCodeFormatterPHP.h"
#import "_CCFCodeFormatterJavaScript.h"
#import "_CCFCodeFormatterRuby.h"
#import "_CCFCodeFormatterJava.h"

void simpleReplace(NSMutableString *codeStr, NSString *pattern, NSString *subText) {
    if( !subText ) {
        subText = @"";
    }
    [codeStr replaceOccurrencesOfString:pattern withString:subText options:0 range:entireStringRange(codeStr)];
}

@implementation CCFCodeFormatter

+ (Class)classForCodeType:(CCFCodeType)type {
    static NSDictionary *cMap = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cMap = @{   [NSNumber numberWithInteger:CCFCodeTypeObjC] : [_CCFCodeFormatterObjC class],
                    [NSNumber numberWithInteger:CCFCodeTypePerl] : [_CCFCodeFormatterPerl class],
                    [NSNumber numberWithInteger:CCFCodeTypePython] : [_CCFCodeFormatterPython class],
                    [NSNumber numberWithInteger:CCFCodeTypePHP] : [_CCFCodeFormatterPHP class],
                    [NSNumber numberWithInteger:CCFCodeTypeJavaScript] : [_CCFCodeFormatterJavaScript class],
                    [NSNumber numberWithInteger:CCFCodeTypeRuby] : [_CCFCodeFormatterRuby class],
                    [NSNumber numberWithInteger:CCFCodeTypeJava] : [_CCFCodeFormatterJava class]};
    });
    return [cMap objectForKey:[NSNumber numberWithInteger:type]];
}

- (id)initWithCodeType:(CCFCodeType)type {
    id instance = nil;
    instance = [[[CCFCodeFormatter classForCodeType:type] alloc] init];
    return instance;
}

- (void)setRegex:(CCFRegex *)regex {
    _regex = regex;
    
}

@end
