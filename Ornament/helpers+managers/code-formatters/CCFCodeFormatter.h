/**
 *   @file CCFCodeFormatter.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-25 05:43:48
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFCodeTypes.h"
#import "CCFRegex.h"

extern void simpleReplace(NSMutableString *codeStr, NSString *pattern, NSString *subText);

@interface CCFCodeFormatter : NSObject

@property (readonly) NSString *codeString;
@property (nonatomic, strong) CCFRegex *regex;

#if TARGET_OS_IPHONE
@property (nonatomic, strong) NSString *sampleText;
#endif

- (id)initWithCodeType:(CCFCodeType)type;

@end
