//
//  _CCFExpressionFormatterPython.m
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "_CCFExpressionFormatterPython.h"
#import "CCFRegex.h"

static NSString *SingleBackslash;
static NSString *DoubleBackslash;

@implementation _CCFExpressionFormatterPython

+ (void)initialize {
    if( self == [_CCFExpressionFormatterPython class] ) {
        char asciiBackslash = 0x5C;
        SingleBackslash = [[NSString alloc] initWithBytes:&asciiBackslash length:1 encoding:NSASCIIStringEncoding];
        DoubleBackslash = [NSString stringWithFormat:@"%@%@",SingleBackslash,SingleBackslash];
    }
}

- (NSString *)expression {
    if( self.regex.isMatching ) {
        //_expression = [[[self regex] string] stringByReplacingOccurrencesOfString:SingleBackslash withString:DoubleBackslash];
        _expression = [[self regex] string];
    }
    return _expression;
}

@end
