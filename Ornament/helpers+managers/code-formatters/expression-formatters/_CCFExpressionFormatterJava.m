/**
 *   @file _CCFExpressionFormatterJava.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-14 12:09:05
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "_CCFExpressionFormatterJava.h"
#import "CCFExpressionFormatterHelper.h"
#import "CCFRegex.h"

static NSString *SingleBackslash;
static NSString *DoubleBackslash;

@implementation _CCFExpressionFormatterJava

+ (void)initialize {
    if( self == [_CCFExpressionFormatterJava class] ) {
        char asciiBackslash = 0x5C;
        SingleBackslash = [[NSString alloc] initWithBytes:&asciiBackslash length:1 encoding:NSASCIIStringEncoding];
        DoubleBackslash = [NSString stringWithFormat:@"%@%@",SingleBackslash,SingleBackslash];
    }
}

- (NSString *)expression {
    _expression = [[[self regex] string] stringByReplacingOccurrencesOfString:SingleBackslash withString:DoubleBackslash];
    return _expression;
}

@end
