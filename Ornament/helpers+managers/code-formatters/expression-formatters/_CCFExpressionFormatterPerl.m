//
//  _CCFExpressionFormatterPerl.m
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "_CCFExpressionFormatterPerl.h"
#import "CCFRegex.h"

static NSString * const RegexStringMarker = @"ß";
static NSString * const RegexSubstitutionMarker = @"∂";

@implementation _CCFExpressionFormatterPerl


- (NSString *)expression {
    return self.regex.string;
    
    /*
    NSMutableString *mutableExpression = [[NSMutableString alloc] initWithString:@""];
    NSString *perlDelimeter = [self perlDelimiter];
    if( self.regex.isMatching ) {
        //  create this: m/ß/g 
        [mutableExpression appendFormat:@"m%@ß%@g", perlDelimeter, perlDelimeter];
    }
    else {
        //  create this: s/ß/∂/g
        [mutableExpression appendFormat:@"s%@ß%@∂%@g", perlDelimeter, perlDelimeter, perlDelimeter];
    }
    //  insert the match string
    [mutableExpression replaceOccurrencesOfString:RegexStringMarker withString:self.regex.string options:0 range:entireStringRange(mutableExpression)];
    //  insert the substitution string
    [mutableExpression replaceOccurrencesOfString:RegexSubstitutionMarker withString:self.regex.string options:0 range:entireStringRange(mutableExpression)];
    
    //  deal with any options that we have
    [mutableExpression appendString:[self _optionsString]];
    
    return [NSString stringWithString:mutableExpression];
     */
}

- (NSString *)perlDelimiter {
    return [[NSUserDefaults standardUserDefaults] objectForKey:kPerlDelimeterKey];
}

- (NSString *)_optionsString {
    NSMutableString *optionsString = [[NSMutableString alloc] initWithString:@""];
    if( self.regex.options.caseInsensitive ) {
        [optionsString appendString:@"i"];
    }
    if( self.regex.options.verbose ) {
        [optionsString appendString:@"x"];
    }
    if( self.regex.options.multiLine ) {
        [optionsString appendString:@"m"];
    }
    if( self.regex.options.singleLine ) {
        [optionsString appendString:@"s"];
    }
    return [NSString stringWithString:optionsString];
}

@end
