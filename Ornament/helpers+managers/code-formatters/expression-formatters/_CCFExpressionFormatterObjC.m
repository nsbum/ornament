//
//  _CCFExpressionFormatterObjC.m
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "_CCFExpressionFormatterObjC.h"
#import "CCFRegex.h"

static NSString *SingleBackslash;
static NSString *DoubleBackslash;

@implementation _CCFExpressionFormatterObjC

+ (void)initialize {
    if( self == [_CCFExpressionFormatterObjC class] ) {
        char asciiBackslash = 0x5C;
        SingleBackslash = [[NSString alloc] initWithBytes:&asciiBackslash length:1 encoding:NSASCIIStringEncoding];
        DoubleBackslash = [NSString stringWithFormat:@"%@%@",SingleBackslash,SingleBackslash];
    }
}

- (NSString *)expression {
    _expression = [[[self regex] string] stringByReplacingOccurrencesOfString:SingleBackslash withString:DoubleBackslash];
    return _expression;
}

@end
