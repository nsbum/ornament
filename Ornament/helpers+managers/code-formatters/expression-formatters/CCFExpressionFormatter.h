/**
 *   @file CCFExpressionFormatter.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-25 06:00:14
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFCodeTypes.h"

@class CCFRegex;

@interface CCFExpressionFormatter : NSObject {
    NSString *_expression;
}

@property (nonatomic, strong) CCFRegex *regex;
@property (readonly) NSString *expression;

- (id)initWithCodeType:(CCFCodeType)type;

@end
