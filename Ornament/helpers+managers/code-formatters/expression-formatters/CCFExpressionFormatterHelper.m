/**
 *   @file CCFExpressionFormatterHelper.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 05:19:26
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFExpressionFormatterHelper.h"

static NSString *SingleBackslash;
static NSString *DoubleBackslash;

@implementation CCFExpressionFormatterHelper

+ (void)initialize {
    if( self == [CCFExpressionFormatterHelper class] ) {
        char asciiBackslash = 0x5C;
        SingleBackslash = [[NSString alloc] initWithBytes:&asciiBackslash length:1 encoding:NSASCIIStringEncoding];
        DoubleBackslash = [NSString stringWithFormat:@"%@%@",SingleBackslash,SingleBackslash];
    }
}


- (NSString *)optionsStringForOptions:(NSRegularExpressionOptions)options {
    NSMutableString *optionsString = [[NSMutableString alloc] initWithString:@""];
    if( options & NSRegularExpressionCaseInsensitive ) {
        [optionsString appendString:@"i"];
    }
    if( options & NSRegularExpressionAllowCommentsAndWhitespace ) {
        [optionsString appendString:@"x"];
    }
    if( options & NSRegularExpressionAnchorsMatchLines ) {
        [optionsString appendString:@"m"];
    }
    if( options & NSRegularExpressionDotMatchesLineSeparators ) {
        [optionsString appendString:@"s"];
    }
    return [NSString stringWithString:optionsString];
}

- (NSString *)backslashedString:(NSString *)string {
    return [string stringByReplacingOccurrencesOfString:SingleBackslash withString:DoubleBackslash];
}

@end
