//
//  _CCFExpressionFormatterPHP.m
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "_CCFExpressionFormatterPHP.h"
#import "CCFRegex.h"

static NSString * const RegexStringMarker = @"ß";

@implementation _CCFExpressionFormatterPHP

- (NSString *)expression {
    NSMutableString *mutableExpression = [[NSMutableString alloc] initWithString:@""];
    if( self.regex.isMatching ) {
        [mutableExpression appendFormat:@"%@",self.regex.string];
    }
    return [NSString stringWithString:mutableExpression];
}

@end
