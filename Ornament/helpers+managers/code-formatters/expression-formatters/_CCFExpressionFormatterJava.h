/**
 *   @file _CCFExpressionFormatterJava.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-14 12:09:14
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */
#import "CCFExpressionFormatter.h"

@interface _CCFExpressionFormatterJava : CCFExpressionFormatter

@end
