//
//  _CCFExpressionFormatterRuby.m
//  Ornament
//
//  Created by alanduncan on 10/26/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "_CCFExpressionFormatterRuby.h"
#import "CCFRegex.h"
#import "CCFExpressionFormatterHelper.h"

@implementation _CCFExpressionFormatterRuby

- (NSString *)expression {
    return self.regex.string;
}

@end
