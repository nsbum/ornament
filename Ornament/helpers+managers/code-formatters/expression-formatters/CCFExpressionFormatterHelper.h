/**
 *   @file CCFExpressionFormatterHelper.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-26 05:19:17
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFExpressionFormatterHelper : NSObject

- (NSString *)optionsStringForOptions:(NSRegularExpressionOptions)options;
- (NSString *)backslashedString:(NSString *)string;

@end
