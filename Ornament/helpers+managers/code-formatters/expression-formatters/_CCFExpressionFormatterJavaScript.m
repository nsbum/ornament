//
//  _CCFExpressionFormatterJavaScript.m
//  Ornament
//
//  Created by alanduncan on 10/26/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "_CCFExpressionFormatterJavaScript.h"
#import "CCFRegex.h"
#import "CCFExpressionFormatterHelper.h"

@implementation _CCFExpressionFormatterJavaScript

- (NSString *)expression {
    return self.regex.string;
}

@end
