//
//  _CCFExpressionFormatterPerl.h
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFExpressionFormatter.h"

@interface _CCFExpressionFormatterPerl : CCFExpressionFormatter

- (NSString *)perlDelimiter;

@end
