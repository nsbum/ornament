/**
 *   @file CCFExpressionFormatter.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-25 06:00:22
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFExpressionFormatter.h"
#import "_CCFExpressionFormatterObjC.h"
#import "_CCFExpressionFormatterPerl.h"
#import "_CCFExpressionFormatterPython.h"
#import "_CCFExpressionFormatterPHP.h"
#import "_CCFExpressionFormatterJavaScript.h"
#import "_CCFExpressionFormatterRuby.h"
#import "_CCFExpressionFormatterJava.h"

static id LastAllocatedInstance;

@implementation CCFExpressionFormatter

@synthesize expression = _expression;

+ (Class)_classForCodeType:(CCFCodeType)type {
    static NSDictionary *cMap = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cMap = @{   [NSNumber numberWithInteger:CCFCodeTypeObjC] : [_CCFExpressionFormatterObjC class],
                    [NSNumber numberWithInteger:CCFCodeTypePerl] : [_CCFExpressionFormatterPerl class],
                    [NSNumber numberWithInteger:CCFCodeTypePython] : [_CCFExpressionFormatterPython class],
                    [NSNumber numberWithInteger:CCFCodeTypePHP] : [_CCFExpressionFormatterPHP class],
                    [NSNumber numberWithInteger:CCFCodeTypeJavaScript] : [_CCFExpressionFormatterJavaScript class],
                    [NSNumber numberWithInteger:CCFCodeTypeRuby] : [_CCFExpressionFormatterRuby class],
                    [NSNumber numberWithInteger:CCFCodeTypeJava] : [_CCFExpressionFormatterJava class] };
    });
    return [cMap objectForKey:[NSNumber numberWithInteger:type]];
}

+ (NSSet *)keyPathsForValuesAffectingExpression {
    return [NSSet setWithObjects:@"regex", nil];
}

- (id)initWithCodeType:(CCFCodeType)type {
    id instance = [[[CCFExpressionFormatter _classForCodeType:type] alloc] init];
    
    //  we want to invalidate _expression when the regex changes
    [instance addObserver:instance forKeyPath:@"regex" options:NSKeyValueObservingOptionNew context:NULL];
    
    @synchronized(LastAllocatedInstance) {
        LastAllocatedInstance = instance;
    }
    
    
    return instance;
}


- (void)dealloc {
    [LastAllocatedInstance removeObserver:LastAllocatedInstance forKeyPath:@"regex"];
    LastAllocatedInstance = nil;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if( [keyPath isEqualToString:@"regex"] ) {
        _expression = nil;
        //  ?force the expression property to update
        [[self expression] self];
    }
}

#pragma mark - Accessors

//  concrete subclasses should implement this method
- (NSString *)expression {
    return nil;
}

@end
