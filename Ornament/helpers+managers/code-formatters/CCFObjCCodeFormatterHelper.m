/**
 *   @file CCFObjCCodeFormatterHelper.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-01 23:10:51
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFObjCCodeFormatterHelper.h"

@implementation CCFObjCCodeFormatterHelper

- (NSString *)optionNameForOption:(NSRegularExpressionOptions)option {
    static NSDictionary *cMap = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cMap = @{   [NSNumber numberWithInteger:NSRegularExpressionAllowCommentsAndWhitespace] : @"NSRegularExpressionAllowCommentsAndWhitespace",
                    [NSNumber numberWithInteger:NSRegularExpressionAnchorsMatchLines] : @"NSRegularExpressionAnchorsMatchLines",
                    [NSNumber numberWithInteger:NSRegularExpressionCaseInsensitive] : @"NSRegularExpressionCaseInsensitive",
                    [NSNumber numberWithInteger:NSRegularExpressionDotMatchesLineSeparators] : @"NSRegularExpressionDotMatchesLineSeparators"
        };
    });
    return [cMap objectForKey:[NSNumber numberWithInteger:option]];
}

- (NSString *)optionStringForOptions:(NSRegularExpressionOptions)options {
    NSMutableArray *optionsArray = [[NSMutableArray alloc] init];
    if( options & NSRegularExpressionAllowCommentsAndWhitespace ) {
        [optionsArray addObject:[self optionNameForOption:NSRegularExpressionAllowCommentsAndWhitespace]];
    }
    if( options & NSRegularExpressionAnchorsMatchLines ) {
        [optionsArray addObject:[self optionNameForOption:NSRegularExpressionAnchorsMatchLines]];
    }
    if( options & NSRegularExpressionCaseInsensitive ) {
        [optionsArray addObject:[self optionNameForOption:NSRegularExpressionCaseInsensitive]];
    }
    if( options & NSRegularExpressionDotMatchesLineSeparators ) {
        [optionsArray addObject:[self optionNameForOption:NSRegularExpressionDotMatchesLineSeparators]];
    }
    if( optionsArray.count == 0 ) {
        return @"0";
    }
    return [optionsArray componentsJoinedByString:@" | "];
}

@end
