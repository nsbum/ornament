//
//  _CCFCodeFormatterPython.m
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "_CCFCodeFormatterPython.h"
#import "CCFRegex.h"
#import "CCFRegexOptions.h"
#import "CCFExpressionFormatter.h"
#import "CCFUserDefaultConstants.h"

static NSString * const PythonMatchNoCompileFileName = @"template-python-match-ncompile";
static NSString * const PythonMatchCompileFileName = @"template-python-match-compile";
static NSString * const PythonSubstituteCompileFileName = @"template-python-substitue-compile";
static NSString * const PythonSubstituteNoCompileFileName = @"template-python-substitute-ncompile";

@implementation _CCFCodeFormatterPython

- (NSString *)codeString {
    NSString *templateText = [self templateText];
    if( !templateText ) { return nil; }
    NSMutableString *codeStr = [[NSMutableString alloc] initWithString:templateText];
    
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePython];
    [formatter setRegex:self.regex];
    simpleReplace(codeStr, @"##pattern##", formatter.expression);
    
    //  use our sample text or not?
    BOOL useSample = [[NSUserDefaults standardUserDefaults] boolForKey:kUseSampleTextInCode];
    NSString *sampleText = nil;
#if TARGET_OS_IPHONE
    NSString *escapedSampleText = [[self sampleText] stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
#else
    NSString *escapedSampleText = [[CCFOrnamentData sharedData] sampleTextWithNewlinesEscaped];
#endif
    sampleText = (useSample)?[NSString stringWithFormat:@"sampleText = '%@'",escapedSampleText]:@"";
    simpleReplace(codeStr, @"##sampletext##", sampleText);
    
#if TARGET_OS_IPHONE
    NSString *subText = self.regex.replacementString;
#else
    NSString *subText = [[CCFOrnamentData sharedData] substitutionString];
#endif
    simpleReplace(codeStr, @"##subtext##", subText);
    
    NSString *optionsString = [self _reOptionsStringWithOptionsObject:self.regex.options];
    if( optionsString.length != 0 ) {
        NSString *formattedOptionString = [NSString stringWithFormat:@", %@",optionsString];
        simpleReplace(codeStr, @"##options##", formattedOptionString);
    }
    else {
        simpleReplace(codeStr, @"##options##", @"");
    }

    return [NSString stringWithString:codeStr];
}

- (NSString *)templateText {
    NSString *fileName = [self templateFileName];
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:fileName ofType:@"txt"];
    NSError *loadError = nil;
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&loadError];
    if( loadError ) {
#if TARGET_OS_IPHONE
<<<<<<< HEAD
        NSLog(@"%s - ERROR loading template: %@,%@",__FUNCTION__,loadError,loadError.userInfo);
=======
        NSLog(@"%s - ERROR loading Python template: %@ : %@",__FUNCTION__,loadError,[loadError userInfo]);
        NSString *title = NSLocalizedString(@"Template error", "template loader error alert title");
        NSString *msg = [loadError localizedDescription];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title")
                                              otherButtonTitles:nil];
        [alert show];
>>>>>>> refs/heads/macos-1.1
#else
        NSAlert *alert = [NSAlert alertWithError:loadError];
        [alert runModal];
#endif
        return nil;
    }
    return text;
}

- (NSString *)templateFileName {
    NSString *templateFileName;
    BOOL shouldCompile = [[NSUserDefaults standardUserDefaults] boolForKey:kPythonCompileRegex];
    BOOL isMatching = (self.regex.replacementString == nil) || (self.regex.replacementString.length == 0);
    if( !shouldCompile ) {
        if( isMatching ) {
            templateFileName = PythonMatchNoCompileFileName;
        }
        else {
            templateFileName = PythonSubstituteNoCompileFileName;
        }
    }
    else {
        if( isMatching ) {
            templateFileName = PythonMatchCompileFileName;
        }
        else {
            templateFileName = PythonSubstituteCompileFileName;
        }
    }
    return templateFileName;
}

- (NSString *)_reOptionsStringWithOptionsObject:(CCFRegexOptions *)option {
    static NSDictionary *optMap = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        optMap = @{ [NSNumber numberWithInteger:NSRegularExpressionCaseInsensitive] : @"re.I",
                    [NSNumber numberWithInteger:NSRegularExpressionAllowCommentsAndWhitespace] : @"re.X",
                    [NSNumber numberWithInteger:NSRegularExpressionDotMatchesLineSeparators] : @"re.S",
                    [NSNumber numberWithInteger:NSRegularExpressionAnchorsMatchLines] : @"re.M"
        };
    });
    NSMutableString *optionsString = [[NSMutableString alloc] initWithString:@""];
    NSRegularExpressionOptions requestedOptions = [option regexOptions];
    if( requestedOptions & NSRegularExpressionCaseInsensitive ) {
        [optionsString appendFormat:@"%@ | ",[optMap objectForKey:[NSNumber numberWithInteger:NSRegularExpressionCaseInsensitive]]];
    }
    if( requestedOptions & NSRegularExpressionAllowCommentsAndWhitespace ) {
        [optionsString appendFormat:@"%@ | ",[optMap objectForKey:[NSNumber numberWithInteger:NSRegularExpressionAllowCommentsAndWhitespace]]];
    }
    if( requestedOptions & NSRegularExpressionDotMatchesLineSeparators ) {
        [optionsString appendFormat:@"%@ | ",[optMap objectForKey:[NSNumber numberWithInteger:NSRegularExpressionDotMatchesLineSeparators]]];
    }
    if( requestedOptions & NSRegularExpressionAnchorsMatchLines ) {
        [optionsString appendFormat:@"%@", [optMap objectForKey:[NSNumber numberWithInteger:NSRegularExpressionAnchorsMatchLines]]];
    }
    
    //  trim comma + space at end if needed.
    if( optionsString.length > 3 ) {
        NSRange lastThreeCharsRange = NSMakeRange(optionsString.length - 3, 3);
        NSString *lastThreeString = [optionsString substringWithRange:lastThreeCharsRange];
        if( [lastThreeString isEqualToString:@" | "] ) {
            [optionsString deleteCharactersInRange:lastThreeCharsRange];
        }
    }
    
    
    return [NSString stringWithString:optionsString];
}
@end
