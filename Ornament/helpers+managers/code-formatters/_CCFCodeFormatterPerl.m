//
//  _CCFCodeFormatterPerl.m
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "_CCFCodeFormatterPerl.h"
#import "CCFExpressionFormatter.h"
#import "CCFUserDefaultConstants.h"

static NSString * const PerlMatchFileName = @"template-perl-match";
static NSString * const PerlSubstituteFileName = @"template-perl-substitute";

@implementation _CCFCodeFormatterPerl

- (NSString *)codeString {
    NSString *templateText = [self templateText];
    if( !templateText ) return nil;
    NSMutableString *codeStr = [[NSMutableString alloc] initWithString:templateText];
    
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePerl];
    formatter.regex = self.regex;
    simpleReplace(codeStr, @"##pattern##", formatter.expression);
    
    //  use our sample text or not?
    BOOL useSample = [[NSUserDefaults standardUserDefaults] boolForKey:kUseSampleTextInCode];
    NSString *sampleText = nil;
#if TARGET_OS_IPHONE
    NSString *escapedSampleText = [[self sampleText] stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
#else
    NSString *escapedSampleText = [[CCFOrnamentData sharedData] sampleTextWithNewlinesEscaped];
#endif
    sampleText = (useSample)?[NSString stringWithFormat:@"my $sample_text = \"%@\";",escapedSampleText]:@"";
    simpleReplace(codeStr, @"##sampletext##", sampleText);
    
    simpleReplace(codeStr, @"##options##", [self optionsString] );
    
#if TARGET_OS_IPHONE
    NSString *subText = self.regex.replacementString;
#else
    NSString *subText = [[CCFOrnamentData sharedData] substitutionString];
#endif
    simpleReplace(codeStr, @"##subtext##", subText);
    
    return [NSString stringWithString:codeStr];
    
    /*
    NSMutableString *codeStr = [[NSMutableString alloc] initWithString:@"my $sampleString = \"The quick brown fox\";\n"];
    [codeStr appendFormat:@"$sampleString =~ %@;",expression];
    
    
     */
}

- (NSString *)templateFileName {
    if( (self.regex.replacementString == nil) || (self.regex.replacementString.length == 0) ) {
        return PerlMatchFileName;
    }
    else {
        return PerlSubstituteFileName;
    }
}

- (NSString *)templateText {
    NSString *fileName = [self templateFileName];
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:fileName ofType:@"txt"];
    NSError *loadError = nil;
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&loadError];
    if( loadError ) {
#if TARGET_OS_IPHONE
<<<<<<< HEAD
        NSLog(@"%s - ERROR loading template: %@,%@",__FUNCTION__,loadError,loadError.userInfo);
=======
        NSLog(@"%s - ERROR loading Perl template: %@ : %@",__FUNCTION__,loadError,[loadError userInfo]);
        NSString *title = NSLocalizedString(@"Template error", "template loader error alert title");
        NSString *msg = [loadError localizedDescription];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title")
                                              otherButtonTitles:nil];
        [alert show];
>>>>>>> refs/heads/macos-1.1
#else
        NSAlert *alert = [NSAlert alertWithError:loadError];
        [alert runModal];
#endif
        return nil;
    }
    return text;
}

- (NSString *)optionsString {
    NSMutableString *optionsString = [[NSMutableString alloc] initWithString:@""];
    if( self.regex.options.caseInsensitive ) {
        [optionsString appendString:@"i"];
    }
    if( self.regex.options.verbose ) {
        [optionsString appendString:@"x"];
    }
    if( self.regex.options.multiLine ) {
        [optionsString appendString:@"m"];
    }
    if( self.regex.options.singleLine ) {
        [optionsString appendString:@"s"];
    }
    return [NSString stringWithString:optionsString];
}

@end
