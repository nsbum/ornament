/**
 *   @file CCFCodeType.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-25 05:44:55
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

typedef enum : NSInteger {
    CCFCodeTypeObjC,
    CCFCodeTypePerl,
    CCFCodeTypePHP,
    CCFCodeTypePython,
    CCFCodeTypeRuby,
    CCFCodeTypeJava,
    CCFCodeTypeJavaScript
} CCFCodeType;