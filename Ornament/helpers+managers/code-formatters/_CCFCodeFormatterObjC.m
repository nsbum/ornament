//
//  _CCFCodeFormatterObjC.m
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "_CCFCodeFormatterObjC.h"
#import "CCFObjCCodeFormatterHelper.h"
#import "CCFExpressionFormatter.h"
#import "CCFUserDefaultConstants.h"

static NSString * const ObjCMatchFileName = @"template-objc-match";
static NSString * const ObjCSubstituteFileName = @"template-objc-substitute";

@implementation _CCFCodeFormatterObjC

- (NSString *)codeString {
    NSString *templateText = [self templateText];
    if( !templateText ) return nil;
    
    NSMutableString *codeStr = [[NSMutableString alloc] initWithString:templateText];
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypeObjC];
    [formatter setRegex:self.regex];
    simpleReplace(codeStr, @"##pattern##", formatter.expression);
    
    //  use our sample text or not?
    BOOL useSample = [[NSUserDefaults standardUserDefaults] boolForKey:kUseSampleTextInCode];
#if TARGET_OS_IPHONE
    NSString *escapedSampleText = [[self sampleText] stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
#else
    NSString *escapedSampleText = [[CCFOrnamentData sharedData] sampleTextWithNewlinesEscaped];
#endif
    NSString *sampleText = nil;
    sampleText = (useSample)?[NSString stringWithFormat:@"NSString *sampleText = @\"%@\";",escapedSampleText]:@"";
    simpleReplace(codeStr, @"##sampletext##", sampleText);
    
    //  add options as needed
    CCFObjCCodeFormatterHelper *helper = [[CCFObjCCodeFormatterHelper alloc] init];
    NSRegularExpressionOptions opts = [[[self regex] options] regexOptions];
    NSString *optionsString = [helper optionStringForOptions:opts];
    simpleReplace(codeStr, @"##options##", optionsString);
    
#if TARGET_OS_IPHONE
    NSString *subText = self.regex.replacementString;
#else
    NSString *subText = [[CCFOrnamentData sharedData] substitutionString];
#endif
    simpleReplace(codeStr, @"##substitution##", subText);
    
    return [NSString stringWithString:codeStr];
    
}

- (NSString *)templateFileName {
    if( (self.regex.replacementString == nil) || (self.regex.replacementString.length == 0) ) {
        return ObjCMatchFileName;
    }
    return ObjCSubstituteFileName;
}

- (NSString *)templateText {
    NSString *fileName = [self templateFileName];
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:fileName ofType:@"txt"];
    NSError *loadError = nil;
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&loadError];
    if( loadError ) {
#if TARGET_OS_IPHONE
<<<<<<< HEAD
        NSLog(@"%s - ERROR loading template: %@,%@",__FUNCTION__,loadError,loadError.userInfo);
=======
        NSLog(@"%s - ERROR loading ObjC template: %@ : %@",__FUNCTION__,loadError,[loadError userInfo]);
        NSString *title = NSLocalizedString(@"Template error", "template loader error alert title");
        NSString *msg = [loadError localizedDescription];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title")
                                              otherButtonTitles:nil];
        [alert show];
>>>>>>> refs/heads/macos-1.1
#else
        NSAlert *alert = [NSAlert alertWithError:loadError];
        [alert runModal];
#endif
        return nil;
    }
    return text;
}

@end
