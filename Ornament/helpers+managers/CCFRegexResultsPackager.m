/**
 *   @file CCFRegexResultsPackager.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 05:10:14
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFRegexResultsPackager.h"
#import "CCFMatch.h"

@implementation CCFRegexResultsPackager

@synthesize packagedResults = _packagedResults;

- (id)initWithTextCheckingResults:(NSArray *)results source:(NSString *)source {
    self = [super init];
    if( !self ) return nil;
    
    NSMutableArray *mutablePackagedResults = [[NSMutableArray alloc] init];
    [results enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CCFMatch *match = [[CCFMatch alloc] initWithResult:obj forSourceString:source index:idx];
        [mutablePackagedResults addObject:match];
    }];
    
    _packagedResults = [NSArray arrayWithArray:mutablePackagedResults];
    
    return self;
    
}

#pragma mark - Accessors

- (NSArray *)packagedResults {
    return _packagedResults;
}

@end
