/**
 *   @file _CCFCodeFormatterJava.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-14 12:14:39
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFCodeFormatter.h"

@interface _CCFCodeFormatterJava : CCFCodeFormatter

@end
