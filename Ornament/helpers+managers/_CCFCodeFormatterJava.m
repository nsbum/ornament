/**
 *   @file _CCFCodeFormatterJava.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-14 12:14:29
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "_CCFCodeFormatterJava.h"
#import "CCFRegex.h"
//  helper
#import "CCFExpressionFormatter.h"
#import "CCFUserDefaultConstants.h"

static NSString * const JavaMatchFileName = @"template-java-match";
static NSString * const JavaSubstituteFileName = @"template-java-substitute";

@implementation _CCFCodeFormatterJava

- (NSString *)codeString {
    NSString *templateText = [self templateText];
    if( !templateText ) return nil;
    
    NSMutableString *codeStr = [[NSMutableString alloc] initWithString:templateText];
    CCFExpressionFormatter *javaExpressionFormatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypeJava];
    javaExpressionFormatter.regex = self.regex;
    simpleReplace(codeStr, @"##pattern##", javaExpressionFormatter.expression);
    
    //  use our sample text or not?
    BOOL useSample = [[NSUserDefaults standardUserDefaults] boolForKey:kUseSampleTextInCode];
    NSString *sampleText = nil;
#if TARGET_OS_IPHONE
    NSString *escapedSampleText = [[self sampleText] stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
#else
    NSString *escapedSampleText = [[CCFOrnamentData sharedData] sampleTextWithNewlinesEscaped];
#endif
    sampleText = (useSample)?[NSString stringWithFormat:@" = \"%@\";",escapedSampleText]:@";";
    simpleReplace(codeStr, @"##sampletext##", sampleText);
    
    //  out-of-the-box, Java doesn't support options
    //  we may support them latter, but for now we'll ignore.
    
#if TARGET_OS_IPHONE
    NSString *subText = self.regex.replacementString;
#else
    NSString *subText = [[CCFOrnamentData sharedData] substitutionString];
#endif
    simpleReplace(codeStr, @"##subtext##", subText);
    
    return [NSString stringWithString:codeStr];
}

- (NSString *)templateFileName {
    if( (self.regex.replacementString == nil) || (self.regex.replacementString.length == 0) ) {
        return JavaMatchFileName;
    }
    return JavaSubstituteFileName;
}

- (NSString *)templateText {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *path = [bundle pathForResource:[self templateFileName] ofType:@"txt"];
    NSError *loadError = nil;
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&loadError];
    if( loadError ) {
#if TARGET_OS_IPHONE
<<<<<<< HEAD
        NSLog(@"%s - ERROR loading template: %@,%@",__FUNCTION__,loadError,loadError.userInfo);
=======
        NSLog(@"%s - ERROR loading Java template: %@ : %@",__FUNCTION__,loadError,[loadError userInfo]);
        NSString *title = NSLocalizedString(@"Template error", "template loader error alert title");
        NSString *msg = [loadError localizedDescription];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title")
                                              otherButtonTitles:nil];
        [alert show];
>>>>>>> refs/heads/macos-1.1
#else
        NSAlert *alert = [NSAlert alertWithError:loadError];
        [alert runModal];
#endif
        return nil;
    }
    return text;
}

@end
