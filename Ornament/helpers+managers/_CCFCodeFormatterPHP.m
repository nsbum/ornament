//
//  _CCFCodeFormatterPHP.m
//  Ornament
//
//  Created by alanduncan on 10/26/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "_CCFCodeFormatterPHP.h"
#import "CCFExpressionFormatter.h"
#import "CCFExpressionFormatterHelper.h"
#import "CCFRegex.h"
#import "CCFUserDefaultConstants.h"

static NSString * const PHPTMatchFileName = @"template-php-match";
static NSString * const PHPSubstituteFileName = @"template-php-substitute";

@implementation _CCFCodeFormatterPHP

- (NSString *)codeString {
    NSString *templateText = [self templateText];
    if( !templateText ) { return nil; }
    NSMutableString *codeStr = [[NSMutableString alloc] initWithString:templateText];
    
    CCFExpressionFormatter *formatter = [[CCFExpressionFormatter alloc] initWithCodeType:CCFCodeTypePHP];
    [formatter setRegex:self.regex];
    simpleReplace(codeStr, @"##pattern##", formatter.expression);
    
    //  use our sample text or not?
    BOOL useSample = [[NSUserDefaults standardUserDefaults] boolForKey:kUseSampleTextInCode];
    NSString *sampleText = nil;
#if TARGET_OS_IPHONE
    NSString *escapedSampleText = [[self sampleText] stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
#else
    NSString *escapedSampleText = [[CCFOrnamentData sharedData] sampleTextWithNewlinesEscaped];
#endif
    sampleText = (useSample)?[NSString stringWithFormat:@"$sample_text = @\"%@\";",escapedSampleText]:@"";
    simpleReplace(codeStr, @"##sampletext##", sampleText);
    
    //  add options if necessary
    CCFExpressionFormatterHelper *helper = [[CCFExpressionFormatterHelper alloc] init];
    NSRegularExpressionOptions opts = [[[self regex] options] regexOptions];
    NSString *optionsString = [helper optionsStringForOptions:opts];
    simpleReplace(codeStr, @"##options##", optionsString);
    
    //  add substitute text
#if TARGET_OS_IPHONE
    NSString *subText = self.regex.replacementString;
#else
    NSString *subText = [[CCFOrnamentData sharedData] substitutionString];
#endif
    simpleReplace(codeStr, @"##subtext##", subText);
    
    return [NSString stringWithString:codeStr];
}

- (NSString *)templateFileName {
#if TARGET_OS_IPHONE
    return ((self.regex.replacementString == nil) || (self.regex.replacementString.length == 0)) ? PHPTMatchFileName : PHPSubstituteFileName;
#else
    return ( [[CCFOrnamentData sharedData] isMatching] ) ? PHPTMatchFileName : PHPSubstituteFileName;
#endif

    
}

- (NSString *)templateText {
    NSString *fileName = [self templateFileName];
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:fileName ofType:@"txt"];
    NSError *loadError = nil;
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&loadError];
    if( loadError ) {
#if TARGET_OS_IPHONE
<<<<<<< HEAD
        NSLog(@"%s - ERROR loading template: %@,%@",__FUNCTION__,loadError,loadError.userInfo);
=======
        NSLog(@"%s - ERROR loading PHP template: %@ : %@",__FUNCTION__,loadError,[loadError userInfo]);
        NSString *title = NSLocalizedString(@"Template error", "template loader error alert title");
        NSString *msg = [loadError localizedDescription];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title")
                                              otherButtonTitles:nil];
        [alert show];
>>>>>>> refs/heads/macos-1.1
#else
        NSAlert *alert = [NSAlert alertWithError:loadError];
        [alert runModal];
#endif
        return nil;
    }
    return text;
}

@end
