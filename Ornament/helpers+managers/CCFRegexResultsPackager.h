/**
 *   @file CCFRegexResultsPackager.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-10-24 05:10:01
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFRegexResultsPackager : NSObject

@property (nonatomic, readonly) NSArray *packagedResults;

- (id)initWithTextCheckingResults:(NSArray *)results source:(NSString *)source;

@end
