//
//  CCFRegexValidColorTransformer.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFRegexValidColorTransformer.h"

@implementation CCFRegexValidColorTransformer

+ (Class)transformedValueClass {
    return [NSColor class];
}

+ (BOOL)allowsReverseTransformation {
    return NO;
}

- (id)transformedValue:(id)value {
    if( [value boolValue] ) {
        return [NSColor textColor];
    }
    return [NSColor colorWithCalibratedRed:0.519 green:0.000 blue:0.038 alpha:1.000];
}

@end
