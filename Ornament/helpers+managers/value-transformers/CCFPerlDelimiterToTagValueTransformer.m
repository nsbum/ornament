//
//  CCFPerlDelimiterToTagValueTransformer.m
//  Ornament
//
//  Created by alanduncan on 10/25/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFPerlDelimiterToTagValueTransformer.h"

@implementation CCFPerlDelimiterToTagValueTransformer


+ (Class)transformedValueClass {
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation {
    return YES;
}

- (id)transformedValue:(id)value {
    
    
    static NSArray *rMap = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        rMap = @[@"//",@"|",@"!",@"#",@"%",@"+"];
    });
    return [rMap objectAtIndex:[value integerValue]];
}

- (id)reverseTransformedValue:(id)value {
    static NSDictionary *cMap = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cMap = @{@"//" : @0, @"|" : @1, @"!" : @2, @"#" : @3, @"%" : @4, @"+" : @5};
    });
    return [cMap valueForKey:value];
}

@end
