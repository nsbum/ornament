//
//  CCFFakeValueTransformer.m
//  Ornament
//
//  Created by alanduncan on 10/30/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFFakeValueTransformer.h"

@implementation CCFFakeValueTransformer

+ (Class)transformedValueClass {
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation {
    return NO;
}

- (id)transformedValue:(id)value {
    NSLog(@"%s - %@",__FUNCTION__,NSStringFromClass([value class]));
    return value;
}

@end
