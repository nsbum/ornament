/**
 *   @file CCFResourceUploader.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-28 05:01:18
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFResourceUploader.h"
#import "CCFResourceUploader+Private.h"

@implementation CCFResourceUploader {
    NSURLConnection *_connection;
}

NSString *AFURLEncodedStringFromStringWithEncoding(NSString *string, NSStringEncoding encoding) {
    static NSString * const kAFLegalCharactersToBeEscaped = @"?!@#$^&%*+,:;='\"`<>()[]{}/\\|~ ";
    
    // Following the suggestion in documentation for `CFURLCreateStringByAddingPercentEscapes` to "pre-process" URL strings (using stringByReplacingPercentEscapesUsingEncoding) with unpredictable sequences that may already contain percent escapes.
	return (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)[string stringByReplacingPercentEscapesUsingEncoding:encoding], NULL, (CFStringRef)kAFLegalCharactersToBeEscaped, CFStringConvertNSStringEncodingToEncoding(encoding));
}

NSString *AFQueryStringFromParametersWithEncoding(NSDictionary *parameters, NSStringEncoding encoding) {
    NSMutableArray *mutableParameterComponents = [NSMutableArray array];
    for (id key in [parameters allKeys]) {
        id value = [parameters valueForKey:key];
        if ([value isKindOfClass:[NSArray class]]) {
            NSString *arrayKey = AFURLEncodedStringFromStringWithEncoding([NSString stringWithFormat:@"%@[]", [key description]], encoding);
            for (id arrayValue in value) {
                NSString *component = [NSString stringWithFormat:@"%@=%@", arrayKey, AFURLEncodedStringFromStringWithEncoding([arrayValue description], encoding)];
                [mutableParameterComponents addObject:component];
            }
        } else {
            NSString *component = [NSString stringWithFormat:@"%@=%@", AFURLEncodedStringFromStringWithEncoding([key description], encoding), AFURLEncodedStringFromStringWithEncoding([value description], encoding)];
            [mutableParameterComponents addObject:component];
        }
    }
    
    return [mutableParameterComponents componentsJoinedByString:@"&"];
}

- (id)init {
    self = [super init];
    if( !self ) return nil;
    
    _responseData = [NSMutableData new];
    
    return self;
}

- (id)initWithMethod:(NSString *)method url:(NSURL *)url parameters:(NSDictionary *)parameters {
    self = [self init];
    
    NSString *post = AFQueryStringFromParametersWithEncoding(parameters, NSUTF8StringEncoding);
    if( [method isEqualToString:@"GET" ] ) {
        NSString *urlString = [NSString stringWithFormat:@"%@?%@",[url absoluteString],post];
        _request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    }
    else {
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
        NSString *postLength = [NSString stringWithFormat:@"%ld", [postData length]];
        
        _request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
        
        [_request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [_request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [_request setHTTPBody:postData];
    }
    [_request setHTTPMethod:method];
    return self;
}

- (void)downloadWithCompletionBlock:(CCFDownloadCompletionBlock)handler {
    self.handler = handler;
    _connection = [[NSURLConnection alloc] initWithRequest:self.request delegate:self startImmediately:YES];
    
    /*
    NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
    [runLoop addPort:[NSPort port] forMode:NSDefaultRunLoopMode];
    [connection scheduleInRunLoop:runLoop forMode:NSDefaultRunLoopMode];
    [connection start];
    [runLoop run];
     */
}

- (void)connection:(NSURLConnection*)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite {
    QuietNote(@"sent body data");
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    QuietNote(@"rec'd response");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    QuietNote(@"did rec data");
    [[self responseData] appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    QuietNote(@"error in request: %@:%@",[error localizedDescription], [error userInfo]);
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    QuietNote(@"rec'd auth challenge");
    [[challenge sender] useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    [[challenge sender] continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    QuietLog(@"will send auth");
    if ([[[challenge protectionSpace] authenticationMethod] isEqualToString:@"NSURLAuthenticationMethodServerTrust"]) {
        [[challenge sender] performDefaultHandlingForAuthenticationChallenge:challenge];
    }
}

@end
