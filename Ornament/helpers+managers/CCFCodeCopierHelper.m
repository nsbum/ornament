/**
 *   @file CCFCodeCopierHelper.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-26 08:39:46
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */
#import "CCFCodeCopierHelper.h"
// model
#import "CCFRegex.h"
#import "CCFRegexOptions.h"

// controller
#import "CCFCopiedCodeWindowController.h"

@implementation CCFCodeCopierHelper {
    CCFCodeType _codeType;
    CCFCopiedCodeWindowController *_copiedCodeController;
}

- (id)initWithCodeType:(CCFCodeType)codeType {
    self = [super init];
    if( !self ) return nil;
    
    _codeType = codeType;
    
    return self;
}

- (void)exportCode {
    CCFCodeFormatter *formatter = [[CCFCodeFormatter alloc] initWithCodeType:_codeType];
    
    CCFRegex *regex = [[CCFOrnamentData sharedData] regex];
    formatter.regex = regex;
    
    NSPasteboard *generalPb = [NSPasteboard generalPasteboard];
    [generalPb clearContents];
    [generalPb writeObjects:@[formatter.codeString]];
    
    if( [[NSUserDefaults standardUserDefaults] boolForKey:kShowCopiedCodeAlertKey] ) {
        //  center the alert in the main window
        NSWindow *mainWindow = [[NSApplication sharedApplication] mainWindow];
        NSRect mainWindowFrame = mainWindow.frame;
        
        NSWindow *alertWindow = [[self copiedCodeController] window];
        NSRect alertFrame = alertWindow.frame;
        
        alertFrame.origin.x = NSMinX(mainWindowFrame) + 0.5f * (NSWidth(mainWindowFrame) - NSWidth(alertFrame));
        alertFrame.origin.y = NSMinY(mainWindowFrame) + 0.5f * (NSHeight(mainWindowFrame) - NSHeight(alertFrame));
        
        [[[self copiedCodeController] window] setFrame:alertFrame display:NO];
        [[self copiedCodeController] showWindow:self];
    }

}

#pragma mark - Accessors

- (CCFCopiedCodeWindowController *)copiedCodeController {
    if( !_copiedCodeController ) {
        _copiedCodeController = [[CCFCopiedCodeWindowController alloc] initWithCloseBlock:^{
            [_copiedCodeController close];
            _copiedCodeController = nil;
        }];
    }
    return _copiedCodeController;
}

@end
