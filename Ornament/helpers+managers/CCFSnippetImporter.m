//
//  CCFSnippetImporter.m
//  Ornament
//
//  Created by alanduncan on 10/23/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFSnippetImporter.h"
#import "CCFSnippetCategory.h"
#import "CCFSnippet.h"

@implementation CCFSnippetImporter {
    CCFSnippetImporterCompletionBlock _handler;
}

- (id)initWithCompletionBlock:(CCFSnippetImporterCompletionBlock)block {
    self = [super init];
    if( !self ) return nil;
    
    _handler = block;
    
    return self;
}

- (void)import {
    NSMutableArray *categories = [[NSMutableArray alloc] init];
    
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *path = [bundle pathForResource:@"CCFSnippetsList" ofType:@"plist"];
    NSString *okButton = NSLocalizedString(@"OK", @"OK button title");
    
    if( !path ) {
#if TARGET_OS_IPHONE
        NSString *message = NSLocalizedString(@"No Snippets", @"No snippet failure error title");
        NSString *info = NSLocalizedString(@"Could not find the Snippets file", @"no snippet failure error info");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message
                                                        message:info
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"ok button title")
                                              otherButtonTitles:nil];
        [alert show];
        NSLog(@"%s - ERROR - unable to load snippets file",__FUNCTION__);
#else
        NSString *message = NSLocalizedString(@"Snippets are unavailable", @"snippet load error msg");
        NSString *info = NSLocalizedString(@"Unable to load snippets from the bundle.  The application will still run, but you will not have code snippets.  If this error continues, please contacts the developer", @"snippet load error info");
#if TARGET_OS_IPHONE
        NSLog(@"%s - ERROR importing Snippets",__FUNCTION__);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:info delegate:nil cancelButtonTitle:okButton otherButtonTitles:nil];
        [alert show];
#else
        
        NSString *info = NSLocalizedString(@"Unable to load snippets from the bundle.  The application will still run, but you will not have code snippets.  If this error continues, please contacts the developer", @"snippet load error info");
        NSString *okButton = NSLocalizedString(@"OK", @"OK button title");
        NSAlert *alert = [NSAlert new];
        alert.messageText = message;
        alert.informativeText = info;
        [alert addButtonWithTitle:okButton];
        [alert runModal];
#endif
<<<<<<< HEAD
        
=======
>>>>>>> refs/heads/macos-1.1
        _handler(nil);
        return;
    }
    
    NSDictionary *plistInfo = [NSDictionary dictionaryWithContentsOfFile:path];
    NSArray *topLevelItems = plistInfo[@"topLevel"];
    for( NSDictionary *categoryInfo in topLevelItems) {
        CCFSnippetCategory *category = [[CCFSnippetCategory alloc] init];
        category.title = categoryInfo[@"title"];
        
        for( NSDictionary *snippetInfo in categoryInfo[@"snippets"] ) {
            CCFSnippet *snippet = [[CCFSnippet alloc] init];
            snippet.displayString = snippetInfo[@"disp"];
            snippet.explanation = snippetInfo[@"expl"];
            snippet.tag = [snippetInfo[@"tag"] integerValue];
            snippet.snip = snippetInfo[@"snip"];
            
            [category addSnippet:snippet];
        }
        [categories addObject:category];
    }
    _handler(categories);
}

@end
