//
//  CCFResourceDownloader+Private.m
//  AnxietyCoach
//
//  Created by alanduncan on 9/7/12.
//
//

#import "CCFResourceUploader+Private.h"

@implementation CCFResourceUploader (Private)

- (void)terminateRunLoop {
    [[NSRunLoop currentRunLoop] removePort:[NSPort port] forMode:kResouceUploadMode];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate date]];
}

@end
