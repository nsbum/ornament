/**
 *   @file CCFMenuManager.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-26 04:18:37
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFMainViewController;

@interface CCFMenuManager : NSObject <NSMenuDelegate>

@property (readonly) NSMenu *mainMenu;
@property (nonatomic, assign) IBOutlet CCFMainViewController *mainViewController;

- (void)setMenuItemState:(BOOL)state;

@end
