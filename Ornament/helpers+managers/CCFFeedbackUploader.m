//
//  CCFFeedbackUploader.m
//  Ornament
//
//  Created by alanduncan on 11/28/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFFeedbackUploader.h"
#import "CCFResourceUploader+Private.h"

NSString * const  CCFFeedbackAppVersionKey = @"CCFFeedbackAppVersionKey";
NSString * const  CCFFeedbackSysVersionKey = @"CCFFeedbackSysVersionKey";
NSString * const  CCFFeedbackMessageKey = @"CCFFeedbackMessageKey";
NSString * const  CCFFeedbackEmailKey = @"CCFFeedbackEmailKey";
NSString * const  CCFMachineNameKey = @"CCFMachineNameKey";
NSString * const  CCFIssueTypeKey = @"CCFIssueTypeKey";
NSString * const  CCFAppIDKey = @"CCFAppIDKey";

@implementation CCFFeedbackUploader

- (id)initWithDictionary:(NSDictionary *)info {
    NSMutableDictionary *mutableParams = [NSMutableDictionary new];

    [mutableParams setObject:info[CCFFeedbackAppVersionKey] forKey:@"version"];
    [mutableParams setObject:info[CCFFeedbackSysVersionKey] forKey:@"sysversion"];
    [mutableParams setObject:info[CCFFeedbackMessageKey] forKey:@"message"];
    [mutableParams setObject:info[CCFFeedbackEmailKey] forKey:@"email"];
    [mutableParams setObject:info[CCFMachineNameKey] forKey:@"machine"];
    [mutableParams setObject:info[CCFIssueTypeKey] forKey:@"type"];
    [mutableParams setObject:[NSNumber numberWithInteger:0] forKey:@"app"];

    NSURL *url = [NSURL URLWithString:@"https://www.cocoafactory.net:8080/feedback/fbk"];
    self = [super initWithMethod:@"POST" url:url parameters:mutableParams];
    
    return self;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self terminateRunLoop];
    
    NSError *jsonError = nil;
    NSDictionary *jsonInfo = [NSJSONSerialization JSONObjectWithData:self.responseData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
    if( jsonError ) {
        NSAlert *alert = [NSAlert alertWithError:jsonError];
        [alert runModal];
        self.handler(3,nil);
    }
    else {
        CCFAPIStatus status = (CCFAPIStatus)[jsonInfo integerForKey:@"api_status"];
        NSArray *dataElements = [jsonInfo objectForKey:@"data"];
        NSString *ticketID = nil;
        if( dataElements && dataElements.count != 0 ) {
            ticketID = [dataElements[0] objectForKey:@"maxid"];
        }
        self.handler(status, @[ticketID]);
    }
}

@end
