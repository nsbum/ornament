/**
 *   @file CCFNetworkMediator.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-08-24 07:58:31
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

typedef void(^CCFNetworkReturnBlock)(NSArray *remoteObjs);

typedef enum  : NSInteger {
    CCFAPIStatusSuccess = 0,
    CCFAPIStatusSuccessNoRows,
    CCFAPIStatusFailConnect,
    CCFAPIStatusFailQuery,
    CCFAPIStatusFailMissingParam,
    CCFAPIStatusFailOther
} CCFAPIStatus;

#define HANDLER_IF_EXISTS(a) if( handler ) { handler(a); }
#define HANDLER_NIL_IF_EXISTS if( handler ) { handler(nil); }

@interface CCFNetworkMediator : CCFObject

@end
