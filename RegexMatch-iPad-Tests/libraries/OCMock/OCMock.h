//---------------------------------------------------------------------------------------
//  $Id$
//  Copyright (c) 2004-2008 by Mulle Kybernetik. See License file for details.
//---------------------------------------------------------------------------------------

#if TARGET_OS_IPHONE
#import "OCMockObject.h"
#import "OCMockRecorder.h"
#import "OCMConstraint.h"
#import "OCMArg.h"
#import "NSNotificationCenter+OCMAdditions.h"
#else
#import <OCMock/OCMockObject.h>
#import <OCMock/OCMockRecorder.h>
#import <OCMock/OCMConstraint.h>
#import <OCMock/OCMArg.h>
#import <OCMock/NSNotificationCenter+OCMAdditions.h>
#endif

