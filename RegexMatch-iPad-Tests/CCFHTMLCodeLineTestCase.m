#import "CCFTestCase.h"
#import "CCFHTMLCodeLine.h"

@interface CCFHTMLCodeLineTestCase : CCFTestCase {
    CCFHTMLCodeLine *_codeLine;
}

@end

@implementation CCFHTMLCodeLineTestCase

- (void)setUp {
    [super setUp];
    _codeLine = [CCFHTMLCodeLine new];
}

- (void)tearDown {
    _codeLine = nil;
    [super tearDown];
}

#pragma mark - Property tests

- (void)testThatHTMLCodeLineHasCodeStringProperty {
    objc_property_t property = class_getProperty([CCFHTMLCodeLine class], "codeString");
    STAssertTrue(property != NULL, @"CCFHTMLCodeLine has no codeString property");
}

#pragma mark - Initialization

- (void)testThatDesinatedInitializerSetsCodeString {
    _codeLine = [[CCFHTMLCodeLine alloc] initWithString:@"my $sampleText = 'hello, world.'"];
    STAssertTrue([@"my $sampleText = 'hello, world.'" isEqualToString:_codeLine.codeString], @"Incorrect code string from initializer");
}

#pragma mark - Formatting

- (void)testThatWeCanReturnHTMLCodeVariationA {
    _codeLine.codeString = @"my $sampleText = 'hello';";
    NSString *expected = @"<div style=\"margin: 0px; font-size: 11px; font-family: Menlo; color: rgb(39, 40, 34); \">my $sampleText = 'hello';</div>";
    STAssertTrue([expected isEqualToString:_codeLine.htmlCode], @"Incorrect HTML code for variation A");
}

- (void)testThatWeCanReturnHTMLCodeWithAmpersandSubstitution {
    _codeLine.codeString = @"long p = &q;";
    NSString *expected = @"<div style=\"margin: 0px; font-size: 11px; font-family: Menlo; color: rgb(39, 40, 34); \">long p = &ampq;</div>";
    STAssertTrue([expected isEqualToString:_codeLine.htmlCode], @"Incorrect HTML code for ampersand substitution.");
}


@end
