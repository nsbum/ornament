import re


pattern = '\d{3}'

# single match
m = re.search(pattern, sampleText)

# iteration
for match in re.finditer(pattern, sampleText):
	print match.group()

# findall
for s in re.findall(pattern, sampleText):
	print s