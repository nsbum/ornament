#import "CCFTestCase.h"
#import "CCFHTMLExport.h"

@interface CCFHTMLExportTestCase : CCFTestCase {
    CCFHTMLExport *_export;
}

@end

@implementation CCFHTMLExportTestCase

- (void)setUp {
    [super setUp];
    
    _export = [[CCFHTMLExport alloc] init];
}

- (void)tearDown {
    _export = nil;
    
    [super tearDown];
}

#pragma mark - Property tests

- (void)testThatHTMLExportHasCodeStringProperty {
    objc_property_t property = class_getProperty([CCFHTMLExport class], "codeString");
    STAssertTrue(property != NULL, @"CCFHTMLExport has no codeString property");
}

#pragma mark - Behavior tests

- (void)testThatDesignatedInitializerSetsCodeString {
    _export = [[CCFHTMLExport alloc] initWithCodeString:@"something"];
    STAssertTrue([@"something" isEqualToString:_export.codeString], @"DI does not set codeString property.");
}

- (void)testThatWeCanConstructProperHTMLCodeVariationA {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *variationACode = [NSString stringWithContentsOfFile:[bundle pathForResource:@"expected-html-export-var-A-source" ofType:@"py"]
                                                         encoding:NSUTF8StringEncoding
                                                            error:NULL];
    _export.codeString = variationACode;
    NSString *expected = [NSString stringWithContentsOfFile:[bundle pathForResource:@"expected-html-export-var-A" ofType:@"html"]
                                                   encoding:NSUTF8StringEncoding
                                                      error:NULL];
    NSString *actual = [_export htmlString];
    //STAssertTrue([expected isEqualToString:actual], @"Incorrect HTML string returned for variation A");
}

@end
