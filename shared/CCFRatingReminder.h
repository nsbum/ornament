/**
 *   @file CCFRatingReminder.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2013-02-04 02:06:42
 *   @version 1.0
 *
 *   @note Copyright 2013 Cocoa Factory, LLC.  All rights reserved
 */

#if TARGET_OS_IPHONE
//  any headers to import for iOS
#else
#import "CCFRatingReminderWindowControllerDelegate.h"
#endif

@interface CCFRatingReminder : NSObject
#if TARGET_OS_IPHONE
//  any delegate protocols to which iOS should conform
#else
<CCFRatingReminderWindowControllerDelegate>
#endif

+ (id)sharedReminder;
- (void)remindIfNeeded;

@end
