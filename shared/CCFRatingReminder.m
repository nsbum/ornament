/**
 *   @file CCFRatingReminder.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2013-02-04 02:06:53
 *   @version 1.0
 *
 *   @note Copyright 2013 Cocoa Factory, LLC.  All rights reserved
 */

static const NSInteger ReminderLaunchCountThreshold = 5;
static const NSInteger LauchDateThreshold = 3;

#import "CCFRatingReminder.h"
#if TARGET_OS_IPHONE
//  include whatever we need for iPhone reminder
#else
#import "CCFRatingReminderWindowController.h"
#import "CCFAppDelegate.h"
#import "CCFMainWindowController.h"
#endif

@implementation CCFRatingReminder {
#if TARGET_OS_IPHONE
    //  iOS instance vars
#else
    CCFRatingReminderWindowController *_ratingReminderController;
}
#endif

+ (id)sharedReminder {
    static dispatch_once_t pred;
    static CCFRatingReminder *cSharedInstance = nil;
    
    dispatch_once(&pred, ^{ cSharedInstance = [[self alloc] init]; });
    return cSharedInstance;
}

- (void)remindIfNeeded {
    BOOL shouldRemind = [[NSUserDefaults standardUserDefaults] boolForKey:kRatingShouldAsk];
    if( !shouldRemind ) return;
    
    NSInteger launchCount = [[NSUserDefaults standardUserDefaults] integerForKey:kLaunchCount];
    if( launchCount < ReminderLaunchCountThreshold ) return;
    
    NSDate *firstLaunchDate = [[NSUserDefaults standardUserDefaults] objectForKey:kFirstLaunchDate];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger unitFlags = NSDayCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:firstLaunchDate toDate:[NSDate date] options:0];
    NSInteger daysSinceFirstLaunch = [components day];
    if( daysSinceFirstLaunch < LauchDateThreshold ) return;
    
    //  met criteria to ask user
#if TARGET_OS_IPHONE
    //  nothing yet
#else
    CCFAppDelegate *appDelegate = [NSApp delegate];
    NSWindow *mainWindow = appDelegate.windowController.window;
    NSRect ratingFrame = [[[self ratingReminderController] window] frame];
    NSRect mainFrame = mainWindow.frame;
    NSRect centeredFrame = NSMakeRect(mainFrame.origin.x + 0.5 * (mainFrame.size.width - ratingFrame.size.width),
                                      mainFrame.origin.y + 0.5 * (mainFrame.size.height - ratingFrame.size.height),
                                      ratingFrame.size.width,
                                       ratingFrame.size.height);
    [[[self ratingReminderController] window] setFrame:centeredFrame display:NO];
    [[self ratingReminderController] showWindow:self];
    return;
    
    
    
    
#endif
}

#pragma mark - CCFRatingReminderWindowControllerDelegate

- (void)ratingWindowControllerDidSelectRate:(id)controller {
    [[[self ratingReminderController] window] close];
    _ratingReminderController = nil;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kRatingShouldAsk];
    //  launch rating URL
    NSString *urlString = @"macappstore://appstore.com/mac/regexmatch";
    NSURL *url = [NSURL URLWithString:urlString];
    [[NSWorkspace sharedWorkspace] openURL:url];
}

- (void)ratingWindowControllerDidSelectCancel:(id)controller {
    [[[self ratingReminderController] window] close];
    _ratingReminderController = nil;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kRatingShouldAsk];
}

- (void)ratingWindowControllerDidSelectDefer:(id)controller {
    [[[self ratingReminderController] window] close];
    _ratingReminderController = nil;
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:kLaunchCount];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:kFirstLaunchDate];
}

#if TARGET_OS_IPHONE

#else
- (CCFRatingReminderWindowController *)ratingReminderController {
    if( !_ratingReminderController ) {
        _ratingReminderController = [[CCFRatingReminderWindowController alloc] initWithNib];
        _ratingReminderController.delegate = self;
    }
    return _ratingReminderController;
}
#endif

@end
