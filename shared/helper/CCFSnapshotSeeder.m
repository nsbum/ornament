/**
 *   @file CCFSnapshotSeeder.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 10:23:31
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSnapshotSeeder.h"

#import "CCFSnapshot.h"

static NSString * const SnapshotSeedFile = @"snapshots";

@implementation CCFSnapshotSeeder

- (void)importWithBlock:(CCFSnapshotSeederImportBlock)block {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *seedPath = [bundle pathForResource:SnapshotSeedFile ofType:@"json"];
    
    //  if we do not have a seed path
    if( !seedPath ) {
#if TARGET_OS_IPHONE
<<<<<<< HEAD
        NSLog(@"%s - ERROR finding Snapshot see file",__FUNCTION__);
=======
        NSString *title = NSLocalizedString(@"Unable to seed Snapshots", @"snapshot seed failure alert title");
        NSString *message = NSLocalizedString(@"Could not load Snapshot file", @"snapshot seed failure alert message");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"ok button title")
                                              otherButtonTitles:nil];
        [alert show];
        NSLog(@"%s - ERROR - No Snapshot seed path.",__FUNCTION__);
>>>>>>> refs/heads/macos-1.1
#else
        NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Error locating the Snapshot seed file", @"snapshot seed error msg")
                                         defaultButton:@"OK"
                                       alternateButton:nil
                                           otherButton:nil
                             informativeTextWithFormat:NSLocalizedString(@"The Snapshot seed file appears not be in the application bundle.  Snapshots will still work, but you will not have any example Snapshots to start.", @"Snapshot seed error info")];
        [alert runModal];
#endif
        block(nil);
        return;
    }
    NSData *jsonData = [NSData dataWithContentsOfFile:seedPath];
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    NSArray *jsonObjects = [jsonObject objectForKey:@"data"];
    [jsonObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *jsonObject = (NSDictionary *)obj;
        CCFSnapshot *snapshot = [[CCFSnapshot alloc] init];
        snapshot.regexString = jsonObject[@"regexString"];
        snapshot.sampleText = jsonObject[@"sampleText"];
        snapshot.descriptor = jsonObject[@"description"];
        snapshot.regexOptions = [jsonObject[@"options"] integerValue];
        
        block(snapshot);
    }];
    
    block(nil);
}

@end
