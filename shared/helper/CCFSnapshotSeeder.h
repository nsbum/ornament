/**
 *   @file CCFSnapshotSeeder.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 10:23:38
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFSnapshot;

typedef void(^CCFSnapshotSeederImportBlock)(CCFSnapshot *snapshot);

@interface CCFSnapshotSeeder : NSObject

- (void)importWithBlock:(CCFSnapshotSeederImportBlock)block;

@end
