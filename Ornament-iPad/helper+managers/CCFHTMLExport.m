/**
 *   @file CCFHTMLExport.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-15 20:31:48
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFHTMLExport.h"
#import "CCFHTMLCodeLine.h"

@implementation CCFHTMLExport

- (id)initWithCodeString:(NSString *)codeString {
    self = [super init];
    if( !self ) return nil;
    
    _codeString = codeString;
    
    return self;
}

- (NSString *)htmlString {
    NSString *templatePath = [[NSBundle bundleForClass:[self class]] pathForResource:[self templateName] ofType:@"html"];
    NSString *template = [NSString stringWithContentsOfFile:templatePath encoding:NSUTF8StringEncoding error:nil];
    
    NSMutableString *codeBlock = [[NSMutableString alloc] init];
    [[self codeString] enumerateLinesUsingBlock:^(NSString *line, BOOL *stop) {
        //  empty lines should have hard break, otherwise format as a code line
        if( line.length == 0 ) {
            [codeBlock appendFormat:@"\t\t\t<br>\n"];
        }
        else {
            CCFHTMLCodeLine *codeLine = [[CCFHTMLCodeLine alloc] initWithString:line];
            [codeBlock appendFormat:@"\t\t\t%@\n",[codeLine htmlCode]];
        }
    }];

    return [template stringByReplacingOccurrencesOfString:@"ß" withString:codeBlock];
}

#pragma mark - Private

- (NSString *)templateName {
    if( self.forEmail ) {
        return @"html-export-template";
    }
    return @"html-export-template-pasteboard";
}

@end
