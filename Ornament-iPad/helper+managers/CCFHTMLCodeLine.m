/**
 *   @file CCFHTMLCodeLine.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-15 19:26:35
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFHTMLCodeLine.h"

static NSString *HTMLCodeLineTemplate;

@implementation CCFHTMLCodeLine

+ (void)initialize {
    if( self == [CCFHTMLCodeLine class] ) {
        HTMLCodeLineTemplate = @"<div style=\"margin: 0px; font-size: 11px; font-family: Menlo; color: rgb(39, 40, 34); \">ß</div>";
    }
}

- (id)initWithString:(NSString *)codeString {
    self = [super init];
    if( !self ) return nil;
    
    self.codeString = codeString;
    
    return self;
}

#pragma mark - Public

- (NSString *)htmlCode {
    return [HTMLCodeLineTemplate stringByReplacingOccurrencesOfString:@"ß" withString:self.codeString];
}

#pragma mark - Accessors

- (void)setCodeString:(NSString *)codeString {
    _codeString = [codeString stringByReplacingOccurrencesOfString:@"&" withString:@"&amp"];
    _codeString = [_codeString stringByReplacingOccurrencesOfString:@"\t" withString:@"&nbsp&nbsp&nbsp&nbsp"];
}

@end
