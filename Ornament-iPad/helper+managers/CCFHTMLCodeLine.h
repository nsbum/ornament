/**
 *   @file CCFHTMLCodeLine.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-15 19:26:44
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */


/** This class handles the conversion of a single line of code to HTML
 */
@interface CCFHTMLCodeLine : NSObject

/** Code string
 A single line of code as NSString.
 */
@property (nonatomic, strong) NSString *codeString;

/** Designated initializer
 
 This method creates a new instance of the class to represent the included code string.
 
 @param codeString An NSString object that represents a line of code
 @return The newly-initialized instance of the class
 */
- (id)initWithString:(NSString *)codeString;

/** The html representation of the line of code
 
 @return Returns the HTML representation of the code.
 */
- (NSString *)htmlCode;

@end
