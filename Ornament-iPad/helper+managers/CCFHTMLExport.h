/**
 *   @file CCFHTMLExport.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-15 20:31:41
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

/** A helper class that provides facilities to convert code to an HTML representation
 */
@interface CCFHTMLExport : NSObject

/** A string object that represents the entire block of text to convert
 */
@property (nonatomic, strong) NSString *codeString;

@property (nonatomic, assign) BOOL forEmail;

/** Designated initializer 
 
 @param codeString An NSString object that holds the entire block of text to convert to HTML.
 @return A newly initialized instance of the class.
 */
- (id)initWithCodeString:(NSString *)codeString;

/** The HTML string
 
 @return This method returns the marked-up HTML version of the code block provided by the `codeString` property.
 */
- (NSString *)htmlString;

@end
