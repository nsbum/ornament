/**
 *   @file CCFBarButtonItem.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2011-04-30 05:58:20
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFBarButtonItem.h"
#import <UIKit/UIButton.h>
#import <UIKit/UIView.h>
#import <UIKit/UIImage.h>
#import <objc/runtime.h>

static NSString *CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY = @"com.cocoafactory.barbuttonitem";

@implementation CCFBarButtonItem

#pragma mark - Object life cycle

- (id)initWithImage:(UIImage *)image target:(id)target action:(SEL)action;
{
    _originalTarget = target;
    
    UIButton *imgButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [imgButton setImage:image forState:UIControlStateNormal];
    imgButton.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    [imgButton addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    self = [super initWithCustomView:imgButton];
    
    return self;
}

- (id)initWithLightInfoButtonBlock:(void (^)(id))aBlock;
{
    UIButton *tempButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    UIImage *image = [tempButton currentImage];
    
    return [self initWithImage:image style:UIBarButtonItemStylePlain block:aBlock];
}

- (id)initWithLightInfoButtonTarget:(id)target action:(SEL)action;
{
    _originalTarget = target;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [button setFrame:CGRectMake(0, 0, 25, 25)];
    [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [super initWithCustomView:button];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector;
{
    if( [_originalTarget respondsToSelector:aSelector] )
    {
        return [_originalTarget methodSignatureForSelector:aSelector];
    }
    else
    {
        return [super methodSignatureForSelector:aSelector];
    }
}

- (void)forwardInvocation:(NSInvocation *)anInvocation;
{
    SEL aSelector = [anInvocation selector];
    if( [_originalTarget respondsToSelector:aSelector] )
    {
        //  modify the 'sender' argument so that it points to self
        __unsafe_unretained CCFBarButtonItem *weakSelf = self;
        [anInvocation setArgument:&weakSelf atIndex:2];
        [anInvocation invokeWithTarget:_originalTarget];
    }
    else
    {
        [self doesNotRecognizeSelector:aSelector];
    }
}

- (id)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem block:(void (^)(id))aBlock;
{
    self = [super initWithBarButtonSystemItem:systemItem target:nil action:@selector(CCFCallbackBlockWithSender:)];
    if( !self ) { return nil; }
    
    objc_setAssociatedObject(self,(__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY,aBlock,OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.target = objc_getAssociatedObject(self, (__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY);
    
    return self;
}

- (id)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style block:(void (^)(id))aBlock;
{
    self = [super initWithImage:image style:style target:nil action:@selector(CCFCallbackBlockWithSender:)];
    if( !self ) { return nil; }
    
    objc_setAssociatedObject(self,(__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY,aBlock,OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.target = objc_getAssociatedObject(self, (__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY);
    
    return self;
}

- (id)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style block:(void (^)(id))aBlock;
{
    self = [super initWithTitle:title style:style target:nil action:@selector(CCFCallbackBlockWithSender:)];
    if( !self ) { return nil; }
    
    objc_setAssociatedObject(self,(__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY,aBlock,OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.target = objc_getAssociatedObject(self, (__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY);
    
    return self;
}

@end
