//
//  CCFAppDelegate.m
//  Ornament-iPad
//
//  Created by alanduncan on 11/7/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "AppDelegate_iPad.h"

#import "CCFSnapshotSeeder.h"

#import "CCFMasterViewController.h"
#import "CCFDetailViewController.h"

//  convenience function to insert color NSData into a dictionary (for user defaults)
static void insertColorIntoDictionary(UIColor *color, NSMutableDictionary *dict, NSString *key) {
    dict[key] = [NSKeyedArchiver archivedDataWithRootObject:color];
}

@implementation AppDelegate_iPad

- (id)init {
    self = [super init];
    if( !self ) return nil;
    
    [self registerUserDefaults];
    
    
    return self;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithWhite:0.490 alpha:1.000]];

    CCFMasterViewController *masterViewController = [[CCFMasterViewController alloc] initWithNibName:@"CCFMasterViewController" bundle:nil];
    UINavigationController *masterNavigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];
    //masterNavigationController.navigationBar.tintColor = [UIColor colorWithWhite:0.490 alpha:1.000];

    CCFDetailViewController *detailViewController = [[CCFDetailViewController alloc] initWithNibName:@"CCFDetailViewController" bundle:nil];
    UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
    //detailNavigationController.navigationBar.tintColor = [UIColor colorWithWhite:0.490 alpha:1.000];

    masterViewController.detailViewController = detailViewController;

    self.splitViewController = [[UISplitViewController alloc] init];
    self.splitViewController.delegate = detailViewController;
    self.splitViewController.viewControllers = @[masterNavigationController, detailNavigationController];
    self.window.rootViewController = self.splitViewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Private

- (void)registerUserDefaults {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *defaultsPath = [bundle pathForResource:@"CCFInitialUserDefaults" ofType:@"plist"];
    NSMutableDictionary *initialDefaults = nil;
    initialDefaults = [NSMutableDictionary dictionaryWithDictionary:[NSDictionary dictionaryWithContentsOfFile:defaultsPath]];
    
    //  setup some default colors, though
    insertColorIntoDictionary([UIColor colorWithRed:0.908 green:0.379 blue:0.236 alpha:1.000], initialDefaults, kQuantifierColorDataKey);
    insertColorIntoDictionary([UIColor colorWithRed:0.185 green:0.413 blue:0.575 alpha:1.000], initialDefaults, kGroupingColorDataKey);
    insertColorIntoDictionary([UIColor colorWithRed:0.713 green:0.207 blue:0.504 alpha:1.000], initialDefaults, kCharClassColorDataKey);
    insertColorIntoDictionary([UIColor colorWithRed:0.709 green:0.692 blue:0.369 alpha:1.000], initialDefaults, kMetacharacterColorDataKey);
    insertColorIntoDictionary([UIColor colorWithRed:0.506 green:0.751 blue:0.344 alpha:1.000], initialDefaults, kNumberedGroupColorDataKey);
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:initialDefaults];
}

@end
