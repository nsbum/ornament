//
//  CCFAppDelegate.h
//  Ornament-iPad
//
//  Created by alanduncan on 11/7/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate_iPad : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UISplitViewController *splitViewController;

@end
