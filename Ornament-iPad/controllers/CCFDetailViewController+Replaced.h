/**
 *   @file CCFDetailViewController+Replaced.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-0904:17:16
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFDetailViewController.h"

/** Category on CCFDetailViewController that manages replaced text
 */
@interface CCFDetailViewController (Replaced)

/** Clear the replaced text view
 */
- (void)clearReplacedText;

/** Perform replacement
 
 This method is called whenever the regex or source text changes so that the replaced view
 can perform and substitutions that are required by the resultant matches.
 
 */
- (void)performReplacement;

@end
