/**
 *   @file CCFSnapshotCellSelectionView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-15 20:48:58
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFGradientView.h"

/** A gradient view for the selection of CCFSnapshotCell
 */
@interface CCFSnapshotCellSelectionView : CCFGradientView

@end
