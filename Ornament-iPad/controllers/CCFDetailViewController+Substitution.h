/**
 *   @file CCFDetailViewController+Substitution.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 04:20:03
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFDetailViewController.h"

/** Category on CCFDetailViewController that manages the substitution view.
 
 The substitution view allows the user to enter text that should be substituted for
 matches.
 */
@interface CCFDetailViewController (Substitution)

/** Clears the substitution view 
 */
- (void)clearSubstitutionText;

@end
