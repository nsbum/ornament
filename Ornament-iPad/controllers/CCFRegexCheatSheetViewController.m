/**
 *   @file CCFRegexCheatSheetViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-12-15 23:19:22
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFRegexCheatSheetViewController.h"

static NSString * const NibName = @"CCFRegexCheatSheetView";

@interface CCFRegexCheatSheetViewController ()
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@end

@implementation CCFRegexCheatSheetViewController

- (id)initWithNib {
    self = [super initWithNibName:NibName bundle:nil];
    if( !self ) return nil;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSBundle *mainBundle = [NSBundle bundleForClass:[self class]];
    NSURL *url = [mainBundle URLForResource:@"RegexSyntaxCheatSheet" withExtension:@"html"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [[self webView] loadRequest:request];
}


@end
