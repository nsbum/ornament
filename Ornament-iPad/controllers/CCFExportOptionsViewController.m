//
//  CCFExportOptionsViewController.m
//  Ornament
//
//  Created by alanduncan on 12/28/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFExportOptionsViewController.h"
#import "CCFUserDefaultConstants.h"

static NSString * const NibName = @"CCFExportOptionsViewController";

@interface CCFExportOptionsViewController ()
@property (nonatomic, weak) IBOutlet UISwitch *useSampleTextSwitch;
@end

@implementation CCFExportOptionsViewController

- (id)initWithNib {
    self = [super initWithNibName:NibName bundle:nil];
    if( !self ) return nil;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Export options", @"Title for export options view in popover");
    self.contentSizeForViewInPopover = CGSizeMake(371, 110);
    self.enclosingPopover.popoverContentSize = CGSizeMake(371, 110);
    
	BOOL useSample = [[NSUserDefaults standardUserDefaults] boolForKey:kUseSampleTextInCode];
    [[self useSampleTextSwitch] setOn:useSample];
}

- (IBAction)changeState:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:[[self useSampleTextSwitch] isOn] forKey:kUseSampleTextInCode];
}

@end
