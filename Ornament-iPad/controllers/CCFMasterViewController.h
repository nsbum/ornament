/**
 *   @file CCFMasterViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 14:39:34
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFDetailViewController;

/** A UITableViewController subclass that controls the display of snapshots and holds a reference
 to a detail controller */
@interface CCFMasterViewController : UITableViewController

/** A strong reference to the detail view controller
 */
@property (strong, nonatomic) CCFDetailViewController *detailViewController;

/** Saves the snapshots to the applications documents directory 
 */
- (void)saveSnapshots;

@end
