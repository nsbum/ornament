/**
 *   @file CCFDetailViewController+Regex.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-08 05:39:59
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFDetailViewController.h"

/** Category on CCFDetailViewController that manages regular expression functionality
 */
@interface CCFDetailViewController (Regex) <UIPopoverControllerDelegate>

/** Sets the regex string to the text from snapshot
 
 This message is sent to CCFDetailViewController instances when a CCFSnapshot object is selected.
 
 @param regexString An NSString object that holds a reference to the snapshot's regex string.
 */
- (void)setSnapshotRegexString:(NSString *)regexString;

/** Sets up the regex view
 
 This message is sent by the CCFDetailViewController -viewDidLoad method, primarily to set up the accessory
 keyboard when the view loads.
 */
- (void)regexViewDidLoad;

/** Regex text changed
 
 This message is sent by the delegate of the _regexTextView when the text changes.  This method handles all of the
 syntax highlighting functions and the cascade of matching and markup in other views.
 */
- (void)regexTextDidChange;

/** Insert a snippet from notification
 
 This message is sent by an observer of the kDidSelectSnippetNotification notification.
 
 @param note The kDidSelectSnippetNotification object which contains a userInfo dictionary with a single key-value pair.  The 
        key kSnippetKey points to the CCFSnippet object to be inserted.
 */
- (void)insertSnippetWithNotification:(NSNotification *)note;

/** Adjust regex keyboard accessory view on orientation change
 
 This message is sent by an observer of the UIApplicationDidChangeStatusBarOrientationNotification notification to detect rotation changes.
 
 */
- (void)adjustKeyboardAccessoryViewForOrientationNotification:(NSNotification *)note;

/** Adjust regex keyboard accessory view when the keyboard appears.
 
 This message is sent by an observer of the UIKeyboardWillShowNotification notification.
 */
- (void)adjustKeyboardAccessoryViewForKeyboardAppearanceNotification:(NSNotification *)note;

/** Deleted association objects
 
 This category uses associated objects via the Objective-C runtime.  This message is sent when the 
 instance is about to be deallocated in order to delete any associated objects that the category creates for its use.
 */
- (void)deleteAssociatedObjectsForRegex;

@end
