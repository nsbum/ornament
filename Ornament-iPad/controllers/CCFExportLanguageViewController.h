/**
 *   @file CCFExportLanguageViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 16:13:05
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

/** A table view controller subclass that shows the available export programming languages
 */
@interface CCFExportLanguageViewController : UITableViewController

/** A reference to the popover controller that contains the instance of this class.
 
 Allows for proper resizing behavior
 */
@property (nonatomic, assign) UIPopoverController *enclosingPopover;

@end
