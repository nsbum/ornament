/**
 *   @file CCFSnapshotCellSelectionView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-15 20:49:06
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSnapshotCellSelectionView.h"

@implementation CCFSnapshotCellSelectionView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    
    [self fadeVerticallyFromColor:[UIColor colorWithWhite:0.569 alpha:1.000] toColor:[UIColor colorWithWhite:0.318 alpha:1.000]];
    
    return self;
}

@end
