/**
 *   @file CCFRegexOptionsTableViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 04:32:04
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

/** A table view controller subclass that presents options for the regex
 */
@interface CCFRegexOptionsTableViewController : UITableViewController

/** Regular expression options represented by the table
 */
@property (nonatomic, assign) NSRegularExpressionOptions options;

@end
