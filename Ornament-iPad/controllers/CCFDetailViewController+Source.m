/**
 *   @file CCFDetailViewController+Source.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 04:34:17
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */
#import "CCFDetailViewController+Source.h"

@implementation CCFDetailViewController (Source)

#pragma mark - Public

- (void)setSnapshotSampleString:(NSString *)sampleString {
    NSDictionary *attributes = @{NSForegroundColorAttributeName : [UIColor darkTextColor], NSFontAttributeName : [UIFont fontWithName:@"Courier" size:18.0f]};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:sampleString attributes:attributes];
    _sourceTextView.attributedText = attributedString;
    
    [self performHightlighting];
}

- (void)clearSourceText {
    _sourceTextView.attributedText = [[NSAttributedString alloc] initWithString:@""];
}

- (void)performHightlighting {
    NSRange selectedRange = _sourceTextView.selectedRange;
    //  this may not be the best way to do this...
    [self removeSourceHighlighting];
    
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithAttributedString:_sourceTextView.attributedText];
    UIColor *bgColor = [UIColor colorWithRed:0.998 green:0.887 blue:0.625 alpha:1.000];
    for( NSTextCheckingResult *match in _matches ) {
        if( match.numberOfRanges != 0 ) {
            NSRange range = [match rangeAtIndex:0];
            
            @try {
                [mutableAttributedString addAttribute:NSBackgroundColorAttributeName value:bgColor range:range];
            }
            @catch (NSException *exception) {
                NSString *r = NSStringFromRange(range);
                printf("Range = %s\n",[r UTF8String]);
            }
        }
    }
    
    [_sourceTextView setAttributedText:mutableAttributedString];
    [_sourceTextView setSelectedRange:selectedRange];
}

- (void)removeSourceHighlighting {
    NSRange selectedRange = _sourceTextView.selectedRange;
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithAttributedString:_sourceTextView.attributedText];
    [mutableAttributedString enumerateAttribute:NSBackgroundColorAttributeName
                                        inRange:entireStringRange(mutableAttributedString)
                                        options:0
                                     usingBlock:^(id value, NSRange range, BOOL *stop) {
                                         [mutableAttributedString removeAttribute:NSBackgroundColorAttributeName range:range];
                                     }];
    [_sourceTextView setAttributedText:mutableAttributedString];
    [_sourceTextView setSelectedRange:selectedRange];
}

@end
