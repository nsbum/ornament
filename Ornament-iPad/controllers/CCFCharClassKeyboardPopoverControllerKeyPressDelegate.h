/**
 *   @file CCFCharClassKeyboardPopoverControllerKeyPressDelegate.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 08:26:27
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFCharClassKeyboardPopoverViewController;

/** Delegate protocol for the CCFCharClassKeyboardPopoverViewController class
 */
@protocol CCFCharClassKeyboardPopoverControllerKeyPressDelegate <NSObject>

/** Delegate message sent by an instance of CCFCharClassKeyboardPopoverViewController for key press
 
 @param sequence The sequence (usually dyad) of characters sent by the key that was pressed.
 */
- (void)charClassKeyboardPopoverControllerDidReceiveSequence:(NSString *)sequence;

@end
