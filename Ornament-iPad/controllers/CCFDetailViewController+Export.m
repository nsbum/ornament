/**
 *   @file CFDetailViewController+Export.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 16:23:09
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

//  the char key for the popover's associated object
static char ExportPopoverKey;

#import "CCFDetailViewController+Export.h"
#import <objc/runtime.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MobileCoreServices/UTCoreTypes.h>

#import "CCFRegex.h"

#import "CCFCodeFormatter.h"
#import "CCFHTMLExport.h"
#import "CCFExportContentSizes.h"

#import "CCFExportLanguageViewController.h"
#import "CCFNotificationConstants.h"

enum {exportTypePasteboard,exportTypeEmail};

@implementation CCFDetailViewController (Export)

#pragma mark - Object lifecycle

- (void)deleteAssociatedObjectsForExport {
    objc_setAssociatedObject(self, &ExportPopoverKey, nil, OBJC_ASSOCIATION_RETAIN);
}

#pragma mark - Public

- (void)handleExportRequestWithNotification:(NSNotification *)note {
    
    //  dismiss the popover
    [[self exportOptionsPopover] dismissPopoverAnimated:YES];
    
    //  after 500 ms, pop to the root view controller and reset the content size
    //  so that the next time we show the popover, it has the right view and size
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        UINavigationController *root = (UINavigationController *)[[self exportOptionsPopover] contentViewController];
        [root popToRootViewControllerAnimated:NO];
        [[self exportOptionsPopover] setPopoverContentSize:CGSizeMake(320.0f, LANGUAGE_CONTROLLER_HEIGHT + 44.0f)];
    });
    
    //  handle the export request based on the language and export option
    NSDictionary *userInfo = note.userInfo;
    CCFCodeType codeType = [userInfo[kExportLanguageNotificationKey] integerValue];
    NSInteger exportType = [userInfo[kExportTypeNotificationKey] integerValue];
    CCFCodeFormatter *formatter = [[CCFCodeFormatter alloc] initWithCodeType:codeType];
    
    _representedRegex.string = _regexTextView.text;
    _representedRegex.replacementString = _substitutionTextView.text;
    
    formatter.regex = _representedRegex;
    formatter.sampleText = _sourceTextView.text;
    
    NSString *code = formatter.codeString;
    CCFHTMLExport *htmlExport = [[CCFHTMLExport alloc] initWithCodeString:code];
    htmlExport.forEmail = (exportType == exportTypeEmail);
    NSString *body = [htmlExport htmlString];
    
    if( exportType == exportTypeEmail ) {
        MFMailComposeViewController *mvc = [[MFMailComposeViewController alloc] init];
        [mvc setMailComposeDelegate:self];
        mvc.modalPresentationStyle = UIModalPresentationFormSheet;
        [mvc setSubject:NSLocalizedString(@"Regex code sample", @"Subject for code email")];
        
        
        [mvc setMessageBody:body isHTML:YES];
        
        [self presentViewController:mvc animated:YES completion:^{ (void)0; }];
    }
    else if( exportType == exportTypePasteboard ) {
        NSDictionary *resourceInfo = @{ @"WebResourceData" : [body dataUsingEncoding:NSUTF8StringEncoding],
        @"WebResourceFrameName" : @"",
        @"WebResourceMIMEType" : @"text/html",
        @"WebResourceTextEncodingName" : @"UTF-8",
        @"WebResourceURL" : @"about:blank" };
        
        NSDictionary *htmlInfo = @{ (NSString *)kUTTypeText : code,
        @"Apple Web Archive pasteboard type" : @{ @"WebMainResource" : resourceInfo} };
        
        UIPasteboard *generalPasteboard = [UIPasteboard generalPasteboard];
        [generalPasteboard setItems:@[htmlInfo]];
    }
    else {
        NSLog(@"%s - WARNING - unhandled export type = %d",__FUNCTION__,exportType);
    }
}

- (void)presentExportOptionsAction:(id)sender {
    [[self exportOptionsPopover] presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - Accessors

//  we use Objective-C runtime associated object to hold a reference to the reusable popover for export
//  this is the accessor that provides access to and if necessary creation of that popover
- (UIPopoverController *)exportOptionsPopover {
    UIPopoverController *popover = (UIPopoverController *)objc_getAssociatedObject(self, &ExportPopoverKey);
    if( !popover ) {
        CCFExportLanguageViewController *languageController = [[CCFExportLanguageViewController alloc] init];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:languageController];
        popover = [[UIPopoverController alloc] initWithContentViewController:navController];
        //  provide reference to the enclosing popover primarily for content resizing purposes
        languageController.enclosingPopover = popover;
        //  keep a reference as associated object (because this is in a category)
        objc_setAssociatedObject(self, &ExportPopoverKey, popover, OBJC_ASSOCIATION_RETAIN);
    }
    return popover;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:^{
        (void)0;
    }];
}



@end
