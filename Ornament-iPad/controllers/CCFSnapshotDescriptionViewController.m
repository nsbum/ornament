/**
 *   @file CCFSnapshotDescriptionViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 14:41:20
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSnapshotDescriptionViewController.h"
#import "CCFSnapshot.h"
#import "CCFIOptionMarkerView.h"
#import "CCFBarButtonItem.h"

@interface CCFSnapshotDescriptionViewController ()

@end

@implementation CCFSnapshotDescriptionViewController {
    IBOutlet UITableViewCell *_descriptionCell;
    IBOutlet UITableViewCell *_regexCell;
    IBOutlet UITableViewCell *_optionsCell;
    IBOutlet UILabel *_regexLabel;
    IBOutlet UITextField *_descriptionTextView;
    
    IBOutlet CCFIOptionMarkerView *_caseInsensitiveMarkerView;
    IBOutlet CCFIOptionMarkerView *_verboseMarkerView;
    IBOutlet CCFIOptionMarkerView *_multilineMarkerView;
    IBOutlet CCFIOptionMarkerView *_singleLineMarkerView;
}

- (id)init {
    self = [super initWithNibName:@"CCFSnapshotDescriptionViewController" bundle:nil];
    if( !self ) return nil;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    CCFBarButtonItem *okItem = [[CCFBarButtonItem alloc] initWithTitle:_(@"OK") style:UIBarButtonItemStyleDone block:^(id sender) {
        self.snapshot.descriptor = _descriptionTextView.text;
        self.saveAction(self.snapshot);
    }];
    self.navigationItem.rightBarButtonItem = okItem;
    
    CCFBarButtonItem *cancelItem = [[CCFBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel block:^(id sender) {
        self.saveAction(nil);
    }];
    self.navigationItem.leftBarButtonItem = cancelItem;
    
    _descriptionTextView.delegate = self;
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return NSLocalizedString(@"Add description for your snapshot", @"Add description for snapshot header title");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    switch( indexPath.row ) {
        case 0: {
            cell = _regexCell;
            
            NSDictionary *codeAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.419 green:0.000 blue:0.033 alpha:1.000],
            NSFontAttributeName : [UIFont fontWithName:@"Menlo" size:16.0f] };
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:self.snapshot.regexString attributes:codeAttributes];
            _regexLabel.attributedText = attributedString;
            
            break;
        }
            
            
        case 1: {
            cell = _optionsCell;
            
            _caseInsensitiveMarkerView.state = (self.snapshot.regexOptions & NSRegularExpressionCaseInsensitive );
            _verboseMarkerView.state = (self.snapshot.regexOptions & NSRegularExpressionAllowCommentsAndWhitespace );
            _multilineMarkerView.state = (self.snapshot.regexOptions & NSRegularExpressionDotMatchesLineSeparators );
            _singleLineMarkerView.state = (self.snapshot.regexOptions & NSRegularExpressionAnchorsMatchLines );
            
            break;
        }
            
        case 2:
            cell = _descriptionCell;
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if( indexPath.row == 1 ) {
        return 87.0f;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

#pragma mark - Text view delegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.snapshot.descriptor = _descriptionTextView.text;
}


@end
