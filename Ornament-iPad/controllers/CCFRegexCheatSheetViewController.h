/**
 *   @file CCFRegexCheatSheetViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-12-15 23:19:15
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFRegexCheatSheetViewController : UIViewController

- (id)initWithNib;

@end
