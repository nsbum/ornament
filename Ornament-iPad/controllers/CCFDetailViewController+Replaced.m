/**
 *   @file CCFDetailViewController+Replaced.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-0904:17:16
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFDetailViewController+Replaced.h"
#import "CCFRegex.h"

@implementation CCFDetailViewController (Replaced)

#pragma mark - Public

- (void)clearReplacedText { 
    _replacedTextView.attributedText = [[NSAttributedString alloc] initWithString:@""];
}

- (void)performReplacement {
    NSString *replacementText = _substitutionTextView.text;
    
    if( replacementText.length == 0 ) return;
    
    //  NOTE:
    //  this is a bit of a hack, but other more elegant ways of dealing with did not work
    //  we are going to make one pass first to see if the highlight extends to the end of
    //  the string; because if it does, it fills the highlight all the way to the end of
    //  the line of text, instead of stopping at the end of where the attribute is.  In other
    //  words, it fills the entire line with the background color, from the point where the range
    //  actually is to the end of the line on the right.
    NSInteger lastCharIndex = _replacedTextView.text.length - 1;
    for( NSTextCheckingResult *match in _matches ) {
        NSRange range = match.range;
        if( NSMaxRange(range) >= lastCharIndex ) {
            //  add a space
            _replacedTextView.text = [[_replacedTextView text] stringByAppendingString:@" "];
        }
    }
    
    //  new add attributes for our ranges
    NSDictionary *basicAttributes = @{NSFontAttributeName: [UIFont fontWithName:@"Courier" size:18.0], NSForegroundColorAttributeName:[UIColor darkTextColor] };
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:_replacedTextView.text attributes:basicAttributes];
    UIColor *bgColor = [UIColor colorWithRed:0.741 green:0.000 blue:0.026 alpha:1.000];
    UIFont *boldFont = [UIFont fontWithName:@"Courier-Bold" size:18.0];
    for( NSTextCheckingResult *match in _matches ) {
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:bgColor range:match.range];
        [mutableAttributedString addAttribute:NSFontAttributeName value:boldFont range:match.range];
        [mutableAttributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:match.range];
    }
    
    NSMutableString *mutableString = [mutableAttributedString mutableString];
    //  do the replacements in this mutable string
    [[_representedRegex expression] replaceMatchesInString:mutableString
                                                   options:0
                                                     range:entireStringRange(mutableString)
                                              withTemplate:replacementText];
    
    _replacedTextView.attributedText = mutableAttributedString;
}

@end
