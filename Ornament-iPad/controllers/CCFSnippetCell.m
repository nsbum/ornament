/**
 *   @file CCFSnippetCell.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-07 16:06:39
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSnippetCell.h"

@implementation CCFSnippetCell

+ (CGFloat)cellHeight {
    return 80.0f;
}

- (void)setCodeString:(NSString *)string explanation:(NSString *)explanation {
    NSMutableAttributedString *attributedCodeString = [[NSMutableAttributedString alloc] initWithString:string];
    UIFont *fixedFont = [UIFont fontWithName:@"Menlo" size:18.0];
    
    NSRange range = NSMakeRange(0, attributedCodeString.length);
    [attributedCodeString addAttribute:NSFontAttributeName value:fixedFont range:range];
    [attributedCodeString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.419 green:0.000 blue:0.033 alpha:1.000] range:range];
    self.codeLabel.attributedText = attributedCodeString;
    
    self.descriptionLabel.text = explanation;
}

@end
