/**
 *   @file CCFCharClassKeyboardPopoverViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 08:27:22
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSmallAccessoryKeyDelegate.h"

@protocol CCFCharClassKeyboardPopoverControllerKeyPressDelegate;

/** A popover controller that manages a miniature keyboard.
 
 This UIPopoverController subclass manages a popover that presents selected
 character class options to the user after the user presses the '\' key.
 */
@interface CCFCharClassKeyboardPopoverViewController : UIViewController <CCFSmallAccessoryKeyDelegate>

/** The delegate that receives message of the CCFCharClassKeyboardPopoverControllerKeyPressDelegate protocol
 */
@property (nonatomic, weak) id <CCFCharClassKeyboardPopoverControllerKeyPressDelegate> keyPressDelegate;

@end
