/**
 *   @file CCFSnippetCell.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-07 16:06:32
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

/** A custom table view cell that presents a snippet
 */
@interface CCFSnippetCell : UITableViewCell

/** Interface outlet to the codeLabel
 
 The code label displays the regex string that will be inserted
 */
@property (nonatomic, weak) IBOutlet UILabel *codeLabel;

/** Interface outlet to the descriptionLabel
 
 The description label displays the descriptive text about the snippet
 */
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;

/** @name Class methods
 */

/** Returns the cell height
 
 @return The height of the cell in pixels as CGFloat.
 */
+ (CGFloat)cellHeight;

/** @name Configuration
 
 Configuring the content and appearance of the cell
 */

/** Sets the code string and explanation 
 
 @param string The regex code string for the snippet
 @param explanation The text for the descriptionLabel
 */
- (void)setCodeString:(NSString *)string explanation:(NSString *)explanation;

@end
