/**
 *   @file CCFSnippetCategoryViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-07 14:44:09
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSnippetCategoryViewController.h"
#import "CCFSnippetCategory.h"
#import "CCFSnippetImporter.h"
#import "CCFSnippetViewController.h"

static NSMutableArray *SnippetCategories;

@implementation CCFSnippetCategoryViewController

+ (void)initialize {
    if( self == [CCFSnippetCategoryViewController class] ) {
        SnippetCategories = [[NSMutableArray alloc] init];
        CCFSnippetImporter *importer = [[CCFSnippetImporter alloc] initWithCompletionBlock:^(NSArray *snippetCategories) {
            [SnippetCategories addObjectsFromArray:snippetCategories];
        }];
        [importer import];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Snippets", @"Snippets");
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [SnippetCategories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
	static NSString *cellIdentifier = @"cellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if( !cell )
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    cell.textLabel.text = [SnippetCategories[indexPath.row] title];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CCFSnippetViewController *vc = [[CCFSnippetViewController alloc] initWithStyle:UITableViewStylePlain];
    vc.snippets = [SnippetCategories[indexPath.row] snippets];
    vc.title = [SnippetCategories[indexPath.row] title];
    [[self navigationController] pushViewController:vc animated:YES];
}


@end
