//
//  CCFExportOptionsViewController.h
//  Ornament
//
//  Created by alanduncan on 12/28/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCFExportOptionsViewController : UIViewController

/** The popover that is enclosing the instance of this class.
 */
@property (nonatomic, assign) UIPopoverController *enclosingPopover;

- (id)initWithNib;

@end
