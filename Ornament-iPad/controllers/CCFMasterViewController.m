/**
 *   @file CCFMasterViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 14:39:42
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFMasterViewController.h"

//  model
#import "CCFSnapshot.h"
//  helper
#import "CCFSnapshotSeeder.h"
//  controller
#import "CCFDetailViewController.h"
#import "CCFSnapshotDescriptionViewController.h"
//  view
#import "CCFSnapshotCell.h"

@interface CCFMasterViewController () {
    NSMutableArray *_objects;
    UINib *_cellLoader;
}
@end

@implementation CCFMasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Snapshots", @"Snapshots");
        self.clearsSelectionOnViewWillAppear = NO;
        self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    }

    //  don't load snapshots in screenshot mode
#if !SCREEN_SHOT_MODE
    if( ![[NSUserDefaults standardUserDefaults] boolForKey:kDidSeedSnapshots] ) {
        [self seedSnapshotsInBackground];
    }
    else {
        [self loadSnapshots];
    }
#endif
    
    __block id appBackgrounding = nil;
    appBackgrounding = [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidEnterBackgroundNotification
                                                                         object:nil
                                                                          queue:nil
                                                                     usingBlock:^(NSNotification *note) {
                                                                         [self saveSnapshots];
                                                                     }];
    __block id appForegrounding = nil;
    appForegrounding = [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillEnterForegroundNotification
                                                                         object:nil
                                                                          queue:nil
                                                                     usingBlock:^(NSNotification *note) {
                                                                         [self loadSnapshots];
                                                                     }];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    [[[self navigationController] view] setNeedsDisplay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    //  we can reload these
    _cellLoader = nil;
    _objects = nil;
}

#pragma mark - Public

- (void)saveSnapshots {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *fileError = nil;

    if( ![fm fileExistsAtPath:[[self urlForPrivateDirectory] path]] ) {
        if( ![fm createDirectoryAtPath:[[self urlForPrivateDirectory] path] withIntermediateDirectories:YES attributes:nil error:&fileError] ) {
            NSLog(@"%s - Error saving snapshots: %@ | %@",__FUNCTION__,fileError,[fileError userInfo]);
            return;
        }
        else {
            if( ![fm fileExistsAtPath:[[self urlForSnapshots] path]] ) {
                if( ![fm createFileAtPath:[[self urlForSnapshots] path] contents:nil attributes:nil] ) {
                    NSLog(@"%s - Error saving snapshots: %@ | %@",__FUNCTION__,fileError,[fileError userInfo]);
                    return;
                }
            }
        }
    }
    NSData *objectsData = [NSKeyedArchiver archivedDataWithRootObject:[self objects]];
    [objectsData writeToURL:[self urlForSnapshots] atomically:YES];

}

- (void)loadSnapshots {
    NSData *snapshotData = [NSData dataWithContentsOfURL:[self urlForSnapshots]];
    if( snapshotData ) {
        _objects = [(NSArray *)[NSKeyedUnarchiver unarchiveObjectWithData:snapshotData] mutableCopy];
    }
}

- (NSMutableArray *)objects {
    if( !_objects ) {
#if !SCREEN_SHOT_MODE
        //  try to load from disk first
        [self loadSnapshots];
#endif
        //  if that fails, then give empty array
        if( !_objects ) {
            _objects = [[NSMutableArray alloc] init];
        }
    }
    return _objects;
}

- (void)insertNewObject:(id)sender
{
    CCFSnapshotDescriptionViewController *vc = [[CCFSnapshotDescriptionViewController alloc] init];
    vc.snapshot = [[self detailViewController] snapshot];
    vc.saveAction = ^(CCFSnapshot *snapshot){
        [self dismissViewControllerAnimated:YES completion:^{
            if( snapshot ) {
                if( !snapshot.descriptor ) {
                    snapshot.descriptor = NSLocalizedString(@"New snapshot", @"New snapshot default title");
                }
                [[self objects] insertObject:snapshot atIndex:0];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }];
    };
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    vc.title = _(@"New Snapshot");
    [self presentViewController:navController animated:YES completion:^{
        (void)0;
    }];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self objects] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    //  dequeue a cell or use _cellLoader to create one if we can't dequeue
    CCFSnapshotCell *cell = (CCFSnapshotCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if( !cell ) {
        NSArray *topLevelObjects = [[self cellLoader] instantiateWithOwner:self options:nil];
        if( topLevelObjects && topLevelObjects.count != 0 ) {
            cell = topLevelObjects[0];
        }
    }
    CCFSnapshot *snapshot = [self objects][indexPath.row];
    [cell setCodeString:snapshot.regexString sampleText:snapshot.sampleText];
    [cell setOptions:snapshot.regexOptions];
    [cell setDescriptionText:snapshot.descriptor];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if( indexPath.row < [[self objects] count] ) {
            [[self objects] removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        (void)0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [CCFSnapshotCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CCFSnapshot *object = [self objects][indexPath.row];
    self.detailViewController.detailItem = object;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    });
}

#pragma mark - Accessors

- (UINib *)cellLoader {
    if( !_cellLoader ) {
        _cellLoader = [UINib nibWithNibName:@"CCFSnapshotCell" bundle:[NSBundle bundleForClass:[self class]]];
    }
    return _cellLoader;
}

#pragma mark - Private

- (NSURL *)urlForDocumentsDirectory {
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentationDirectory inDomains:NSUserDomainMask] lastObject];
    return url;
}

- (NSURL *)urlForPrivateDirectory {
    return [[self urlForDocumentsDirectory] URLByAppendingPathComponent:@".private"];
}

- (NSURL *)urlForSnapshots {
    return [[self urlForPrivateDirectory] URLByAppendingPathComponent:@"snapshots.regexdata"];
}

- (void)seedSnapshotsInBackground {
    dispatch_queue_t snapshotSeederQueue = dispatch_queue_create("com.cocoafactory.SnapshotSeeder", 0);
    dispatch_async(snapshotSeederQueue, ^{
        __block CCFSnapshotSeeder *seeder = [[CCFSnapshotSeeder alloc] init];
        [seeder importWithBlock:^(CCFSnapshot *snapshot) {
            if( snapshot ) {
                [[self objects] addObject:snapshot];
            }
            else {
                //  if snapshot is nil, that means we reached the end
                seeder = nil;
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kDidSeedSnapshots];
                //  save in background just in case we get suddenly terminated
                [self saveSnapshots];
            }
        }];
    });
}


@end
