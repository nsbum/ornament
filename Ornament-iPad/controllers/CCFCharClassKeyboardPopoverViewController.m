/**
 *   @file CCFCharClassKeyboardPopoverViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 08:27:29
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFCharClassKeyboardPopoverViewController.h"
#import "CCFCharClassKeyboardPopoverControllerKeyPressDelegate.h"
#import <QuartzCore/QuartzCore.h>

#import "CCFSmallAccessoryKeyView.h"
#import "CCFSmallAccessoryKeyDelegate.h"

@interface CCFCharClassKeyboardPopoverViewController ()

@end

@implementation CCFCharClassKeyboardPopoverViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    for( UIView *subview in self.view.subviews ) {
        if( [subview isKindOfClass:[CCFSmallAccessoryKeyView class]] ) {
            CCFSmallAccessoryKeyView *keyView = (CCFSmallAccessoryKeyView *)subview;
            keyView.delegate = self;
        }
    }
}

#pragma mark - CCFSmallAccessoryKeyDelegate

- (void)smallAccessoryKeyViewDidReceiveTap:(CCFSmallAccessoryKeyView *)keyView {
    [[self keyPressDelegate] charClassKeyboardPopoverControllerDidReceiveSequence:keyView.keyLabel];
}


@end
