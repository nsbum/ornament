/**
 *   @file CCFDetailViewController+Regex.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-08 05:40:05
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFDetailViewController+Regex.h"
//  frameworks
#import <objc/runtime.h>
//  model
#import "CCFRegex.h"
#import "CCFRegexOptions.h"
#import "CCFSnippet.h"
//  helper
#import "CCFRegexHighlighter.h"
//  controller
#import "CCFRegexOptionsTableViewController.h"
#import "CCFSnippetCategoryViewController.h"
#import "CCFRegexCheatSheetViewController.h"
//  view
#import "CCFRegexKeyboardAccessoryView.h"
#import "CCFAccessoryKeyView.h"

static char RegexHighlighterKey;
static char RegexOptionsPopoverKey;
static char SnippetsPopoverKey;
static char AccessoryViewKey;
static char RegexCheatSheetKey;

@implementation CCFDetailViewController (Regex)

#pragma mark - Object lifecycle

- (void)deleteAssociatedObjectsForRegex {
    objc_setAssociatedObject(self, &RegexHighlighterKey, nil, OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, &RegexOptionsPopoverKey, nil, OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, &SnippetsPopoverKey, nil, OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, &RegexCheatSheetKey, nil, OBJC_ASSOCIATION_RETAIN);
}

#pragma mark - Public

- (void)setSnapshotRegexString:(NSString *)regexString {
    //  apply attributes
    NSDictionary *attributes = @{   NSForegroundColorAttributeName:[UIColor darkTextColor],
                                    NSFontAttributeName:[UIFont fontWithName:@"Courier-Bold" size:24.0]};
    NSAttributedString *attributedRegexString = [[NSAttributedString alloc] initWithString:regexString attributes:attributes];
    _regexTextView.attributedText = attributedRegexString;
    
    //  note change in regex to validate, apply syntax highlighting etc.
    [self regexTextDidChange];
}

- (void)regexViewDidLoad {
    _regexTextView.inputAccessoryView = [self accessoryKeyboardView];
}

- (void)regexTextDidChange {
    [self highlightSyntax];
    [self validSyntaxInBackground];
}

- (void)insertSnippetWithNotification:(NSNotification *)note {
    [[self snippetPopoverController] dismissPopoverAnimated:YES];
    
    if( _regexTextView.text.length == 0 ) {
        _regexTextView.text = @"";
    }
    
    CCFSnippet *snippet = note.userInfo[kSnippetKey];
    NSRange insertRange = _regexTextView.selectedRange;
    
    //[_regexTextView becomeFirstResponder];
    
    //  correct the insertion location is NSNotFound
    if( insertRange.location == NSNotFound ) {
        insertRange.location = 0;
        _regexTextView.selectedRange = insertRange;
    }
    
    NSInteger cursorOffset = 0;
    NSMutableString *mutableSnip = [[snippet snip] mutableCopy];
    //  offset to the cursor mark
    NSRange cursorRange = [mutableSnip rangeOfString:@"ß"];
    if( cursorRange.location != NSNotFound ) {
        cursorOffset = cursorRange.location;
        //  remove the marker now that we measured the location
        [mutableSnip deleteCharactersInRange:cursorRange];
    }
    
    [_regexTextView insertText:mutableSnip];
    if( cursorOffset != 0 ) {
        insertRange.location = insertRange.location + cursorOffset;
        _regexTextView.selectedRange = insertRange;
    }
    
    [self regexTextDidChange];
}

- (void)adjustKeyboardAccessoryViewForOrientationNotification:(NSNotification *)note {
    UIInterfaceOrientation orientation = [note.userInfo[UIApplicationStatusBarOrientationUserInfoKey] integerValue];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGRect frame = [[self accessoryKeyboardView] frame];
    CGFloat widthDimension = (UIInterfaceOrientationIsLandscape(orientation)) ? bounds.size.height : bounds.size.width;
    [[self accessoryKeyboardView] setFrame:CGRectMake(frame.origin.x, frame.origin.y, widthDimension, frame.size.height)];
}

- (void)adjustKeyboardAccessoryViewForKeyboardAppearanceNotification:(NSNotification *)note {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGRect frame = [[self accessoryKeyboardView] frame];
    CGFloat widthDimension = (UIInterfaceOrientationIsLandscape(orientation)) ? bounds.size.height : bounds.size.width;
    [[self accessoryKeyboardView] setFrame:CGRectMake(frame.origin.x, frame.origin.y, widthDimension, frame.size.height)];
}

#pragma mark - Private

- (void)highlightSyntax {
    NSRange savedRange = _regexTextView.selectedRange;
    NSAttributedString *sourceString = _regexTextView.attributedText;
    [[self regexHighlighter] setSourceAttributedString:sourceString];
    _regexTextView.attributedText = [[self regexHighlighter] highlightedString];
    _regexTextView.selectedRange = savedRange;
}

- (void)validSyntaxInBackground {
    NSString *currentText = _regexTextView.text;
    dispatch_queue_t backgroundQueue = dispatch_queue_create("com.cocoafactory.SyntaxChecking", 0);
    dispatch_async(backgroundQueue, ^{
        _representedRegex.string = currentText;
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^{
            BOOL regexIsValid = [_representedRegex regexIsValid];
            [self setRegexBarTitleState:regexIsValid];
            [self performRegexAction];

        });
    });
}

- (void)setRegexBarTitleState:(BOOL)valid {
    UIColor *titleColor = (valid) ? [UIColor darkTextColor]:[UIColor colorWithRed:0.741 green:0.000 blue:0.026 alpha:1.000];
    _regexBarTitle.textColor = titleColor;
}

#pragma mark - Accessors

- (CCFRegexHighlighter *)regexHighlighter {
    CCFRegexHighlighter *highlighter = (CCFRegexHighlighter *)objc_getAssociatedObject(self, &RegexHighlighterKey);
    if( !highlighter ) {
        highlighter = [[CCFRegexHighlighter alloc] init];
        objc_setAssociatedObject(self, &RegexHighlighterKey, highlighter, OBJC_ASSOCIATION_RETAIN);
    }
    return highlighter;
}

- (UIPopoverController *)optionsPopover {
    UIPopoverController *popover = (UIPopoverController *)objc_getAssociatedObject(self, &RegexOptionsPopoverKey);
    if( !popover ) {
        CCFRegexOptionsTableViewController *vc = [[CCFRegexOptionsTableViewController alloc] initWithStyle:UITableViewStylePlain];
        CCFRegexOptions *options = _representedRegex.options;
        if( !options ) {
            vc.options = 0;
        }
        else {
            vc.options = _representedRegex.options.regexOptions;
        }
        vc.contentSizeForViewInPopover = CGSizeMake(280, 320);
        
        popover = [[UIPopoverController alloc] initWithContentViewController:vc];
        popover.delegate = self;
        
        objc_setAssociatedObject(self, &RegexOptionsPopoverKey, popover, OBJC_ASSOCIATION_RETAIN);
    }
    
    return popover;
}

- (UIPopoverController *)snippetPopoverController {
    UIPopoverController *popover = (UIPopoverController *)objc_getAssociatedObject(self, &SnippetsPopoverKey);
    if( !popover ) {
        CCFSnippetCategoryViewController *vc = [[CCFSnippetCategoryViewController alloc] initWithStyle:UITableViewStylePlain];
        vc.contentSizeForViewInPopover = CGSizeMake(280, 320);
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
        popover = [[UIPopoverController alloc] initWithContentViewController:navController];
        
        objc_setAssociatedObject(self, &SnippetsPopoverKey, popover, OBJC_ASSOCIATION_RETAIN);
        
    }
    
    return popover;
}

- (UIPopoverController *)regexCheatSheetPopoverController {
    UIPopoverController *popover = (UIPopoverController *)objc_getAssociatedObject(self, &RegexCheatSheetKey);
    if( !popover ) {
        CCFRegexCheatSheetViewController *vc = [[CCFRegexCheatSheetViewController alloc] initWithNib];
        vc.contentSizeForViewInPopover = CGSizeMake(340, 380);
        popover = [[UIPopoverController alloc] initWithContentViewController:vc];
        
        objc_setAssociatedObject(self, &RegexCheatSheetKey, popover, OBJC_ASSOCIATION_RETAIN);
    }
    return  popover;
}

- (CCFRegexKeyboardAccessoryView *)accessoryKeyboardView {
    CCFRegexKeyboardAccessoryView *view = (CCFRegexKeyboardAccessoryView *)objc_getAssociatedObject(self, &AccessoryViewKey);
    if( !view ) {
        NSBundle *classBundle = [NSBundle bundleForClass:[self class]];
        NSArray *topLevelObjects = [classBundle loadNibNamed:@"CCFRegexKeyboardAccessoryView" owner:self options:nil];
        if( topLevelObjects && topLevelObjects.count != 0 ) {
            if( [topLevelObjects[0] isKindOfClass:[CCFRegexKeyboardAccessoryView class]] ) {
                view = (CCFRegexKeyboardAccessoryView *)topLevelObjects[0];
            }
        }
        for(UIView *subview in view.subviews) {
            if( [subview isKindOfClass:[CCFAccessoryKeyView class]] ) {
                CCFAccessoryKeyView *keyView = (CCFAccessoryKeyView *)subview;
                keyView.tapBlock = ^(NSString *keyChar){
                    [_regexTextView insertText:keyChar];
                    [self regexTextDidChange];
                };
            }
        }
        
        objc_setAssociatedObject(self, &AccessoryViewKey, view, OBJC_ASSOCIATION_RETAIN);
    }
    return view;
}

#pragma mark - Interface actions

- (IBAction)optionsAction:(id)sender {
    CCFRegexOptionsTableViewController *optionsViewController = (CCFRegexOptionsTableViewController *)[[self optionsPopover] contentViewController];
    optionsViewController.options = _representedRegex.options.regexOptions;
    [[self optionsPopover] presentPopoverFromRect:[sender frame] inView:_regexContainerView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)snippetButtonAction:(id)sender {
    [[self snippetPopoverController] presentPopoverFromRect:[sender frame] inView:_regexContainerView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)cheatSheetAction:(id)sender {
    [[self regexCheatSheetPopoverController] presentPopoverFromRect:[sender frame]
                                                             inView:_regexContainerView
                                           permittedArrowDirections:UIPopoverArrowDirectionAny
                                                           animated:YES];
}

#pragma mark - UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    if( [popoverController isEqual:[self optionsPopover]] ) {
        CCFRegexOptionsTableViewController *vc = (CCFRegexOptionsTableViewController *)popoverController.contentViewController;
        CCFRegexOptions *optionsObj = [CCFRegexOptions optionsObjectWithOptions:[vc options]];
        _representedRegex.options = optionsObj;
        
        [self regexTextDidChange];
    }
}

@end
