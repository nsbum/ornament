/**
 *   @file CCFExportLanguageViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 16:13:13
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFExportLanguageViewController.h"
#import "CCFCodeTypes.h"
#import "CCFExportContentSizes.h"

#import "CCFExportTypeViewController.h"
#import "CCFExportOptionsViewController.h"

@interface CCFExportLanguageViewController ()

@end

@implementation CCFExportLanguageViewController {
    NSArray *_titles;
}

- (id)init {
    self = [super initWithNibName:NSStringFromClass([self class]) bundle:[NSBundle bundleForClass:[self class]]];
    if( !self ) return nil;
    
    _titles = @[@"Objective-C",@"Perl",@"PHP",@"Python",@"Ruby",@"Java",@"JavaScript",@"Options"];
    
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Language", @"Title of language choice controller in export popover");
}

- (CGSize)contentSizeForViewInPopover {
    return CGSizeMake(320, LANGUAGE_CONTROLLER_HEIGHT);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_titles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [_titles objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if( indexPath.row == 7 ) {
        cell.imageView.image = [UIImage imageNamed:@"19-gear.png"];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == 7 ) {
        CCFExportOptionsViewController *vc = [[CCFExportOptionsViewController alloc] initWithNib];
        vc.enclosingPopover = self.enclosingPopover;
        [[self navigationController] pushViewController:vc animated:NO];
    }
    else {
        CCFExportTypeViewController *vc = [[CCFExportTypeViewController alloc] initWithStyle:UITableViewStylePlain];
        vc.codeType = (CCFCodeType)indexPath.row;
        vc.enclosingPopover = self.enclosingPopover;
        [[self navigationController] pushViewController:vc animated:NO];
    }
    
    
}

@end
