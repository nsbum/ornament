/**
 *   @file CCFSnippetViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-07 15:41:17
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

/** A table view subclass that presents snippets of a given category
 */
@interface CCFSnippetViewController : UITableViewController

/** The snippets to present to the user
 */
@property (nonatomic, strong) NSArray *snippets;

@end
