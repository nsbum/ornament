/**
 *   @file CCFDetailViewController+Source.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 04:34:17
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFDetailViewController.h"

/** A category on CCFDetailViewController that manages the source text subview
 */
@interface CCFDetailViewController (Source)

/** @name Appearance
 */

/** Clears the source text view
 */
- (void)clearSourceText;

/** Sets the content of the view
 
 @param sampleString The text as NSString to place in the source text view
 */
- (void)setSnapshotSampleString:(NSString *)sampleString;

/** Perform source text highlighting
 
 This message is sent to instances of the class whenever highlighting should be
 performed for matches.
 */
- (void)performHightlighting;

/** Remove highlighting
 
 This message is sent to instances of the class whenever sample text (source text)
 highlighting should be removed.
 
 */
- (void)removeSourceHighlighting;

@end
