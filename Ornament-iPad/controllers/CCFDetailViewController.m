/**
 *   @file CCFDetailViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-07 21:06:36
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */
#import "CCFDetailViewController.h"
#import "CCFDetailViewController+Regex.h"
#import "CCFDetailViewController+Substitution.h"
#import "CCFDetailViewController+Source.h"
#import "CCFDetailViewController+Replaced.h"
#import "CCFDetailViewController+Export.h"
//  frameworks
#import <QuartzCore/QuartzCore.h>
//  models
#import "CCFRegex.h"
#import "CCFSnapshot.h"
//  views
#import "CCFBarButtonItem.h"

typedef void (^BasicBlock)(void);

@interface CCFDetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (nonatomic, assign) BOOL isModifyingSourceText;
- (void)configureView;
@end

@implementation CCFDetailViewController {
    UITextView *_firstResponder;
    CGFloat _distanceAnimatedForKeyboard;
}

void WhileChangingSourceText(CCFDetailViewController *self, BasicBlock block) {
    self.isModifyingSourceText = YES;
    block();
    self.isModifyingSourceText = NO;
}


#pragma mark - Object lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Regex", @"Regex");
    }
    
    __block id selectedSnippet = nil;
    selectedSnippet = [[NSNotificationCenter defaultCenter] addObserverForName:kDidSelectSnippetNotification
                                                                        object:nil
                                                                         queue:nil
                                                                    usingBlock:^(NSNotification *note) {
                                                                        [self insertSnippetWithNotification:note];
                                                                    }];
    
    __block id orientationChanged = nil;
    orientationChanged = [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidChangeStatusBarOrientationNotification
                                                                           object:nil
                                                                            queue:nil
                                                                       usingBlock:^(NSNotification *note) {
                                                                           [self adjustKeyboardAccessoryViewForOrientationNotification:note];
                                                                       }];
    
    __block id keyboardWillAppear = nil;
    keyboardWillAppear = [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification
                                                                           object:nil
                                                                            queue:nil
                                                                       usingBlock:^(NSNotification *note) {
                                                                           [self adjustKeyboardAccessoryViewForKeyboardAppearanceNotification:note];
                                                                       }];
    
    __block id exportRequested = nil;
    exportRequested = [[NSNotificationCenter defaultCenter] addObserverForName:kDidRequestExportNotification
                                                                        object:nil
                                                                         queue:nil
                                                                    usingBlock:^(NSNotification *note) {
                                                                        [self handleExportRequestWithNotification:note];
                                                                    }];
    
    __block id memoryWarning = nil;
    memoryWarning = [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification
                                                                      object:nil
                                                                       queue:nil
                                                                  usingBlock:^(NSNotification *note) {
                                                                      [self didReceiveMemoryWarning];
                                                                  }];
    
    [self addObserver:self forKeyPath:@"detailItem" options:NSKeyValueObservingOptionNew context:NULL];
    
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"detailItem"];
    
    [self deleteAssociatedObjectsForExport];
    [self deleteAssociatedObjectsForRegex];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if( [keyPath isEqualToString:@"detailItem"] ) {
        //  clear the substitution
        _substitutionTextView.text = @"";
        
        NSString *regexString = [[self detailItem] regexString];
        [self setSnapshotRegexString:regexString];
        
        NSRegularExpressionOptions opts = [[self detailItem] regexOptions];
        _representedRegex.options = [CCFRegexOptions optionsObjectWithOptions:opts];
        
        //  add our sample text
        NSString *sampleString = [[self detailItem] sampleText];
        [self setSnapshotSampleString:sampleString];
        
        //  copy to replaced
        [self copyAndReplaceSource];
    }
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self regexViewDidLoad];
    [self clearSubstitutionText];
    [self clearSourceText];
    [self clearReplacedText];
    
    _regexTextView.attributedText = [[NSAttributedString alloc] initWithString:@""];
	
    _representedRegex = [[CCFRegex alloc] init];
    _representedRegex.isMatching = YES;
    
    
    CCFBarButtonItem *actionItem = [[CCFBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction block:^(id sender) {
        [self presentExportOptionsAction:sender];
    }];
    self.navigationItem.rightBarButtonItem = actionItem;
    
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Public

- (void)performRegexAction {
    _matches = [_representedRegex matchesWithSourceText:_sourceTextView.text];
    if( _matches && _matches.count != 0) {
        WhileChangingSourceText(self, ^{
            [self performHightlighting];
        });
        [self copyAndReplaceSource];
    }
    else {
        WhileChangingSourceText(self, ^{
            [self removeSourceHighlighting];
        });
        //  copy source text over to replaced text
        NSString *sourceString = _sourceTextView.text;
        //  We should probably make the text attributes centralized somehow
        NSDictionary *attributes = @{NSFontAttributeName : [UIFont fontWithName:@"Courier" size:18.0f]};
        _replacedTextView.attributedText = [[NSAttributedString alloc] initWithString:sourceString attributes:attributes];
    }
}

- (CCFSnapshot *)snapshot {
    CCFSnapshot *snapshot = [[CCFSnapshot alloc] init];
    snapshot.regexString = _regexTextView.text;
    snapshot.regexOptions = _representedRegex.options.regexOptions;
    snapshot.sampleText = _sourceTextView.text;
    snapshot.replacementText = _substitutionTextView.text;
    
    return snapshot;
}
							
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Snapshots", @"Snapshots");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self animateTextField:textView up:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self animateTextField:textView up:NO];
}


- (void)textViewDidChange:(UITextView *)textView {
    if( [textView isEqual:_regexTextView] ) {
        [self regexTextDidChange];
    }
    else if( [textView isEqual:_sourceTextView] ) {
        if( _isModifyingSourceText ) return;
        [self performRegexAction];
        
        [self copyAndReplaceSource];
    }
    else if( [textView isEqual:_substitutionTextView] ) {
        [self copyAndReplaceSource];
    }
}

- (void) animateTextField:(UITextView *) textField up: (BOOL) up
{
    CGPoint temp = [textField.superview convertPoint:textField.frame.origin toView:nil];
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if( orientation == UIInterfaceOrientationPortrait ){
        
        if( up ) {
            int moveUpValue = temp.y+textField.frame.size.height;
            _distanceAnimatedForKeyboard = 264-(1024-moveUpValue-5);
        }
    }
    else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
        if( up ) {
            int moveUpValue = 1004-temp.y+textField.frame.size.height;
            _distanceAnimatedForKeyboard = 264-(1004-moveUpValue-5);
        }
    }
    else if(orientation == UIInterfaceOrientationLandscapeLeft) {
        if( up ) {
            int moveUpValue = temp.x+textField.frame.size.height;
            _distanceAnimatedForKeyboard = 352-(768-moveUpValue-5);
        }
    }
    else
    {
        if( up ) {
            int moveUpValue = 768-temp.x+textField.frame.size.height;
            _distanceAnimatedForKeyboard = 352-(768-moveUpValue-5);
        }
        
    }
    if( _distanceAnimatedForKeyboard > 0 )
    {
        const int movementDistance = _distanceAnimatedForKeyboard;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView animateWithDuration:0.3 animations:^{
            if( orientation == UIInterfaceOrientationPortrait ) {
                self.view.frame = CGRectOffset(self.view.frame, 0, movement);
            }
            else if( orientation == UIInterfaceOrientationPortraitUpsideDown ) {
                
                self.view.frame = CGRectOffset(self.view.frame, 0, movement);
            }
            else if( orientation == UIInterfaceOrientationLandscapeLeft ) {
                
                self.view.frame = CGRectOffset(self.view.frame, 0, movement);
            }
            else {
                self.view.frame = CGRectOffset(self.view.frame, 0, movement);
            }
        }];
    }
}



#pragma mark - Interface actions

#pragma mark - Accessors

#pragma mark - Private

- (void)copyAndReplaceSource {
    //  copy source text over to replaced text
    NSString *sourceString = _sourceTextView.text;
    NSDictionary *replacedTextAttributes = @{NSFontAttributeName : [UIFont fontWithName:@"Courier" size:18.0]};
    _replacedTextView.attributedText = [[NSAttributedString alloc] initWithString:sourceString attributes:replacedTextAttributes];
    
    //  do any substitutions
    [self performReplacement];
}


@end
