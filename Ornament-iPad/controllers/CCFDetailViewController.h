/**
 *   @file CCFDetailViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-07 21:06:36
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFRegex, CCFSnapshot;

/** The top level view controller that manages the display of a single regular expression and its associated data.
 */
@interface CCFDetailViewController : UIViewController <UISplitViewControllerDelegate, UITextViewDelegate> {
    IBOutlet UITextView *_regexTextView;
    IBOutlet UILabel *_regexBarTitle;
    IBOutlet UIView *_regexContainerView;
    
    IBOutlet UITextView *_substitutionTextView;
    
    IBOutlet UITextView *_sourceTextView;
    
    IBOutlet UITextView *_replacedTextView;
    
    
    CCFRegex *_representedRegex;
    NSArray *_matches;
}

/** The represented snapshot item
 */
@property (strong, nonatomic) id detailItem;

/** Perfom all actions on content when regex changes
 */
- (void)performRegexAction;

/** Return the CCFSnapshot object represented by view's data
 */
- (CCFSnapshot *)snapshot;

@end
