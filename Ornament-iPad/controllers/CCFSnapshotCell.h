/**
 *   @file CCFSnapshotCell.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 14:31:17
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFSnapshotCell : UITableViewCell

/** @name Class methods
 */

/** The height of an instance of the cell in pixels.
 @return The pixel height of the cell.
 */
+ (CGFloat)cellHeight;

/** @name Setting cell properties
 
 Setters for cell properties
 */

/** Sets the code string and sample tet
 
 @param codeString An NSString that represents the regex string for a Snapshot
 @param sampleText An NSString that represents the sample text for a Snapshot
 */
- (void)setCodeString:(NSString *)codeString sampleText:(NSString *)sampleText;

/** Sets the regex options for cell
 
 @param options The NSRegularExpressionsOptions value for the cell.
 */
- (void)setOptions:(NSRegularExpressionOptions)options;

/** Sets the description text for the cell
 
 @param An NSString object that represents the user-provided descriptive label for the Snapshot
 */
- (void)setDescriptionText:(NSString *)descriptor;

@end
