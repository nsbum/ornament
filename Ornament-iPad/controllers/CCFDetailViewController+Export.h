/**
 *   @file CFDetailViewController+Export.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 16:23:14
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFDetailViewController.h"
#import <MessageUI/MessageUI.h>

/** A category on CCFDetailViewController that provides code export functionality
 */
@interface CCFDetailViewController (Export) <MFMailComposeViewControllerDelegate>

/** Presents the export options popover on user request
 
 @param sender The object that triggered the message on the user's behalf.
 */
- (void)presentExportOptionsAction:(id)sender;

/** Handles the request to export code
 
 @param note The NSNotification object that contains the data of interest for exporting code.  This notification should be a kDidRequestExportNotification type notification
                whose userInfo dictionary contains the following keys: kExportLanguageNotificationKey and kExportTypeNotificationKey. 
*/
- (void)handleExportRequestWithNotification:(NSNotification *)note;

/** Delete any associated objects.
 
 This category users associated objects available via the Objective-C runtime.  These objects must be deleted when instances of the class are deallocated.
 */
- (void)deleteAssociatedObjectsForExport;

@end
