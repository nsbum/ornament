/**
 *   @file CCFSnippetViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-07 15:41:24
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSnippetViewController.h"
#import "CCFSnippet.h"
#import "CCFSnippetCell.h"

static NSString * const SnippetCellNib = @"CCFSnippetCell";

@implementation CCFSnippetViewController {
    UINib *_cellLoader;
}

#pragma mark - Object lifecycle

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if( !self ) return nil;
    
    self.contentSizeForViewInPopover = CGSizeMake(280, 320);
    
    _cellLoader = [UINib nibWithNibName:SnippetCellNib bundle:[NSBundle bundleForClass:[self class]]];
    
    return self;
}

#pragma mark - View lifecycle

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return self.snippets.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
	static NSString *cellIdentifier = @"cellID";
    
    CCFSnippetCell *cell = (CCFSnippetCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if( !cell ) {
        NSArray *topLevelObjects = [_cellLoader instantiateWithOwner:self options:nil];
        if( topLevelObjects && topLevelObjects.count != 0 ) {
            cell = topLevelObjects[0];
        }
    }
    CCFSnippet *snippet = [self snippets][indexPath.row];
    [cell setCodeString:snippet.displayString explanation:snippet.explanation];
    cell.codeLabel.text = [self.snippets[indexPath.row] displayString];
    cell.descriptionLabel.text = [self.snippets[indexPath.row] explanation];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [CCFSnippetCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CCFSnippet *snippet = self.snippets[indexPath.row];
    NSDictionary *userInfo = @{kSnippetKey : snippet};
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidSelectSnippetNotification object:self userInfo:userInfo];
}

@end
