/**
 *   @file CCFDetailViewController+Substitution.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 04:20:03
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFDetailViewController+Substitution.h"

@implementation CCFDetailViewController (Substitution)

- (void)clearSubstitutionText {
    _substitutionTextView.attributedText = [[NSAttributedString alloc] initWithString:@""];
}

@end
