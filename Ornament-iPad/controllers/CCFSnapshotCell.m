/**
 *   @file CCFSnapshotCell.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 14:31:26
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSnapshotCell.h"
#import "CCFIOptionMarkerView.h"

@implementation CCFSnapshotCell {
    IBOutlet UILabel *_descriptionLabel;
    IBOutlet UILabel *_codeLabel;
    IBOutlet UILabel *_sampleLabel;
    
    IBOutlet CCFIOptionMarkerView *_caseInsensitiveMarkerView;
    IBOutlet CCFIOptionMarkerView *_verboseMarkerView;
    IBOutlet CCFIOptionMarkerView *_singleLineMarkerView;
    IBOutlet CCFIOptionMarkerView *_multiLineMarkerView;
}

+ (CGFloat)cellHeight {
    return 109.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    UIColor *codeLabelColor = (selected)?[UIColor colorWithRed:0.631 green:0.992 blue:0.969 alpha:1.000]:[UIColor colorWithRed:0.419 green:0.000 blue:0.033 alpha:1.000];
    NSMutableAttributedString *mutableColorString = [[_codeLabel attributedText] mutableCopy];
    [mutableColorString addAttribute:NSForegroundColorAttributeName value:codeLabelColor range:entireStringRange(mutableColorString)];
    _codeLabel.attributedText = mutableColorString;
    
    UIColor *sampleLabelColor = (selected)?[UIColor lightTextColor]:[UIColor lightGrayColor];
    mutableColorString = [[_sampleLabel attributedText] mutableCopy];
    [mutableColorString addAttribute:NSForegroundColorAttributeName value:sampleLabelColor range:entireStringRange(mutableColorString)];
    _sampleLabel.attributedText = mutableColorString;
}

#pragma mark - Public

- (void)setCodeString:(NSString *)codeString sampleText:(NSString *)sampleText {
    _codeLabel.attributedText = [[NSAttributedString alloc] initWithString:codeString attributes:[self attributesForCodeLabel]];
    _sampleLabel.attributedText = [[NSAttributedString alloc] initWithString:sampleText attributes:[self attributesForSampleLabel]];
}

- (void)setOptions:(NSRegularExpressionOptions)options {
    _caseInsensitiveMarkerView.state = (options & NSRegularExpressionCaseInsensitive);
    _verboseMarkerView.state = (options & NSRegularExpressionAllowCommentsAndWhitespace);
    _multiLineMarkerView.state= (options & NSRegularExpressionAnchorsMatchLines);
    _singleLineMarkerView.state = (options & NSRegularExpressionDotMatchesLineSeparators);
}

- (void)setDescriptionText:(NSString *)descriptor {
    _descriptionLabel.text = descriptor;
}

#pragma mark - Private

- (NSDictionary *)attributesForCodeLabel {
    return @{NSFontAttributeName : [UIFont fontWithName:@"Menlo" size:16.0], NSForegroundColorAttributeName : [UIColor colorWithRed:0.419 green:0.000 blue:0.033 alpha:1.000] };
}

- (NSDictionary *)attributesForSampleLabel {
    return @{NSFontAttributeName : [UIFont systemFontOfSize:15.0f], NSForegroundColorAttributeName : [UIColor lightGrayColor] };
}

@end
