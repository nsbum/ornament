/**
 *   @file CCFSnapshotDescriptionViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 14:41:13
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFSnapshot;


typedef void(^CCFSaveSnapshotAction)(CCFSnapshot *snapshot);

/** A modal view controller that collects descriptive information from the user about a snapshot that is to be saved.
 */
@interface CCFSnapshotDescriptionViewController : UITableViewController <UITextFieldDelegate>

/** The CCFSnapshot object that is being saved by the user 
 */
@property (nonatomic, strong) CCFSnapshot *snapshot;

/** The CCFSaveSnapshotAction block that will be executed when the user taps the save button, or nil if the user taps the cancel button.
 */
@property (nonatomic, strong) CCFSaveSnapshotAction saveAction;

@end
