/**
 *   @file CCFExportTypeViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 16:17:18
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFCodeTypes.h"

/** A table view controller subclass that shows the options for exporting code.
 */
@interface CCFExportTypeViewController : UITableViewController

/** The code type that the user selected.
 */
@property (nonatomic, assign) CCFCodeType codeType;

/** The popover that is enclosing the instance of this class.
 */
@property (nonatomic, assign) UIPopoverController *enclosingPopover;

@end
