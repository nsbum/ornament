/**
 *   @file CCFExportTypeViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 16:17:25
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFExportTypeViewController.h"

@interface CCFExportTypeViewController ()

@end

@implementation CCFExportTypeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Export option", @"Title for export option view in popover");
    self.contentSizeForViewInPopover = CGSizeMake(320, 90);
    self.enclosingPopover.popoverContentSize = CGSizeMake(320, 90);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if( !cell ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if( indexPath.row == 0 ) {
        cell.imageView.image = [UIImage imageNamed:@"219-scissors.png"];
        cell.textLabel.text = NSLocalizedString(@"Copy", @"Export option row text for copy action");
    }
    else {
        cell.imageView.image = [UIImage imageNamed:@"18-envelope.png"];
        cell.textLabel.text = NSLocalizedString(@"Email", @"Export option row text for email action");
    }
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *userInfo = @{kExportTypeNotificationKey : [NSNumber numberWithInteger:indexPath.row], kExportLanguageNotificationKey:[NSNumber numberWithInteger:_codeType]};
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidRequestExportNotification object:self userInfo:userInfo];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    });
}

@end
