/**
 *   @file CCFSnippetCategoryViewController.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-07 14:44:02
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

/** A table view controller subclass that presents the categories of snippet types that can be selected 
 */
@interface CCFSnippetCategoryViewController : UITableViewController

@end
