/**
 *   @file CCFRegexOptionsTableViewController.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 04:32:04
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFRegexOptionsTableViewController.h"

static NSUInteger *RowOptions;

@interface CCFRegexOptionsTableViewController ()

@end

@implementation CCFRegexOptionsTableViewController {
    NSRegularExpressionOptions _options;
}

+ (void)initialize {
    if( self == [CCFRegexOptionsTableViewController class] ) {
        RowOptions = malloc(sizeof(NSUInteger));
        RowOptions[0] = NSRegularExpressionCaseInsensitive;
        RowOptions[1] = NSRegularExpressionAllowCommentsAndWhitespace;
        RowOptions[2] = NSRegularExpressionDotMatchesLineSeparators;
        RowOptions[3] = NSRegularExpressionAnchorsMatchLines;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if( !cell ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *title = nil;
    switch( indexPath.row ) {
        case 0: {
            title = NSLocalizedString(@"Case insensitive", @"Case insensitive options row title");
            break;
        }
        case 1: {
            title = NSLocalizedString(@"Verbose", @"Verbose options row title");
            break;
        }
        case 2: {
            title = NSLocalizedString(@"Single line mode", @"Single line mode options row title");
            break;
        }
        case 3: {
            title = NSLocalizedString(@"Multi-line mode", @"Multi-line mode options row title");
            break;
        }
    }
    cell.textLabel.text = title;
    cell.accessoryType = ( _options & RowOptions[indexPath.row] ) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSRegularExpressionOptions rowOptions = RowOptions[indexPath.row];
    
    if( cell.accessoryType == UITableViewCellAccessoryCheckmark ) {
        _options -= rowOptions;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        _options += rowOptions;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    });
}

#pragma mark - Accessors

- (NSRegularExpressionOptions)options {
    return _options;
}

- (void)setOptions:(NSRegularExpressionOptions)options {
    _options = options;
    [[self tableView] reloadData];
}

@end
