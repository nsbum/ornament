/**
 *   @file CCFSmallAccessoryKeyView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 08:22:29
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */


@protocol CCFSmallAccessoryKeyDelegate;

typedef void(^CCFSmallKeyTapAction)(NSString *keySequence);

@interface CCFSmallAccessoryKeyView : UIView

@property (nonatomic, assign) BOOL state;
@property (nonatomic, strong) NSString *keyLabel;
@property (nonatomic, strong) CCFSmallKeyTapAction tapBlock;
@property (nonatomic, weak) id <CCFSmallAccessoryKeyDelegate> delegate;

@end
