/**
 *   @file CCFIOptionMarkerView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 16:01:08
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFIOptionMarkerView.h"

@implementation CCFIOptionMarkerView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    
    [self addObserver:self forKeyPath:@"state" options:NSKeyValueObservingOptionNew context:NULL];
    
    self.backgroundColor = [UIColor clearColor];
    
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"state"];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if( [keyPath isEqualToString:@"state"] ) {
        [self setNeedsDisplay];
    }
}

- (void)drawRect:(CGRect)rect
{
    //// Color Declarations
    UIColor* color = [UIColor colorWithRed: 0.004 green: 0.602 blue: 0.692 alpha: 1];
    
    //// Rounded Rectangle Drawing
    UIBezierPath* roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(2, 2, 13, 13) cornerRadius: 2];
    [[UIColor whiteColor] setFill];
    [roundedRectanglePath fill];
    [[UIColor grayColor] setStroke];
    roundedRectanglePath.lineWidth = 1;
    [roundedRectanglePath stroke];
    
    
    if( self.state ) {
        //// Oval Drawing
        UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(4.5, 4.5, 8, 8)];
        [color setFill];
        [ovalPath fill];
    }
}
    


@end
