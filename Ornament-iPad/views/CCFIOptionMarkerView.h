/**
 *   @file CCFIOptionMarkerView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-09 16:01:01
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFIOptionMarkerView : UIView

@property (nonatomic, assign) BOOL state;

@end
