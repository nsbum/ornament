//
//  CCFRegexKeyboardAccessoryView.m
//  Ornament
//
//  Created by alanduncan on 11/9/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFRegexKeyboardAccessoryView.h"
#import <QuartzCore/QuartzCore.h>

static CCFRegexKeyboardAccessoryView *commonInit(CCFRegexKeyboardAccessoryView *self) {
    CAGradientLayer *layer = [CAGradientLayer layer];
    layer.frame = self.bounds;
    layer.colors = @[(id)[UIColor colorWithWhite:0.648 alpha:1.000].CGColor, (id)[UIColor colorWithRed:0.612 green:0.608 blue:0.651 alpha:1.000].CGColor];
    [[self layer] insertSublayer:layer atIndex:0];
    return self;
}

///---------------------------------------------------------------------------------------
///    Private methods and properties
///---------------------------------------------------------------------------------------
@interface CCFRegexKeyboardAccessoryView()

@property (nonatomic, strong) UIView *separatorHideView;

@end



@implementation CCFRegexKeyboardAccessoryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return commonInit(self);
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    
    return commonInit(self);
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, 0, CGRectGetWidth(rect));
    CGContextSetStrokeColorWithColor(context, [[UIColor darkGrayColor] CGColor] );
    CGContextStrokePath(context);
    
    CGContextMoveToPoint(context, 0, 1);
    CGContextAddLineToPoint(context, 0, CGRectGetWidth(rect));
    CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor] );
    CGContextStrokePath(context);
}

- (void)didMoveToSuperview {
    
    [self.separatorHideView removeFromSuperview];
    
    CGRect separatorRect = CGRectMake(self.frame.origin.x,
                                      self.frame.size.height,
                                      self.frame.size.width,
                                      3.0);
    
    self.separatorHideView = [[UIView alloc] initWithFrame:separatorRect];
    self.separatorHideView.backgroundColor = [UIColor colorWithRed:0.612 green:0.608 blue:0.651 alpha:1.000];
    self.separatorHideView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self addSubview:self.separatorHideView];
}

@end
