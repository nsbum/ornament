/**
 *   @file CCFAccessoryKeyView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 08:21:00
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFAccessoryKeyView.h"

#import <QuartzCore/QuartzCore.h>

#import "CCFCharClassKeyboardPopoverViewController.h"

static CCFAccessoryKeyView *commonInit(CCFAccessoryKeyView *self) {
    self.state = NO;
    
    self.keyLabel = @"";
    self.backgroundColor = [UIColor clearColor];
    
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.7f;
    self.layer.shadowRadius = 1.5f;
    self.layer.shadowOffset = CGSizeMake(1, 1);
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapRecognizer];
    
    UILongPressGestureRecognizer *pressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(pressAction:)];
    [self addGestureRecognizer:pressRecognizer];
    
    return self;
}

@implementation CCFAccessoryKeyView {
    UIPopoverController *_charClassPopover;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return commonInit(self);
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    return commonInit(self);
}


- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    //return;
    
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor *keyTopColor = nil;
    UIColor *keyBottomColor = nil;
    if( self.state ) {
        
        keyTopColor = [UIColor colorWithRed: 0.758 green: 0.758 blue: 0.767 alpha: 1];
        keyBottomColor = [UIColor colorWithRed: 0.543 green: 0.542 blue: 0.561 alpha: 1];
    }
    else {
        keyTopColor = [UIColor colorWithRed:0.875 green:0.875 blue:0.886 alpha:1.000];
        keyBottomColor = [UIColor colorWithRed:0.663 green:0.663 blue:0.682 alpha:1.000];
        
    }
    
    
    //// Gradient Declarations
    NSArray* keyGradientColors = [NSArray arrayWithObjects:
                                  (id)keyTopColor.CGColor,
                                  (id)keyBottomColor.CGColor, nil];
    CGFloat keyGradientLocations[] = {0, 1};
    CGGradientRef keyGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)keyGradientColors, keyGradientLocations);
    
    //// Frames
    //CGRect frame = CGRectMake(0, 82, 76, 38);
    CGRect frame = rect;
    
    //// Shadow Declarations
    UIColor* labelShadow = (self.state)?[UIColor blackColor]:[UIColor whiteColor];
    CGSize labelShadowOffset = CGSizeMake(0.1, 1.1);
    CGFloat labelShadowBlurRadius = 1;

    
    //// keyRect Drawing
    //CGRect keyRectRect = CGRectMake(CGRectGetMinX(frame) + 1.5, CGRectGetMinY(frame) + 0.5, 55, 35);
    CGRect keyRectRect = rect;
    UIBezierPath* keyRectPath = [UIBezierPath bezierPathWithRoundedRect: keyRectRect cornerRadius: 7];
    CGContextSaveGState(context);
    [keyRectPath addClip];
    CGContextDrawLinearGradient(context, keyGradient,
                                CGPointMake(CGRectGetMidX(keyRectRect), CGRectGetMinY(keyRectRect)),
                                CGPointMake(CGRectGetMidX(keyRectRect), CGRectGetMaxY(keyRectRect)),
                                0);
    CGContextRestoreGState(context);
    [[UIColor blackColor] setStroke];
    keyRectPath.lineWidth = 1;
    [keyRectPath stroke];
    
    //// keyLabel Drawing
    CGRect keyLabelRect = CGRectMake(CGRectGetMinX(frame) + 23, CGRectGetMinY(frame) +3, 13, 34);
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, labelShadowOffset, labelShadowBlurRadius, labelShadow.CGColor);
    UIColor *fillColor = (self.state)?[UIColor whiteColor] : [UIColor blackColor];
    [fillColor setFill];
    UIFont *keyLabelFont = [UIFont fontWithName:@"Menlo" size:24];
    [[self keyLabel] drawInRect: keyLabelRect withFont:keyLabelFont lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];
    CGContextRestoreGState(context);
    
    //// Cleanup
    CGGradientRelease(keyGradient);
    CGColorSpaceRelease(colorSpace);
    

}

#pragma mark - Accessors

- (void)setKeyLabel:(NSString *)keyLabel {
    _keyLabel = keyLabel;
    [self setNeedsDisplay];
    
}

- (UIPopoverController *)charClassPopover {
    if( !_charClassPopover ) {
        CCFCharClassKeyboardPopoverViewController *vc = nil;
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        vc = [[CCFCharClassKeyboardPopoverViewController alloc] initWithNibName:@"CCFCharClassKeyboardPopoverView" bundle:bundle];
        vc.contentSizeForViewInPopover = CGSizeMake(364.0f, 94.0f);
        vc.keyPressDelegate = self;
        
        _charClassPopover = [[UIPopoverController alloc] initWithContentViewController:vc];
    }
    return _charClassPopover;
}

#pragma mark - Gestures

- (void)tapAction:(UIGestureRecognizer *)recognizer {
    self.state = YES;
    [self setNeedsDisplay];
    
    if( self.tapBlock ) {
        self.tapBlock(self.keyLabel);
    }
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.19f * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self.state = NO;
        [self setNeedsDisplay];
    });
}

- (void)pressAction:(UIGestureRecognizer *)recognizer {
    UIGestureRecognizerState pressState = recognizer.state;
    if( pressState == UIGestureRecognizerStateBegan ) {
        self.state = YES;
        [self setNeedsDisplay];
        
        if( [[self keyLabel] isEqualToString:@"\\"] ) {
            [[self charClassPopover] presentPopoverFromRect:self.frame inView:self.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
    else if( pressState == UIGestureRecognizerStateEnded ) {
        self.state = NO;
        [self setNeedsDisplay];
    }
    
}

#pragma mark - CCFCharClassKeyboardControllerKeyPressDelegate

- (void)charClassKeyboardPopoverControllerDidReceiveSequence:(NSString *)sequence {
    if( self.tapBlock ) {
        self.tapBlock(sequence);
    }
}

@end
