/**
 *   @file CCFSmallAccessoryKeyDelegate.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 08:19:21
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

@class CCFSmallAccessoryKeyView;

@protocol CCFSmallAccessoryKeyDelegate <NSObject>

- (void)smallAccessoryKeyViewDidReceiveTap:(CCFSmallAccessoryKeyView *)keyView;

@end
