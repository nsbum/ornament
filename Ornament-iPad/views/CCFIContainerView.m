//
//  CCFContainerView.m
//  Ornament
//
//  Created by alanduncan on 11/8/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "CCFIContainerView.h"
#import <QuartzCore/QuartzCore.h>

static CCFIContainerView *commonInit(CCFIContainerView *self) {
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.layer.borderWidth = 1.0f;
    
    return self;
}

@implementation CCFIContainerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return commonInit(self);
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) { return nil; }
    
    return commonInit(self);
}

@end
