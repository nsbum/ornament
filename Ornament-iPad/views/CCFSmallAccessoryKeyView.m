/**
 *   @file CCFSmallAccessoryKeyView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 08:22:37
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFSmallAccessoryKeyView.h"
#import "CCFSmallAccessoryKeyDelegate.h"
#import <QuartzCore/QuartzCore.h>

static CCFSmallAccessoryKeyView *commonInit(CCFSmallAccessoryKeyView *self) {
    self.state = NO;
    
    self.keyLabel = @"";
    self.backgroundColor = [UIColor clearColor];
    
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.7f;
    self.layer.shadowRadius = 1.5f;
    self.layer.shadowOffset = CGSizeMake(1, 1);
    
    UILongPressGestureRecognizer *tapRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapRecognizer.minimumPressDuration = 0.1;
    [self addGestureRecognizer:tapRecognizer];
    
    return self;
}

@implementation CCFSmallAccessoryKeyView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return commonInit(self);
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    return commonInit(self);
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor *keyTopColor = nil;
    UIColor *keyBottomColor = nil;
    if( self.state ) {
        
        keyTopColor = [UIColor colorWithRed: 0.758 green: 0.758 blue: 0.767 alpha: 1];
        keyBottomColor = [UIColor colorWithRed: 0.543 green: 0.542 blue: 0.561 alpha: 1];
    }
    else {
        keyTopColor = [UIColor colorWithRed:0.875 green:0.875 blue:0.886 alpha:1.000];
        keyBottomColor = [UIColor colorWithRed:0.663 green:0.663 blue:0.682 alpha:1.000];
        
    }
    
    //// Gradient Declarations
    NSArray* keyGradientColors = [NSArray arrayWithObjects:
                                  (id)keyTopColor.CGColor,
                                  (id)keyBottomColor.CGColor, nil];
    CGFloat keyGradientLocations[] = {0, 1};
    CGGradientRef keyGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)keyGradientColors, keyGradientLocations);
    
    //// Shadow Declarations
    UIColor* labelShadow = (self.state)?[UIColor blackColor]:[UIColor whiteColor];;
    CGSize labelShadowOffset = CGSizeMake(0.1, 1.1);
    CGFloat labelShadowBlurRadius = 1;
    
    //// Frames
    CGRect frame = CGRectMake(0, 0, 39, 26);
    

    
    //// keyRect Drawing
    CGRect keyRectRect = CGRectMake(CGRectGetMinX(frame) + 1, CGRectGetMinY(frame) + 1, 36, 23);
    UIBezierPath* keyRectPath = [UIBezierPath bezierPathWithRoundedRect: keyRectRect cornerRadius: 7];
    CGContextSaveGState(context);
    [keyRectPath addClip];
    CGContextDrawLinearGradient(context, keyGradient,
                                CGPointMake(CGRectGetMidX(keyRectRect), CGRectGetMinY(keyRectRect)),
                                CGPointMake(CGRectGetMidX(keyRectRect), CGRectGetMaxY(keyRectRect)),
                                0);
    CGContextRestoreGState(context);
    [[UIColor blackColor] setStroke];
    keyRectPath.lineWidth = 1;
    [keyRectPath stroke];
    
    
    //// keyLabel Drawing
    CGRect keyLabelRect = CGRectMake(CGRectGetMinX(frame) + 7, CGRectGetMinY(frame) + 5, 26, 23);
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, labelShadowOffset, labelShadowBlurRadius, labelShadow.CGColor);
    UIColor *fillColor = (self.state)?[UIColor whiteColor] : [UIColor blackColor];
    [fillColor setFill];
    [[self keyLabel] drawInRect: keyLabelRect withFont: [UIFont fontWithName: @"Menlo-Regular" size: 14] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter ];
    CGContextRestoreGState(context);
    
    
    //// Cleanup
    CGGradientRelease(keyGradient);
    CGColorSpaceRelease(colorSpace);
    

}

#pragma mark - Accessors

- (void)setKeyLabel:(NSString *)keyLabel {
    _keyLabel = keyLabel;
    [self setNeedsDisplay];
    
}

#pragma mark - Gestures

- (void)tapAction:(UIGestureRecognizer *)recognizer {
    if( recognizer.state == UIGestureRecognizerStateBegan ) {
        self.state = YES;
        [self setNeedsDisplay];
    }
    else if( recognizer.state == UIGestureRecognizerStateEnded ) {
        if( self.tapBlock ) {
            self.tapBlock(self.keyLabel);
        }
        [[self delegate] smallAccessoryKeyViewDidReceiveTap:self];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.19f * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            self.state = NO;
            [self setNeedsDisplay];
        });
    }
}


@end
