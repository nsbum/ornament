/**
 *   @file CCFAccessoryKeyView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2012-11-12 08:20:54
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFCharClassKeyboardPopoverControllerKeyPressDelegate.h"

typedef void(^CCFKeyViewTapAction)(NSString *keyChar);

@interface CCFAccessoryKeyView : UIView <CCFCharClassKeyboardPopoverControllerKeyPressDelegate>

@property (nonatomic, assign) BOOL state;
@property (nonatomic, strong) NSString *keyLabel;
@property (nonatomic, strong) CCFKeyViewTapAction tapBlock;

@end
