//
//  main.m
//  Ornament-iPad
//
//  Created by alanduncan on 11/7/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate_iPad.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate_iPad class]));
    }
}
