//
//  conveniences.m
//  Ornament
//
//  Created by alanduncan on 11/8/12.
//  Copyright (c) 2012 Cocoa Factory, LLC. All rights reserved.
//

#import "conveniences.h"


NSRange entireStringRange(id string) {
    return NSMakeRange(0, [string length] );
}