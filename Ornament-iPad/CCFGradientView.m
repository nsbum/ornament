/**
 *   @file CCFGradientView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2011-07-15 08:19:43
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFGradientView.h"
#import <QuartzCore/QuartzCore.h>
#import "CCFUIParameters.h"
//#import "macros.h"
@implementation CCFGradientView

+ (CGFloat)dropShadowThickness;
{
    return kCCFUIShadowEdgeThickness;
}

+ (CCFGradientView *)horizontalShadow:(BOOL)bottomToTop;
{
    CCFGradientView *instance = [[self alloc] init];
    
    UIColor *bottomColor = [UIColor colorWithWhite:0.0 alpha:kCCFUIShadowEdgeMaximumAlpha];
    UIColor *topColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    
    if (!bottomToTop) {
        UIColor *tempColor = bottomColor;
        bottomColor = topColor;
        topColor = tempColor;
    }
    //SWAP(bottomColor, topColor);
    
    [instance fadeVerticallyFromColor:bottomColor toColor:topColor];
    return instance;
}

+ (CCFGradientView *)verticalShadow:(BOOL)leftToRight;
{
    CCFGradientView *instance = [[self alloc] init];
    
    UIColor *leftColor = [UIColor colorWithWhite:0.0 alpha:kCCFUIShadowEdgeMaximumAlpha];
    UIColor *rightColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    
    if (!leftToRight) {
        UIColor *tempColor = leftColor;
        leftColor = rightColor;
        rightColor = tempColor;
    }
        //SWAP(leftColor, rightColor);
    
    [instance fadeHorizontallyFromColor:leftColor toColor:rightColor];
    return instance;
}

static id _commonInit(CCFGradientView *self)
{
    self.userInteractionEnabled = NO;
    return self;
}

- initWithFrame:(CGRect)frame;
{
    if (!(self = [super initWithFrame:frame]))
        return nil;
    return _commonInit(self);
}

- initWithCoder:(NSCoder *)coder;
{
    if (!(self = [super initWithCoder:coder]))
        return nil;
    return _commonInit(self);
}

- (void)fadeHorizontallyFromColor:(UIColor *)leftColor toColor:(UIColor *)rightColor;
{
    // Needed, at least, for transitioning between the highlighted state when in row-marking mode.
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    {
        CAGradientLayer *layer = (CAGradientLayer *)self.layer;
        layer.colors = [NSArray arrayWithObjects:(id)[leftColor CGColor], (id)[rightColor CGColor], nil];
        
        layer.startPoint = CGPointMake(0.0, 0.5);
        layer.endPoint = CGPointMake(1.0, 0.5);
        layer.type = kCAGradientLayerAxial;
    }
    [CATransaction commit];
}

- (void)fadeVerticallyFromColor:(UIColor *)bottomColor toColor:(UIColor *)topColor; // where 'bottom' == min y
{    
    // Needed, at least, for transitioning between the highlighted state when in row-marking mode.
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    {
        CAGradientLayer *layer = (CAGradientLayer *)self.layer;
        layer.colors = [NSArray arrayWithObjects:(id)[bottomColor CGColor], (id)[topColor CGColor], nil];
        
        layer.startPoint = CGPointMake(0.5, 0.0);
        layer.endPoint = CGPointMake(0.5, 1.0);
        layer.type = kCAGradientLayerAxial;
    }
    [CATransaction commit];
}

#pragma mark -
#pragma mark UIView subclass

+ (Class)layerClass;
{
    return [CAGradientLayer class];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event;
{
    UIView *hitView = [super hitTest:point withEvent:event];
    if (hitView == self)
        return nil;
    return hitView;
}

@end
