/**
 *   @file CCFBarButtonItem.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2011-04-30 05:58:11
 *   @version 1.0
 *
 *   @note  Copyright (c) 2011 Cocoa Factory, LLC. All rights reserved.
 *          Permission is hereby granted, free of charge, to any person obtaining a copy of this
 *          software and associated documentation files (the "Software"), to deal in the
 *          Software without restriction, including without limitation the rights to use, copy,
 *          modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *          and to permit persons to whom the Software is furnished to do so, subject to the
 *          following conditions:
 *          The above copyright notice and this permission notice shall be included in all
 *          copies or substantial portions of the Software.
 *          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *          INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 *          PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *          HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *          CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 *          OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import <uIKit/UIBarButtonItem.h>

@interface NSObject (CCFBlockCallback)
- (void)CCFCallbackBlock;
- (void)CCFCallbackBlockWithSender:(id)sender;
@end

@implementation NSObject(CCFBlockCallback)
- (void)CCFCallbackBlock
{ 
    void (^block)(void) = (id)self;
    block();
}
- (void)CCFCallbackBlockWithSender:(id)sender {
    void (^block)(id obj) = (id)self;
    block(sender); 
}
@end

@interface CCFBarButtonItem : UIBarButtonItem
{
@protected
    id _originalTarget;
}

- (id)initWithImage:(UIImage *)image target:(id)target action:(SEL)action;
- (id)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem block:(void (^)(id))block;
- (id)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style block:(void (^)(id))aBlock;
- (id)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style block:(void (^)(id))aBlock;
- (id)initWithLightInfoButtonBlock:(void (^)(id))aBlock;
- (id)initWithLightInfoButtonTarget:(id)target action:(SEL)action;

@end
